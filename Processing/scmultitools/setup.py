import os

from setuptools import Command, find_packages, setup

__version__ = None
exec(open("scmultitools/version.py").read())


class CleanCommand(Command):
    """
    Custom clean command to tidy up the project root.
    """

    user_options = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        os.system("rm -vrf ./build ./dist ./*.pyc ./*.tgz ./*.egg-info ./htmlcov")


setup(
    name="scmultitools",
    version=__version__,
    description="Useful functions for multiome Analysis in Python",
    author="Paul-Georg Majev",
    author_email="majev@paul-ge.org",
    license="GNUGPLv3",
    setup_requires=["setuptools>=18.0", "cython"],
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        "numpy>=1.22.3",
        "scipy",
        "matplotlib>=3.1.0",
        "pandas>=0.15.0",
        "anndata>=0.8.0",
        "scanpy",
        "gseapy",
        "requests",
        "muon",
        "pysam",
        "polars",
        "pyarrow>=1.0.1",
        "seaborn",
        "tqdm",
        "MACS2",
        "pyranges",
        "statsmodels",
        "plotnine",
        "torch",
        "genomepy",
        "gimmemotifs",
        "patsy",
        "scikit-learn",
        "pybedtools",
        "rpy2[all]==3.4.2",
        "anndata2ri",
        "leidenalg",
    ],
    cmdclass={"clean": CleanCommand},
)
