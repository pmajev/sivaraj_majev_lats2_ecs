# scMultiTools

This is a constantly growing repository of losely connected helper functions, useful for single-cell multiome Analysis (scATAC- and scRNA-Seq in particular) building on the frameworks of [Anndata](https://anndata.readthedocs.io/en/latest/), [Scanpy](https://scanpy.readthedocs.io/en/stable/) and [Muon](https://muon.readthedocs.io/en/latest/).

So far this repository includes functions to:
- Process unique cutting-sites from STAR/CellRanger Output BAM files into Fragment File Format ("filter_reads")
- Call Peaks from these Fragment files ("callPeaks")
- Calculate reproducible Peaks similar to the implementation in ArchR ("getReproduciblePeaks")
- Process Called Peaks and Fragments into Peak x Cell matrices ("createPeakMatrix") and annotate these with various statistics ("prepare_atac_matrix","CalculateFractions","calculate_peak_stats")
- Find similar background peaks based on calculated metrics ("get_background_peaks")
- Plot QC Graphs for both ATAC and GEX Modalities ("plot_ATAC_qc"/"plot_GEX_qc")
- Filter ATAC and GEX modalities based on calculated QC statistics ("filter_ATAC_cells"/"filter_GEX_cells")
- Get significant (compared to background sequences) transcription factor binding motif sites within peaks ("count_motifs_in_peaks")
- Run a full python implementation of the chromVAR method ("chromVAR")
- Run scOpen on the ATAC data ("run_scopen")
- Run a wrapper for the R method slingshot ("run_slingshot")
- Run a wrapper for the scran normalisation method implemented in R ("scran_adata")
- Run a wrapper for the scDblFinder method implemented in R ("run_scDblFinder")
- Calculate Pseudobulk differential gene expression, wrapping DeSeq2 implemented in R ("run_deseq2")
- Calculate binned Pseudotime ("CreatePseudotimeAdata") and plot it in various ways ("DotPlotPseudotime","PlotPseudotimeHeatmap","PlotPseudotimeLines")
- Get Dictionary with Barcode names and cell identity annotations, for splitting bam/Fragment files by cell identity ("GetReadAnnotation")
- Plot All peaks containing a TFs motif across all cells in the data as a heatmap
- Various other plotting functions found in plotting.py

This work is licensed under the GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007






- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)
- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)

***
