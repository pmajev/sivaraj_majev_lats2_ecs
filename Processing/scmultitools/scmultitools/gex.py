import logging
import os
import re

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import plotnine as p
import scanpy as sc
import scvi
import seaborn as sns
from anndata import AnnData
from mudata import MuData

logger = logging.getLogger(__name__)


def load_GEX_STAR(
    base_path,
    sample_id,
    sample_group,
    STAR_assay="Gene",
    matrix_name="matrix.mtx",
    round_matrix=False,
    velocity=False,
    normalize=False,
    filtering=True,
    plotting=True,
    min_cells=20,
    min_counts=1,
):
    logger.info("Reading in Count Matrix")
    base_matrix = sc.read_mtx(os.path.join(base_path, STAR_assay, "raw", matrix_name))
    feature_names = pd.read_csv(
        os.path.join(base_path, STAR_assay, "raw", "features.tsv"),
        sep="\t",
        names=["symbol", "annotation"],
        header=None,
        index_col=0,
    )

    feature_names["ENSEMBL_ID"] = feature_names.index.tolist()

    cell_names = pd.read_csv(
        os.path.join(base_path, STAR_assay, "raw", "barcodes.tsv"),
        sep="\t",
        names=[],
        header=None,
        index_col=0,
    )

    logger.info("Creating AnnData Object")
    adata = sc.AnnData(base_matrix.X.T, obs=cell_names, var=feature_names)

    adata.var.index = adata.var["symbol"].to_list()
    adata.var_names_make_unique()

    adata.obs["SAMPLE"] = sample_id
    adata.obs["GROUP"] = sample_group

    if velocity:
        logger.info("Reading in Velocity Matrices")
        spliced_matrix = sc.read_mtx(
            os.path.join(base_path, "Velocyto", "raw", "spliced.mtx")
        )
        unspliced_matrix = sc.read_mtx(
            os.path.join(base_path, "Velocyto", "raw", "unspliced.mtx")
        )
        adata.layers["spliced"] = spliced_matrix.X.T
        adata.layers["unspliced"] = unspliced_matrix.X.T

    adata.layers["counts"] = adata.X.copy()

    if round_matrix:
        logger.info(
            "Rounding matrix values to closest integer and storing in 'counts' layer. Non rounded matrix will be stored in 'raw' layer"
        )
        adata.layers["raw"] = adata.layers["counts"].copy()
        adata.layers["counts"].data = np.round(adata.layers["counts"].data)
        adata.X = adata.layers["raw"].copy()

    logger.info("Calculating Basic Statistics")

    adata.var["mito"] = np.asarray(
        [bool(re.search("^mt-|^MT-", x)) for x in adata.var_names.tolist()]
    )
    adata.var["ribo"] = np.asarray(
        [
            bool(re.search("Rp[sl]|RP[SL]|Rn18s|RN18S", x))
            for x in adata.var_names.tolist()
        ]
    )
    adata.var["hemo"] = np.asarray(
        [bool(re.search("Hb[abq]|HB[ABQ]", x)) for x in adata.var_names.tolist()]
    )

    adata.var["mean_"] = np.array(adata.layers["counts"].mean(0))[0]
    adata.var["frac_zero"] = (
        1 - np.array((adata.layers["counts"] > 0).sum(0))[0] / adata.shape[0]
    )

    if filtering:
        logger.info("Filtering Cells (also needed for normalisation)")
        n_all_cells = len(adata.obs)
        sc.pp.filter_genes(adata, min_cells=min_cells)
        sc.pp.filter_cells(adata, min_counts=min_counts)
        n_filtered_cells = len(adata.obs)
        logger.info(
            "Remaining Cells: {remaining}/{original}".format(
                remaining=n_filtered_cells, original=n_all_cells
            )
        )

    sc.pp.calculate_qc_metrics(
        adata,
        qc_vars=["mito", "ribo", "hemo"],
        percent_top=None,
        log1p=False,
        inplace=True,
    )

    adata.obs["complexity"] = np.log10(adata.obs["n_genes_by_counts"]) / np.log10(
        adata.obs["total_counts"]
    )

    return adata.copy()


def plot_GEX_qc(
    adata_input, settings, output_folder, output=False, min_count_plotted=10
):
    if isinstance(adata_input, AnnData):
        adata = adata_input.copy()
    elif isinstance(adata_input, MuData) and "rna" in adata_input.mod:
        adata = adata_input.mod["rna"].copy()
    else:
        raise TypeError("Expected AnnData or MuData object with 'rna' modality")
    logger.info("Prepare Data for QC")
    # Preparing Data for Analysis #####
    count_depth_per_cell_sorted = np.sort(adata.obs["total_counts"])[::-1]
    count_depth_per_cell_sorted = pd.DataFrame(
        count_depth_per_cell_sorted, columns=["total_counts"]
    )

    count_depth_per_cell_sorted["rank"] = count_depth_per_cell_sorted.index + 1
    count_depth_per_cell_sorted["cum_pct"] = (
        np.cumsum(count_depth_per_cell_sorted["total_counts"])
        / np.sum(count_depth_per_cell_sorted["total_counts"])
    ) * 100

    counts_cut_off_rank = int(
        count_depth_per_cell_sorted[
            count_depth_per_cell_sorted["total_counts"]
            == min(
                count_depth_per_cell_sorted["total_counts"],
                key=lambda x: abs(x - settings["library_size_lower"]),
            )
        ]["rank"].tolist()[0]
    )

    feature_per_cell_sorted = np.sort(adata.obs["n_genes_by_counts"])[::-1]
    feature_per_cell_sorted = pd.DataFrame(
        feature_per_cell_sorted, columns=["n_genes_by_counts"]
    )

    feature_per_cell_sorted["rank"] = feature_per_cell_sorted.index + 1
    feature_per_cell_sorted["cum_pct"] = (
        np.cumsum(feature_per_cell_sorted["n_genes_by_counts"])
        / np.sum(feature_per_cell_sorted["n_genes_by_counts"])
    ) * 100

    features_cut_off_rank = int(
        feature_per_cell_sorted[
            feature_per_cell_sorted["n_genes_by_counts"]
            == min(
                feature_per_cell_sorted["n_genes_by_counts"],
                key=lambda x: abs(x - settings["number_features_lower"]),
            )
        ]["rank"].tolist()[0]
    )

    log_data = adata.obs[["SAMPLE", "total_counts", "n_genes_by_counts"]]
    log_data = log_data[
        (log_data["n_genes_by_counts"] >= min_count_plotted)
        & (log_data["total_counts"] >= min_count_plotted)
    ].copy()
    log_data["total_counts"] = np.log10(log_data["total_counts"])
    log_data["n_genes_by_counts"] = np.log10(log_data["n_genes_by_counts"])
    #######################################

    if output:
        os.makedirs(output_folder, exist_ok=True)
    logger.info("Plot Top Genes")
    # Plot Top genes #####
    plt.figure(1)
    sc.pl.highest_expr_genes(adata, n_top=20)
    plt.show()
    ##########################

    logger.info("Violin Plot for UMI Counts")
    # Violin Plot of UMI Counts #####
    plt.figure(2)
    sns.violinplot(
        x="SAMPLE", y="total_counts", data=log_data, bw=0.2, cut=0, scale="width"
    )
    sns.stripplot(
        x="SAMPLE",
        y="total_counts",
        data=log_data,
        s=1,
        alpha=0.25,
        color="black",
        edgecolor=None,
    )
    plt.axhline(np.log10(settings["library_size_lower"]))
    plt.xlabel("Sample")
    plt.ylabel("log10(Counts)")
    plt.ylim((0, None))

    if output:
        plt.savefig(os.path.join(output_folder, "Total_Counts_Violin.png"))
    else:
        plt.show()

    ######################################

    logger.info("Bar Plot for UMI Counts")
    # Bar Plot of UMI Counts #####
    plt.figure(3)
    fig, ax = plt.subplots(figsize=(12, 8))

    n, bins, patches = ax.hist(
        "total_counts",
        bins=1000,
        histtype="bar",
        align="mid",
        orientation="vertical",
        rwidth=1,
        data=log_data,
        color="black",
        edgecolor=None,
        linewidth=0,
    )
    ax.set_title("Bar Plot of UMI Counts")
    ax.set_xlabel("Total Counts", fontsize="large", color="black")
    ax.set_ylabel("Number of Cells", fontsize="large", color="black")
    ax.spines[["left", "bottom"]].set_color("black")
    ax.spines[["left", "bottom"]].set_linewidth(1)
    ax.spines[["top", "right"]].set_visible(False)
    ax.grid(False)
    ax.set_facecolor("white")
    plt.axvline(x=np.log10(settings["library_size_lower"]), color="red", label="Cutoff")
    if output:
        plt.savefig(os.path.join(output_folder, "Total_Counts_BarPlot.png"))
    else:
        plt.show()
    ##################################

    logger.info("KneePlot for UMI Counts")
    # KneePlot of UMI Counts #####
    plt.figure(4)
    f, ax = plt.subplots(figsize=(5, 5))
    ax.scatter(
        data=count_depth_per_cell_sorted,
        x="rank",
        y="total_counts",
        s=10,
        c="orange",
        alpha=1,
        linewidths=0,
        edgecolors=None,
        label=None,
    )

    ax.set_xlabel("Barcode rank")
    ax.set_ylabel("Count Depth per Cell")

    plt.xscale("log", base=10, nonpositive="clip")
    plt.yscale("log", base=10, nonpositive="clip")

    plt.axhline(y=settings["library_size_lower"], color="red", label="Cutoff")

    if output:
        plt.savefig(os.path.join(output_folder, "Total_Counts_KneePlot.png"))
    else:
        plt.show()

    ##################################

    logger.info("CumSum Plot for UMIs")
    # UMI CumSum Plot #####
    plt.figure(5)
    f, ax = plt.subplots(figsize=(5, 5))
    sns.scatterplot(
        x="rank",
        y="cum_pct",
        s=10,
        alpha=1,
        linewidth=0,
        data=count_depth_per_cell_sorted,
        ax=ax,
    )

    ax.set_xlabel("Barcode rank")
    ax.set_ylabel("Cumulative Count Depth Percentage")

    plt.xscale("log", base=10, nonpositive="clip")
    plt.yscale("log", base=10, nonpositive="clip")

    plt.axvline(x=counts_cut_off_rank, color="red", label="Cutoff")

    if output:
        plt.savefig(os.path.join(output_folder, "Total_Counts_CumSum.png"))
    else:
        plt.show()

    ##########################

    logger.info("UMI vs. Feature Number")
    # UMI Counts vs Number of Genes Scatter #####
    plt.figure(6)
    adata.obs.sort_values("pct_counts_mito").plot.scatter(
        x="total_counts",
        y="n_genes_by_counts",
        c="pct_counts_mito",
        s=2,
        cmap="magma",
        vmin=0,
        vmax=100,
    )
    ax.set_ylabel("Number of Features")
    ax.set_xlabel("Number of Total Counts")

    plt.axhline(
        y=settings["number_features_lower"],
        color="red",
        label="Feature Number Cutoff",
        linewidth=1,
    )
    plt.axvline(
        x=settings["library_size_lower"],
        color="red",
        label="Library Size Cutoff",
        linewidth=1,
    )

    if output:
        plt.savefig(os.path.join(output_folder, "Total_Counts_vs_Genes_Scatter.png"))
    else:
        plt.show()

    #################################################

    logger.info("Bar Plot for Feature Number")
    # Number of Genes BarPlot #####
    plt.figure(7)
    fig, ax = plt.subplots(figsize=(12, 8))
    n, bins, patches = ax.hist(
        "n_genes_by_counts",
        bins=1000,
        histtype="bar",
        align="mid",
        orientation="vertical",
        rwidth=1,
        data=log_data,
        color="black",
        edgecolor=None,
        linewidth=0,
    )
    ax.set_title("Bar Plot of Feature number")
    ax.set_xlabel("Total Counts", fontsize="large", color="black")
    ax.set_ylabel("Number of Cells", fontsize="large", color="black")
    ax.spines[["left", "bottom"]].set_color("black")
    ax.spines[["left", "bottom"]].set_linewidth(1)
    ax.spines[["top", "right"]].set_visible(False)
    ax.grid(False)
    ax.set_facecolor("white")
    plt.axvline(
        x=np.log10(settings["number_features_lower"]), color="red", label="Cutoff"
    )

    if output:
        plt.savefig(os.path.join(output_folder, "Features_BarPlot.png"))
    else:
        plt.show()

    ###################################

    logger.info("CumSum Plot for Feature Number")
    # Number of genes CumSum Plot #####
    plt.figure(8)
    f, ax = plt.subplots(figsize=(5, 5))
    sns.scatterplot(
        x="rank",
        y="cum_pct",
        s=10,
        alpha=1,
        linewidth=0,
        data=feature_per_cell_sorted,
        ax=ax,
    )

    ax.set_xlabel("Barcode rank")
    ax.set_ylabel("Cumulative Feature Percentage")

    plt.xscale("log", base=10, nonpositive="clip")
    plt.yscale("log", base=10, nonpositive="clip")

    plt.ylim((1, 150))
    plt.xlim((1, 100000))
    plt.axvline(
        x=features_cut_off_rank,
        color="red",
        label="Cutoff " + str(settings["number_features_lower"]),
    )

    if output:
        plt.savefig(os.path.join(output_folder, "Features_CumSum.png"))
    else:
        plt.show()
    #######################################

    logger.info("Scatter Plot for Complexity")
    # Complexity per Cell Plot #####
    plt.figure(9)
    sns.displot(
        adata.obs,
        x="complexity",
        kde=True,
        rug=True,
        kde_kws={"bw_adjust": 2, "cut": 0},
        aspect=1,
        height=5,
        binwidth=100,
    )

    plt.axvline(x=settings["complexity"], color="red", label="Cutoff")

    plt.xlim((0.7, 1))
    if output:
        plt.savefig(os.path.join(output_folder, "Complexity_per_Cell.png"))
    else:
        plt.show()
    ####################################

    logger.info("Scatter Plot for Mitochondrial Percentage")

    # Mitochondrial Percentage Scatter #####
    plt.figure(10)
    f, ax = plt.subplots(figsize=(5, 5))
    sns.scatterplot(
        x="n_genes_by_counts",
        y="pct_counts_mito",
        s=3,
        alpha=0.5,
        linewidth=0,
        data=adata.obs,
        ax=ax,
    )

    ax.set_xlabel("Number of Features")
    ax.set_ylabel("Percentage of mitochondrial Reads")

    plt.ylim((1, 100))
    plt.xlim((1, 10000))
    plt.axhline(y=settings["mito_level"], color="red", label="Cutoff")
    plt.axvline(
        x=settings["number_features_lower"],
        color="red",
        label="Feature Number Cutoff",
        linewidth=1,
    )
    if output:
        plt.savefig(os.path.join(output_folder, "Mitochondrial_Percentage.png"))
    else:
        plt.show()
    ############################################

    logger.info("Scatter Plot for Ribosomal Percentage")
    # Ribosomal Percentage Scatter #####
    plt.figure(11)
    f, ax = plt.subplots(figsize=(5, 5))
    sns.scatterplot(
        x="n_genes_by_counts",
        y="pct_counts_ribo",
        s=3,
        alpha=0.5,
        linewidth=0,
        data=adata.obs,
        ax=ax,
    )

    ax.set_xlabel("Number of Features")
    ax.set_ylabel("Percentage of ribosomal Reads")

    plt.ylim((1, 100))
    plt.xlim((1, 10000))
    plt.axhline(y=settings["ribo_level"], color="red", label="Cutoff")
    plt.axvline(
        x=settings["number_features_lower"],
        color="red",
        label="Feature Number Cutoff",
        linewidth=1,
    )
    if output:
        plt.savefig(os.path.join(output_folder, "Ribosomal_Percentage.png"))
    else:
        plt.show()
    ########################################

    logger.info("Scatter Plot for Hemoglobin Percentage")
    # Hemoglobin Percentage Scatter #####
    plt.figure(12)
    f, ax = plt.subplots(figsize=(5, 5))
    sns.scatterplot(
        x="n_genes_by_counts",
        y="pct_counts_hemo",
        s=3,
        alpha=0.5,
        linewidth=0,
        data=adata.obs,
        ax=ax,
    )

    ax.set_xlabel("Total Counts")
    ax.set_ylabel("Percentage of hemoglobin Reads")

    plt.ylim((1, 100))
    plt.xlim((1, 10000))
    plt.axhline(y=settings["hemo_level"], color="red", label="Cutoff")
    plt.axvline(
        x=settings["number_features_lower"],
        color="red",
        label="Feature Number Cutoff",
        linewidth=1,
    )
    if output:
        plt.savefig(os.path.join(output_folder, "Hemoglobin_Percentage.png"))
    else:
        plt.show()
    #########################################

    # Plot Count distribution per Gene #####
    logger.info("Calculate highly variable Genes using scVI")
    scvi.data.poisson_gene_selection(adata, n_top_genes=5_000, layer="counts")
    logger.info("Scatter Plot of Zero enrichment")
    p.options.figure_size = (5, 5)
    count_dist = (
        p.ggplot(
            p.aes(x="mean_", y="observed_fraction_zeros", color="prob_zero_enrichment"),
            adata.var,
        )
        + p.geom_point(p.aes(y="expected_fraction_zeros"), color="r")
        + p.geom_point()
        + p.geom_point(
            data=adata.var.query("highly_variable"),
            shape=".",
            size=0.2,
            color="k",
            alpha=0.5,
        )
        + p.scale_x_log10()
        + p.theme_minimal()
    )
    count_dist.draw(show=True)
    if output:
        count_dist.save(
            filename=os.path.join(output_folder, "Gene_Count_Distribution.png"),
            height=5,
            width=5,
            units="in",
            dpi=300,
        )
    ############################################
    return


def filter_GEX_cells(adata_input, settings):
    if isinstance(adata_input, AnnData):
        adata = adata_input.copy()
    elif isinstance(adata_input, MuData) and "rna" in adata_input.mod:
        adata = adata_input.mod["rna"].copy()
    else:
        raise TypeError("Expected AnnData or MuData object with 'atac' modality")

    umi_per_cell = adata.obs[
        adata.obs["total_counts"] >= settings["library_size_lower"]
    ].index.tolist()

    features_per_cell = adata.obs[
        adata.obs["n_genes_by_counts"] >= settings["number_features_lower"]
    ].index.tolist()

    complexity_per_cell = adata.obs[
        adata.obs["complexity"] >= settings["complexity"]
    ].index.tolist()

    mitochondrial_per_cell = adata.obs[
        adata.obs["pct_counts_mito"] <= settings["mito_level"]
    ].index.tolist()

    ribosomal_per_cell = adata.obs[
        adata.obs["pct_counts_ribo"] <= settings["ribo_level"]
    ].index.tolist()

    hemoglobin_per_cell = adata.obs[
        adata.obs["pct_counts_hemo"] <= settings["hemo_level"]
    ].index.tolist()

    lists_of_cells = [
        umi_per_cell,
        features_per_cell,
        complexity_per_cell,
        mitochondrial_per_cell,
        ribosomal_per_cell,
        hemoglobin_per_cell,
    ]

    gex_final_cells = set.intersection(*map(set, list(lists_of_cells)))

    len_umi_per_cell = len(umi_per_cell)
    len_features_per_cell = len(features_per_cell)
    len_complexity_per_cell = len(complexity_per_cell)
    len_mitochondrial_per_cell = len(mitochondrial_per_cell)
    len_ribosomal_per_cell = len(ribosomal_per_cell)
    len_hemoglobin_per_cell = len(hemoglobin_per_cell)

    gex_filtering_values = [
        "Original: " + str(len(adata.obs)),
        "Total Counts: " + str(len_umi_per_cell),
        "Features per Cell: " + str(len_features_per_cell),
        "Complexity: " + str(len_complexity_per_cell),
        "Mitochondrial Fraction: " + str(len_mitochondrial_per_cell),
        "Ribosomal Fraction: " + str(len_ribosomal_per_cell),
        "Hemoglobin Fraction: " + str(len_hemoglobin_per_cell),
        "Final Number: " + str(len(gex_final_cells)),
    ]

    for message in gex_filtering_values:
        logger.info(message)

    adata.obs["cell_name"] = adata.obs.index.tolist()
    adata = adata[adata.obs["cell_name"].isin(gex_final_cells)].copy()
    genes_pf = adata.shape[1]
    sc.pp.filter_genes(adata, min_cells=settings["min_cells_per_feature"])
    sc.pp.filter_genes(adata, min_counts=settings["min_total_expression_per_gene"])

    sc.pp.calculate_qc_metrics(
        adata,
        qc_vars=["mito", "ribo", "hemo"],
        percent_top=None,
        log1p=False,
        inplace=True,
    )
    genes_af = adata.shape[1]
    logger.info("Kept {kept}/{all} Genes".format(kept=genes_af, all=genes_pf))

    if isinstance(adata_input, AnnData):
        return adata.copy()
    elif isinstance(adata_input, MuData) and "rna" in adata_input.mod:
        adata_input.mod["rna"] = adata.copy()
        return adata_input.copy()
