import logging
from collections import defaultdict

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import scanpy as sc
import scipy
import seaborn as sns
from anndata import AnnData
from mudata import MuData
from patsy import dmatrix
from scipy.stats import combine_pvalues

logger = logging.getLogger(__name__)

#####
# The functions in this modules are based on code provided by Kai Kruse, for which I am greatly thankful
#####

try:
    import anndata2ri
    import anndata2ri.scipy2ri
    import rpy2
    import rpy2.rinterface_lib.callbacks
    import rpy2.robjects as ro

    #    import rpy2.robjects.packages as rpacks
    from rpy2.robjects import numpy2ri, pandas2ri
    from rpy2.robjects.conversion import Converter
    from rpy2.robjects.conversion import converter as template_converter
    from rpy2.robjects.conversion import localconverter

    #    from rpy2.robjects.packages import importr

    rpy2.rinterface_lib.callbacks.logger.setLevel(logging.ERROR)

    def _none2null(none_obj):
        return ro.r("NULL")

    none_converter = Converter("None converter")
    none_converter.py2rpy.register(type(None), _none2null)

    template_converter += ro.default_converter
    template_converter += numpy2ri.converter
    template_converter += anndata2ri.converter
    template_converter += pandas2ri.converter
    template_converter += anndata2ri.scipy2ri.converter
    template_converter += none_converter
    with_rpy2 = True

except (ModuleNotFoundError, OSError) as e:
    logger.error("Cannot load rpy2: {}".format(e))
    with_rpy2 = False

logger = logging.getLogger(__name__)


def run_slingshot(
    adata,
    cluster_col,
    starting_clusters,
    target_clusters,
    output_folder,
    output=False,
    layer="data",
    use_dim="X_umap",
    n_dims=None,
):
    logger.info("Importing Data into R")

    adata = adata.copy()

    if "slingshot_pt" in adata.obs.columns.tolist():
        logger.info("Removing already existing `slingshot_pt` colum` from `obs`")
        adata.obs = adata.obs.drop(["slingshot_pt"], axis=1)

    data_mat = scipy.sparse.csr_matrix(adata.layers[layer]).T.tocoo()

    if n_dims is None:
        n_dims = adata.obsm[use_dim].shape[1]

    logger.info(
        "Using {n_dims} dimensions of {dim_red}".format(dim_red=use_dim, n_dims=n_dims)
    )

    adata.var["id"] = adata.var.index.tolist()
    with localconverter(template_converter):
        ro.r.assign("data_mat", data_mat)
        ro.r.assign("obs", adata.obs)
        ro.r.assign("do_output", output)
        ro.r.assign(
            "projection",
            pd.DataFrame(adata.obsm[use_dim][:, :n_dims]),
        )
        ro.r.assign("var", adata.var)
        ro.r.assign("cluster_labels", cluster_col)
        ro.r.assign("use_dim", use_dim)
        ro.r.assign("start_clus", starting_clusters)
        ro.r.assign("end_clus", target_clusters)
        ro.r.assign("output_folder", output_folder)

        logger.info("Running Slingshot")

        slingshot_df = ro.r(
            """
if(!is.element("SingleCellExperiment", rownames(installed.packages()))) {{
            if (!requireNamespace("BiocManager", quietly = TRUE))
                install.packages("BiocManager")

            BiocManager::install("SingleCellExperiment", update=FALSE);
        }}
        library(SingleCellExperiment);
        if(!is.element("slingshot", rownames(installed.packages()))) {{
            if (!requireNamespace("BiocManager", quietly = TRUE))
                install.packages("BiocManager")

            BiocManager::install("slingshot", update=FALSE);
        }}
        library(slingshot);
sce_internal <- SingleCellExperiment::SingleCellExperiment(
      assays      = list(counts=data_mat),
      colData     = obs,
      rowData     = var,
      reducedDims = list(projection = projection)
);

sce_internal <- slingshot::slingshot(
  data=sce_internal,
  reducedDim ="projection",
  clusterLabels=cluster_labels,
  start.clus = start_clus,
  end.clus=end_clus,
  maxit = 100,
  approx_points = FALSE
);
df <- colData(sce_internal);

if(do_output){{
png(file=file.path(output_folder, 'trajectory.png'));
plot(reducedDims(sce_internal)$projection, pch=16, asp=1);
lines(SlingshotDataSet(sce_internal), lwd=2, col='black');
dev.off();

png(file=file.path(output_folder, 'lineages.png'));
plot(reducedDims(sce_internal)$projection, pch=16, asp=1);
lines(SlingshotDataSet(sce_internal), lwd=2, type='lineages', col='black');
dev.off();
}}

df <- colData(sce_internal);
df <- df[, startsWith(colnames(df),"slingPseudotime" )];

return(as.data.frame(df))
"""
        )
        slingshot_df = ro.conversion.rpy2py(slingshot_df)
        slingshot_df.index = adata.obs.index

    adata.obs = adata.obs.merge(slingshot_df, left_index=True, right_index=True)
    if "df" in adata.obs.columns.tolist():
        adata.obs = adata.obs.rename({"df": "slingshot_pt"}, axis=1)
    return adata


def scran_adata(
    adata,
    min_mean=0.1,
    plotting=True,
    layer="counts",
    return_layer="data",
    random_state=0,
    dims=None,
    min_size=100,
    clusters=None,
):
    adata = adata.copy()

    logger.info("Converting variables to R")

    data_mat = scipy.sparse.csr_matrix(adata.layers[layer]).T.tocoo().copy()
    logger.info("Seed set to {}".format(random_state))

    if clusters is None:
        logger.info("Dimensions to use for clustering set to {}".format(dims))
    else:
        logger.info("Clusters set to {}".format(set(clusters)))

    with localconverter(template_converter):
        ro.r.assign("data_mat", data_mat)
        ro.r.assign("seed", random_state)
        ro.r.assign("dims", dims)
        ro.r.assign("clusters", clusters)

        logger.info("Running scran size factor estimation")
        size_factors = ro.r(
            """
if(!is.element("SingleCellExperiment", rownames(installed.packages()))) {{
            if (!requireNamespace("BiocManager", quietly = TRUE))
                install.packages("BiocManager")

            BiocManager::install("SingleCellExperiment", update=FALSE);
        }}
        library(SingleCellExperiment);
        if(!is.element("scran", rownames(installed.packages()))) {{
            if (!requireNamespace("BiocManager", quietly = TRUE))
                install.packages("BiocManager")

            BiocManager::install("scran", update=FALSE);
        }}
        library(scran);
    sce = SingleCellExperiment::SingleCellExperiment(list(counts=data_mat));
    if ( is.null(clusters) ){{
    print( paste0("Clustering with ",dims, " Dimensions" ))
    set.seed(seed)
    clusters = quickCluster(sce, method="igraph",d=dims,min.size={size})
    }}else{{
    print("Using the following clusters: ");
    print( unique(unlist(clusters)) );
    clusters <- unlist(clusters)}};
    set.seed(seed);
    sce = scran::computeSumFactors(sce, clusters=clusters, min.mean={mean});
    return(sce$sizeFactor)
    """.format(
                mean=min_mean, size=min_size
            )
        )

    logger.info("Finished successfully!")
    logger.info("Writing to AnnData")
    adata.obs["size_factors"] = size_factors

    if plotting:
        sc.pl.scatter(adata, "size_factors", "total_counts")
        sc.pl.scatter(adata, "size_factors", "n_genes_by_counts")
        sns.distplot(adata.obs["size_factors"], bins=50, kde=False)
        plt.show()

    adata.X = adata.layers[layer].copy()
    adata.X /= adata.obs["size_factors"].values[:, None]
    adata.X = scipy.sparse.csr_matrix(adata.X)
    sc.pp.log1p(adata)
    adata.layers["data"] = adata.X.copy()
    logger.info("Scaling Data")
    sc.pp.scale(adata, max_value=10)
    adata.layers["scale.data"] = adata.X.copy()

    adata.X = adata.layers[return_layer].copy()

    return adata.copy()


def _none2null(none_obj):
    return ro.r("NULL")


def run_scDblFinder(
    adata,
    random_state=42,
    sample_col=None,
    includePCs=10,
    aggregate_features=False,
    iters=3,
    dims=20,
    doublet_rate=None,
    doublet_sd=0.015,
    clusters=None,
    clustCor=None,
    processing="default",
    top_features=1000,
    layer="counts",
    norm_layer="data",
    pca_name="X_pca",
):
    if isinstance(adata, AnnData):
        adata = adata.copy()
    elif isinstance(adata, MuData) and "rna" in adata.mod:
        mdata = adata.copy()
        adata = mdata.mod["rna"].copy()
    else:
        raise TypeError("Expected AnnData or MuData object with 'rna' modality")

    logger.info("Importing Data into R")
    data_mat = scipy.sparse.csr_matrix(adata.layers[layer]).T.tocoo()
    data_mat_norm = scipy.sparse.csr_matrix(adata.layers[norm_layer]).T.tocoo()

    adata.obs = adata.obs.loc[
        :,
        ~adata.obs.columns.isin(
            [
                "scDblFinder.sample",
                "scDblFinder.cluster",
                "scDblFinder.class",
                "scDblFinder.score",
                "scDblFinder.weighted",
                "scDblFinder.difficulty",
                "scDblFinder.cxds_score",
                "scDblFinder.mostLikelyOrigin",
                "scDblFinder.originAmbiguous",
            ]
        ),
    ].copy()

    none_converter = Converter("None converter")
    none_converter.py2rpy.register(type(None), _none2null)

    with localconverter(template_converter):
        ro.r.assign("data_mat", data_mat)
        ro.r.assign("data_mat_norm", data_mat_norm)
        ro.r.assign("obs", adata.obs[[clusters, sample_col]].copy())
        ro.r.assign("var", adata.var)
        ro.r.assign("pca", adata.obsm[pca_name])
        ro.r.assign("seed", random_state)
        ro.r.assign("sample_id", sample_col)
        ro.r.assign("dbr", doublet_rate)
        ro.r.assign("dbr_sd", doublet_sd)
        ro.r.assign("clst", clusters)
        ro.r.assign("clustcor", clustCor)
        ro.r.assign("top_f", top_features)
        ro.r.assign("pcs", includePCs)
        ro.r.assign("n_dims", dims)
        ro.r.assign("iter", iters)
        ro.r.assign("aggregateFeatures", aggregate_features)
        ro.r.assign("processing", processing)

        logger.info("Running scDblFinder")
        scDbl_df = ro.r(
            """
if(!is.element("SingleCellExperiment", rownames(installed.packages()))) {{
            if (!requireNamespace("BiocManager", quietly = TRUE))
                install.packages("BiocManager")

            BiocManager::install("SingleCellExperiment", update=FALSE);
        }}
        library(SingleCellExperiment);
        if(!is.element("scDblFinder", rownames(installed.packages()))) {{
            if (!requireNamespace("BiocManager", quietly = TRUE))
                install.packages("BiocManager")

            BiocManager::install("scDblFinder", update=FALSE);
        }}
        library(scDblFinder);
        if(!is.element("tidyverse", rownames(installed.packages()))) {{
            if (!requireNamespace("BiocManager", quietly = TRUE))
                install.packages("BiocManager")

            BiocManager::install("tidyverse", update=FALSE);
        }};
        library(tidyverse);
        if(!is.element("BiocParallel", rownames(installed.packages()))) {{
            if (!requireNamespace("BiocManager", quietly = TRUE))
                install.packages("BiocManager")

            BiocManager::install("BiocParallel", update=FALSE);
        }};
        library(BiocParallel);
sce <- SingleCellExperiment::SingleCellExperiment(
      assays      = list(counts=data_mat,logcounts=data_mat_norm),
      colData     = obs,
      rowData     = var
);
reducedDims(sce) <- list(PCA=pca);
print(paste0("Using Clusters: ", paste0(unique(colData(sce)[,colnames(colData(sce)) %in% c(clst)]),collapse=",") ));
print(paste0("Setting doublet sd to: ",dbr_sd));
print(paste0("ColData has the following columns: ",paste(colnames(colData(sce)),collapse=",")));
#bp <- MulticoreParam(2, RNGseed=seed);
bp <- SerialParam(RNGseed =seed);
set.seed(seed);
print(paste0("Setting seed to: ",seed));
sce <- scDblFinder(sce, samples=sample_id,
dbr=dbr,
dbr.sd=dbr_sd,
clusters=clst,
clustCor=clustcor,
nfeatures=top_f,
returnType="sce",
includePCs = pcs,
dims=n_dims,
iter=iter,
aggregateFeatures=aggregateFeatures,
processing=processing,
nrounds=NULL,
verbose=TRUE, BPPARAM=bp);
df <- colData(sce)[,colnames(colData(sce)) %in% c('scDblFinder.sample',
       'scDblFinder.cluster', 'scDblFinder.class', 'scDblFinder.score',
       'scDblFinder.weighted', 'scDblFinder.difficulty',
       'scDblFinder.cxds_score', 'scDblFinder.mostLikelyOrigin',
       'scDblFinder.originAmbiguous')];
return(as.data.frame(df))
"""
        )
        scDbl_df = ro.conversion.rpy2py(scDbl_df)
    logger.info("Writing Results to AnnData")
    adata.obs = adata.obs.merge(scDbl_df, left_index=True, right_index=True)

    if clusters is not None:
        logger.info("Determining Doublet Fraction per cluster")
        db_frac = (
            adata.obs.groupby("clusters")["scDblFinder.class"]
            .value_counts()
            .reset_index()
            .pivot(index=clusters, columns="level_1", values="scDblFinder.class")
        )
        db_frac["frac"] = db_frac["doublet"] / (db_frac["doublet"] + db_frac["singlet"])
        db_frac = db_frac.sort_values("frac", ascending=False)
        remove_clus_scdblf = db_frac[db_frac["frac"] >= 0.2].index.tolist()

        adata.obs["doublet_clusters"] = np.where(
            adata.obs[clusters].isin(remove_clus_scdblf), "True", "False"
        )
        adata.obs["doublet_clusters"] = pd.Categorical(
            adata.obs["doublet_clusters"], categories=["True", "False"], ordered=True
        )

    if isinstance(adata, AnnData):
        return adata.copy()
    elif isinstance(mdata, MuData) and "rna" in mdata.mod:
        mdata.mod["rna"] = adata.copy()
        mdata.update()
        return mdata.copy()


def run_deseq2(
    adata,
    sample_key,
    model_key,
    mod="rna",
    layer="counts",
    replicate_key=None,
    one_v_all=False,
    n_pseudoreplicates=2,
    log2_cutoff=0,
    min_counts_per_gene=10,
    min_counts_per_sample=1000,
    post_process=False,
    comp_of_interest=None,
):
    if isinstance(adata, AnnData):
        obs = adata.obs.copy()
    elif isinstance(adata, MuData) and mod in adata.mod:
        adata = adata.mod[mod].copy()
        obs = adata.obs.copy()
    else:
        raise TypeError(
            "Expected AnnData or MuData object with '{}' modality".format(mod)
        )

    logger.info("Checking Replicates")
    obs[sample_key] = pd.Categorical([v for v in obs[sample_key]])
    if replicate_key is None:
        logger.info("Calculating " + str(n_pseudoreplicates) + " Pseudoreplicates")
        replicate_key = "pseudo_replicate"
        obs[replicate_key] = pd.Categorical(
            np.random.choice(
                [str(i) for i in range(1, n_pseudoreplicates + 1)], size=obs.shape[0]
            )
        )

    n_replicates = len(obs[replicate_key].unique())
    annotation_keys = [sample_key, replicate_key, model_key]
    annotation_keys = list(set(annotation_keys))

    obs[replicate_key] = pd.Categorical([v for v in obs[replicate_key]])

    logger.info("Calculate Fraction of cells expressing Gene per provided Groups")
    raw_matrix = pd.DataFrame(
        adata.layers[layer].toarray() > 0,
        columns=adata.var_names.tolist(),
        index=adata.obs_names.tolist(),
    )
    raw_matrix["groups"] = obs[model_key].tolist()
    raw_matrix = raw_matrix.groupby(["groups"])

    fraction_expressed = raw_matrix.sum() / raw_matrix.count()
    fraction_expressed = fraction_expressed.T.reset_index().melt("index")
    fraction_expressed.columns = ["Gene", "Up_In", "frac_expression"]

    n_cells = raw_matrix.count()
    n_cells = n_cells.T.reset_index().melt("index")
    n_cells.columns = ["Gene", "Up_In", "n_cells"]

    fraction_expressed["n_cells"] = n_cells["n_cells"].copy()

    del n_cells

    logger.info("Creating Model Matrix")
    mm = dmatrix(
        f"~0 + {sample_key}:{replicate_key}:{model_key}", obs, return_type="dataframe"
    )
    mm = mm.loc[:, mm.sum(axis=0) > 0]
    expr = adata[mm.index].layers[layer].toarray()
    mat_mm = np.matmul(expr.T, mm)
    mat_mm.index = adata.var.index.copy()

    logger.info("Preparing necessary Sample Annotations")
    data = defaultdict(list)
    for name in mm.columns:
        for col_info in name.split(":"):
            groupby, value = col_info[:-1].split("[")
            data[groupby].append(value)
    sample_annotation = pd.DataFrame(data, index=mm.columns)
    sample_annotation["sample_names"] = sample_annotation.index.tolist()

    additional_annotation = obs.drop_duplicates(annotation_keys)[annotation_keys]

    sample_annotation = sample_annotation.merge(additional_annotation, how="left")
    sample_annotation.index = sample_annotation["sample_names"].tolist()

    if comp_of_interest is None:
        logger.info("Calculating Contrasts of Interest")
        all_combinations = sample_annotation[model_key].tolist()
        all_combinations = [(x, y) for x in all_combinations for y in all_combinations]
        all_combinations = pd.DataFrame(all_combinations, columns=["comp_1", "comp_2"])
        all_combinations = (
            all_combinations.loc[
                all_combinations["comp_1"] != all_combinations["comp_2"], :
            ]
            .drop_duplicates()
            .reset_index(drop=True)
        )
        print(all_combinations)
    else:
        all_combinations = comp_of_interest
        print(all_combinations)

    with localconverter(template_converter):
        ro.r.assign("annotation", sample_annotation)
        ro.r.assign("data", mat_mm)
        ro.r.assign("final_contrasts", all_combinations)
        ro.r.assign("post_process", post_process)

        logger.info("Running DESeq2")

        final_df = ro.r(
            """
    message("Loading Packages");
    if(!is.element("DESeq2", rownames(installed.packages()))) {{
            if (!requireNamespace("BiocManager", quietly = TRUE))
                install.packages("BiocManager")

            BiocManager::install("DESeq2", update=FALSE);
        }}
        library(DESeq2);
        if(!is.element("IHW", rownames(installed.packages()))) {{
            if (!requireNamespace("BiocManager", quietly = TRUE))
                install.packages("BiocManager")

            BiocManager::install("IHW", update=FALSE);
        }}
        library(IHW);
        if(!is.element("tidyverse", rownames(installed.packages()))) {{
            if (!requireNamespace("BiocManager", quietly = TRUE))
                install.packages("BiocManager")

            BiocManager::install("tidyverse", update=FALSE);
        }};
        library(tidyverse);
        if(!is.element("metap", rownames(installed.packages()))) {{
            if (!requireNamespace("BiocManager", quietly = TRUE))
                install.packages("BiocManager")

            BiocManager::install("metap", update=FALSE);
        }};
        library(metap);

        DESeq_VS <- function(cell_type,vs_cell_type,factor_id="{key}"){{
        message(paste0("Calculating: ",factor_id,"_",cell_type,"_vs_",factor_id,"_",vs_cell_type))
        res <- results(dds, contrast=c(factor_id,cell_type,vs_cell_type),filterFun=ihw)
        results_df <- as.data.frame(res) %>% rownames_to_column("Gene")
        results_df$comparison <- paste0(cell_type,"_vs_",vs_cell_type)
        return(results_df)
        }};
        message("Load data into DESeq2 Object");
        dds <- DESeqDataSetFromMatrix(countData = data,
                                    colData = annotation,
                                    design = ~ {key});

        dds <- dds[rowSums(DESeq2::counts(dds) >= {min_counts_per_gene}) >= {n_replicates},
                    colSums(DESeq2::counts(dds)) >= {min_counts_per_sample}];
        message("Calculate Size Factors");
        dds <- estimateSizeFactors(dds);
        dds <- estimateDispersions(dds);
        dds <- nbinomWaldTest(dds, maxit=5000);
        dds <- dds[which(mcols(dds)$betaConv),];

        message("Extract Contrasts of Interest");
        DE_results <-  pmap(list(final_contrasts$comp_1,final_contrasts$comp_2), DESeq_VS );
        combined_DE_results <- bind_rows(DE_results);
        combined_DE_results$padj[is.na(combined_DE_results$padj)] <- 1;

        return(combined_DE_results)
""".format(
                key=model_key,
                min_counts_per_gene=min_counts_per_gene,
                min_counts_per_sample=min_counts_per_sample,
                n_replicates=n_replicates,
            )
        )
        final_df = ro.conversion.rpy2py(final_df)
        logger.info("Done.")

        logger.info("Preparing output")

        final_df[["Up_In", "Down_In"]] = final_df["comparison"].str.extract(
            "^(.*)_vs_(.*)$", expand=True
        )

        final_df = final_df.merge(fraction_expressed, how="left", on=["Up_In", "Gene"])
        fraction_expressed = fraction_expressed.rename(
            {
                "Up_In": "Down_In",
                "n_cells": "Down_In_n_cells",
                "frac_expression": "Down_In_frac_expression",
            },
            axis=1,
        )

        final_df = final_df.merge(
            fraction_expressed, how="left", on=["Down_In", "Gene"]
        )

        if post_process:
            logger.info("Post-Processing")
            processed_df = (
                final_df.groupby(["Up_In", "Gene"])["padj"]
                .apply(lambda x: combine_pvalues(x, method="fisher")[1])
                .reset_index()
                .rename({"padj": "minimum_p_val"}, axis=1)
                .fillna(1.0)
                .copy()
            )

            processed_df["mean_log2FC"] = (
                final_df.groupby(["Up_In", "Gene"])["log2FoldChange"]
                .mean()
                .reset_index()["log2FoldChange"]
                .fillna(0.0)
                .copy()
            )

            processed_df["n_up"] = (
                final_df.groupby(["Up_In", "Gene"])["log2FoldChange"]
                .apply(lambda x: (x >= log2_cutoff).sum())
                .reset_index()["log2FoldChange"]
                .copy()
            )

            processed_df["n_down"] = (
                final_df.groupby(["Up_In", "Gene"])["log2FoldChange"]
                .apply(lambda x: (x < log2_cutoff).sum())
                .reset_index()["log2FoldChange"]
                .copy()
            )

            processed_df["Down_In_n_cells"] = (
                final_df.groupby(["Up_In", "Gene"])["Down_In_n_cells"]
                .sum()
                .reset_index()["Down_In_n_cells"]
                .fillna(0.0)
                .copy()
            )

            processed_df["Down_In_frac_expression"] = (
                final_df.groupby(["Up_In", "Gene"])["Down_In_frac_expression"]
                .max()
                .reset_index()["Down_In_frac_expression"]
                .fillna(0.0)
                .copy()
            )

            final_df = final_df.drop(
                [
                    "Down_In",
                    "Down_In_frac_expression",
                    "Down_In_n_cells",
                    "padj",
                    "log2FoldChange",
                ],
                axis=1,
            ).merge(processed_df)

            final_df = final_df.sort_values(
                ["minimum_p_val", "mean_log2FC"], ascending=[True, False]
            ).drop_duplicates(["Up_In", "Gene", "mean_log2FC", "minimum_p_val"])

        final_df["frac_diff"] = (
            final_df["frac_expression"] - final_df["Down_In_frac_expression"]
        )

        final_df = final_df.fillna({"frac_expression": 0.0})

        logger.info("All Done.")

        return final_df
