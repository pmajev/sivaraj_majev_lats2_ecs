import logging
import math
import random
from collections import defaultdict

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import numpy as np
import pandas as pd
import seaborn as sns
from adjustText import adjust_text
from anndata import AnnData
from matplotlib.font_manager import FontProperties
from matplotlib.gridspec import GridSpec
from matplotlib.patches import Rectangle
from mudata import MuData
from scipy.stats import zscore
from sklearn.cluster import KMeans

from .helpers import castToList, closest_to_centroid, seed_everything

logger = logging.getLogger(__name__)


###
# This function was taken from
# https://stackoverflow.com/questions/7404116/
# defining-the-midpoint-of-a-colormap-in-matplotlib
###
def remappedColorMap(cmap, start=0, midpoint=0.5, stop=1.0, name="shiftedcmap"):
    """
    Function to offset the median value of a colormap, and scale the
    remaining color range. Useful for data with a negative minimum and
    positive maximum where you want the middle of the colormap's dynamic
    range to be at zero.
    Input
    -----
      cmap : The matplotlib colormap to be altered
      start : Offset from lowest point in the colormap's range.
          Defaults to 0.0 (no lower ofset). Should be between
          0.0 and 0.5; if your dataset mean is negative you should leave
          this at 0.0, otherwise to (vmax-abs(vmin))/(2*vmax)
      midpoint : The new center of the colormap. Defaults to
          0.5 (no shift). Should be between 0.0 and 1.0; usually the
          optimal value is abs(vmin)/(vmax+abs(vmin))
      stop : Offset from highets point in the colormap's range.
          Defaults to 1.0 (no upper ofset). Should be between
          0.5 and 1.0; if your dataset mean is positive you should leave
          this at 1.0, otherwise to (abs(vmin)-vmax)/(2*abs(vmin))
    """
    cdict = {"red": [], "green": [], "blue": [], "alpha": []}

    # regular index to compute the colors
    reg_index = np.hstack(
        [np.linspace(start, 0.5, 128, endpoint=False), np.linspace(0.5, stop, 129)]
    )

    # shifted index to match the data
    shift_index = np.hstack(
        [
            np.linspace(0.0, midpoint, 128, endpoint=False),
            np.linspace(midpoint, 1.0, 129),
        ]
    )

    for ri, si in zip(reg_index, shift_index):
        r, g, b, a = cmap(ri)

        cdict["red"].append((si, r, r))
        cdict["green"].append((si, g, g))
        cdict["blue"].append((si, b, b))
        cdict["alpha"].append((si, a, a))

    newcmap = matplotlib.colors.LinearSegmentedColormap(name, cdict)
    plt.register_cmap(cmap=newcmap)

    return newcmap


def plot_libsize_dimred_correlation(adata, dim_red_use="X_lsi", dims_to_use=50):
    dim_red = pd.DataFrame(adata.obsm[dim_red_use][:, :(dims_to_use)])
    dim_red.index = adata.obs_names
    total_counts = adata.obs["total_counts"]
    final_matrix = dim_red
    final_matrix["total_counts"] = total_counts
    correlation = pd.DataFrame(
        dim_red.corrwith(total_counts, axis=0, drop=False, method="pearson")[:-1],
        columns=["Correlation"],
    )
    correlation["PC"] = range(1, dim_red.shape[1], 1)

    sns.scatterplot(data=correlation, x="PC", y="Correlation")
    plt.ylim((-1, 1))


###
# Calculation of the cluster centroids was taken from scanpy
# https://github.com/theislab/scanpy/blob/d072abd05bda07f280ea91f5e7e4a84f9782c118/scanpy/plotting/_tools/scatterplots.py
###


def feature_plot(
    adata,
    features,
    n_rows,
    n_cols,
    cluster_id_col=None,
    color_map_num="ocean",
    color_map_cat="Dark2",
    color_synched=False,
    color_vmax=None,
    color_vmin=None,
    color_dict=None,
    color_nan="#a9a9a9",
    mod="rna",
    point_size=1,
    point_alpha=1,
    point_linewidth=0,
    point_edgecolor=None,
    size_mod_width=6,
    size_mod_height=5,
    color_label="Raw Counts",
    color_format="%.1f",
    dim_red="X_umap",
    dim_name="UMAP",
    dims=[0, 1],
    layer="counts",
    pad_left=0,
    pad_right=1,
    pad_top=1,
    pad_bottom=0,
    share_axes=True,
    x_max=None,
    x_min=None,
    y_max=None,
    y_min=None,
    hide_axis_labels=True,
    draw_grid=True,
    axis_to_hide=["top", "right"],
    legend_on_data=True,
    random_order=True,
    fig=None,
):
    font = FontProperties()
    font.set_family("serif")
    font.set_name("Times New Roman")

    if isinstance(color_map_num, str):
        color_map_num = matplotlib.colormaps[color_map_num]

    color_map_num = matplotlib.cm.get_cmap(color_map_num)

    color_map_num.set_bad(color_nan)

    if isinstance(adata, AnnData):
        input_data = pd.DataFrame(
            adata.obsm[dim_red][:, dims].copy(),
            columns=[dim_name + "_1", dim_name + "_2"],
        )
    elif isinstance(adata, MuData) and mod in adata.mod:
        input_data = pd.DataFrame(
            adata.obsm[dim_red], columns=[dim_name + "_1", dim_name + "_2"]
        )
        adata = adata.mod[mod].copy()
    else:
        raise TypeError("Expected AnnData or MuData object with 'atac' modality")

    # get Dim Red

    # Filter for Features Present in AnnData Object
    initial_features_len = len(features)
    features = [
        feature
        for feature in features
        if (feature in adata.obs.columns) or (feature in adata.var_names)
    ]

    if len(features) < initial_features_len:
        logging.warning("Not all provided features were found in AnnData Object")

    elif len(features) == 0:
        logging.error("No features found in AnnData Object. Terminating...")
        return

    # Add Features to DimRed Data
    for feature in features:
        if feature in adata.obs.columns:
            input_data[feature] = adata.obs[feature].reset_index(drop=True)
        elif feature in adata.var_names:
            input_data[feature] = pd.DataFrame(
                adata.layers[layer][:, adata.var_names == feature].toarray(),
                columns=[feature],
            )[feature]
        else:
            input_data[feature] = "Feature not found"
            input_data[feature] = input_data[feature].astype("category")
    if cluster_id_col is not None:
        input_data[cluster_id_col] = adata.obs[cluster_id_col].reset_index(drop=True)

    # Check Row and Col Number
    if len(features) > n_rows * n_cols:
        logging.warning(
            "Not Enough Rows and Columns provided to fit Features. Determining automatically..."
        )
        n_cols = 2
        n_rows = math.ceil(len(features) / n_cols)

    # Create Figure
    if fig is None:
        fig, axs = plt.subplots(
            n_rows,
            n_cols,
            figsize=(size_mod_width * n_cols, size_mod_height * n_rows),
            sharex=share_axes,
            sharey=share_axes,
            squeeze=False,
            facecolor="white",
            edgecolor="black",
        )
    else:
        axs = fig.axes

    row_id = 0
    col_id = 0

    # If color Limits are provided, set them here
    if color_synched is True and ((color_vmax is None) or (color_vmin is None)):
        logging.info(
            "Instructed to use one color bar for all plots, but limits were not set. Determining from data..."
        )
        color_vmax = np.percentile(input_data.select_dtypes(["number"]), 60)
        if color_vmax <= 0:
            color_vmax = 1
        color_vmin = input_data.select_dtypes(["number"]).to_numpy().min()

    for feature in features:
        data = input_data.copy()
        ax = axs[row_id, col_id]

        # Set Color Palette
        if str(data[feature].dtype) == "category" and color_dict is None:
            color_dict_feature = {}
            unique_members = list(data[feature].unique())
            colors = sns.color_palette(
                color_map_cat, n_colors=len(unique_members)
            ).as_hex()
            for i, member in enumerate(unique_members):
                color_dict_feature[member] = colors[i]

                if data[feature].isnull().values.any():
                    data[feature] = data[feature].cat.add_categories(["not assigned"])
                    data[feature] = data[feature].fillna("not assigned")
                    color_dict_feature["not assigned"] = color_nan

            data["colors"] = data[feature].map(color_dict_feature).tolist()

        elif str(data[feature].dtype) == "category" and color_dict is not None:
            if data[feature].isnull().values.any():
                data[feature] = data[feature].cat.add_categories(["not assigned"])
                data[feature] = data[feature].fillna("not assigned")
                color_dict["not assigned"] = color_nan

            data["colors"] = data[feature].map(color_dict).tolist()
        else:
            data["colors"] = data[feature]

        # Check if Labels should be put on data, calculate centroids if so. Also check if labels should be the same category as used for coloring,
        # or a specified (e.g. clustering) column

        if legend_on_data is True and str(data[feature].dtype) == "category":
            # identify centroids to put labels
            all_pos = (
                data[[dim_name + "_1", dim_name + "_2", feature]]
                .groupby(feature, observed=True)
                .median()
                .sort_index()
            )
            for label, x_pos, y_pos in all_pos.itertuples():
                ax.text(
                    x_pos,
                    y_pos,
                    label,
                    verticalalignment="center",
                    horizontalalignment="center",
                )
        elif legend_on_data is True and cluster_id_col is not None:
            all_pos = (
                data[[dim_name + "_1", dim_name + "_2", cluster_id_col]]
                .groupby(cluster_id_col, observed=True)
                .median()
                .sort_index()
            ).iloc[:, 0:2]
            for label, x_pos, y_pos in all_pos.itertuples():
                ax.text(
                    x_pos,
                    y_pos,
                    label,
                    verticalalignment="center",
                    horizontalalignment="center",
                )

        if random_order:
            seed_everything(seed=42)
            data = data.sample(frac=1).reset_index(drop=True)
        else:
            data = data.sort_values(feature, ascending=True)

        if str(data[feature].dtype) == "category":
            data = data.groupby(feature)
            for name, group in data:
                plot = ax.scatter(
                    data=group,
                    x=dim_name + "_1",
                    y=dim_name + "_2",
                    s=point_size,
                    c=group["colors"],
                    alpha=point_alpha,
                    linewidths=point_linewidth,
                    edgecolors=point_edgecolor,
                    vmax=color_vmax,
                    vmin=color_vmin,
                    label=name,
                    plotnonfinite=True,
                )

                box = ax.get_position()
                ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])

                lgnd = ax.legend(
                    loc="center left",
                    bbox_to_anchor=(1, 0.5),
                    ncol=2,
                    borderaxespad=0.1,
                    frameon=True,
                    mode=None,
                )
                for handle in lgnd.legendHandles:
                    handle.set_sizes([point_size * 10])

        elif color_synched is not True:
            if color_vmax is None:
                color_vmax_feature = np.percentile(data[feature], 95)
                if color_vmax_feature <= 0:
                    color_vmax_feature = 1
            else:
                color_vmax_feature = color_vmax

            plot = ax.scatter(
                data=data,
                x=dim_name + "_1",
                y=dim_name + "_2",
                s=point_size,
                c=data["colors"],
                alpha=point_alpha,
                linewidths=point_linewidth,
                edgecolors=point_edgecolor,
                cmap=color_map_num,
                vmax=color_vmax_feature,
                vmin=color_vmin,
                label="scatter",
                plotnonfinite=True,
            )
            cbar = fig.colorbar(
                plot,
                ax=ax,
                shrink=0.3,
                location="right",
                label=color_label,
                anchor=(0, 0),
                drawedges=False,
                format=color_format,
            )
            cbar.outline.set_edgecolor("black")
            cbar.ax.tick_params(
                color="black",
                direction="inout",
                labelcolor="black",
                length=5,
                size=8,
                which="both",
                axis="y",
            )
            cbar.set_label(color_label, fontsize=8, fontproperties=font, color="black")

        elif color_synched is True:
            plot = ax.scatter(
                data=data,
                x=dim_name + "_1",
                y=dim_name + "_2",
                s=point_size,
                c=data["colors"],
                alpha=point_alpha,
                linewidths=point_linewidth,
                edgecolors=point_edgecolor,
                cmap=color_map_num,
                vmax=color_vmax,
                vmin=color_vmin,
                label="scatter",
                plotnonfinite=True,
            )

        # Set default Style of Figure
        ax.set_title(feature)
        ax.set_xlabel(
            dim_name + " 1", fontsize="large", fontproperties=font, color="black"
        )
        ax.set_ylabel(
            dim_name + " 2", fontsize="large", fontproperties=font, color="black"
        )
        ax.set_facecolor("white")

        ax.set_xlim(left=x_min, right=x_max)
        ax.set_ylim(bottom=y_min, top=y_max)

        if draw_grid:
            ax.grid(alpha=0.3, color="#a6a6a6")
        else:
            ax.grid(False)

        ax.spines[["left", "bottom", "top", "right"]].set_color("black")
        ax.spines[["left", "bottom", "top", "right"]].set_linewidth(1)
        ax.spines[["left", "bottom", "top", "right"]].set_visible(True)
        if axis_to_hide is not None:
            ax.spines[axis_to_hide].set_visible(False)

        if hide_axis_labels:
            ax.xaxis.set_label_coords(0.2, -0.01)
            ax.yaxis.set_label_coords(-0.01, 0.1)
            ax.set_xticklabels([])
            ax.set_yticklabels([])
            ax.set_xticks([])
            ax.set_yticks([])
        else:
            ax.xaxis.set_label_coords(0.1, -0.13)
            ax.yaxis.set_label_coords(-0.13, 0.1)

        ax.set_aspect(1.0 / ax.get_data_ratio())
        # ax.axis("equal")

        if col_id < n_cols - 1:
            col_id += 1
        else:
            row_id += 1
            col_id = 0

    # Fill remaining slots with empty Figures
    while row_id < n_rows and col_id < n_cols:
        axs[row_id, col_id].axis("off")
        if col_id < n_cols - 1:
            col_id += 1
        else:
            row_id += 1
            col_id = 0

    # Draw global color Bar, if requested
    if color_synched is True:
        main_cbar = fig.colorbar(
            plot,
            ax=axs,
            shrink=0.15,
            location="right",
            label=color_label,
            anchor=(0, 1),
            drawedges=False,
            format=color_format,
        )
        main_cbar.outline.set_edgecolor("black")
        main_cbar.ax.tick_params(
            color="black",
            direction="inout",
            labelcolor="black",
            length=5,
            size=8,
            which="both",
            axis="y",
        )
        main_cbar.set_label(color_label, fontsize=8, fontproperties=font, color="black")

    # fig.subplots_adjust(left=pad_left,right=pad_right,top=pad_top,bottom=pad_bottom)
    plt.tight_layout()
    plt.close()

    return fig


def feature_plot_split(
    adata,
    features,
    n_rows,
    n_cols,
    split_by,
    mod="rna",
    cluster_id_col=None,
    color_map_num="ocean",
    color_map_cat="Dark2",
    color_synched=False,
    color_vmax=None,
    color_vmin=None,
    color_dict=None,
    color_nan="#a9a9a9",
    point_size=1,
    point_alpha=1,
    point_linewidth=0,
    point_edgecolor=None,
    size_mod_width=6,
    size_mod_height=5,
    color_label="Raw Counts",
    color_format="%.1f",
    dim_red="X_umap",
    dim_name="UMAP",
    layer="counts",
    pad_left=0,
    pad_right=1,
    pad_top=1,
    pad_bottom=0,
    share_axes=True,
    x_max=None,
    x_min=None,
    y_max=None,
    y_min=None,
    hide_axis_labels=True,
    draw_grid=True,
    remove_legend=False,
    axis_to_hide=["top", "right"],
    legend_on_data=True,
    random_order=True,
    fig=None,
):
    font = FontProperties()
    font.set_family("serif")
    font.set_name("Times New Roman")

    if isinstance(color_map_num, str):
        color_map_num = matplotlib.colormaps[color_map_num]

    color_map_num = matplotlib.cm.get_cmap(color_map_num)

    color_map_num.set_bad(color_nan)

    if isinstance(adata, AnnData):
        input_data = pd.DataFrame(
            adata.obsm[dim_red], columns=[dim_name + "_1", dim_name + "_2"]
        )
    elif isinstance(adata, MuData) and mod in adata.mod:
        input_data = pd.DataFrame(
            adata.obsm[dim_red], columns=[dim_name + "_1", dim_name + "_2"]
        )
        adata = adata.mod[mod].copy()
    else:
        raise TypeError("Expected AnnData or MuData object with 'atac' modality")

    boundary_values = [
        input_data[dim_name + "_1"].min() - 1,
        input_data[dim_name + "_1"].max() + 1,
        input_data[dim_name + "_2"].min() - 1,
        input_data[dim_name + "_2"].max() + 1,
    ]
    features.append(split_by)

    # Filter for Features Present in AnnData Object
    initial_features_len = len(features)
    features = [
        feature
        for feature in features
        if (feature in adata.obs.columns) or (feature in adata.var_names)
    ]

    if len(features) < initial_features_len:
        logging.warning("Not all provided features were found in AnnData Object")

    elif len(features) == 0:
        logging.error("No features found in AnnData Object. Terminating...")
        return

    # Add Features to DimRed Data
    for feature in features:
        if feature in adata.obs.columns:
            input_data[feature] = adata.obs[feature].reset_index(drop=True)
        elif feature in adata.var_names:
            input_data[feature] = pd.DataFrame(
                adata.layers[layer][:, adata.var_names == feature].toarray(),
                columns=[feature],
            )[feature]
        else:
            input_data[feature] = "Feature not found"
            input_data[feature] = input_data[feature].astype("category")
    if cluster_id_col is not None:
        input_data[cluster_id_col] = adata.obs[cluster_id_col].reset_index(drop=True)

    # Get number of splits
    groups = input_data.groupby(split_by, sort=False)
    n_groups = groups.ngroups
    # Check Row and Col Number
    features.remove(split_by)
    if len(features) * n_groups > n_rows * n_cols:
        logging.warning(
            "Not Enough Rows and Columns provided to fit Features. Determining automatically..."
        )
        n_cols = n_groups
        n_rows = math.ceil(len(features) * n_groups / n_cols)

    # Create Figure
    if fig is None:
        fig, axs = plt.subplots(
            n_rows,
            n_cols,
            figsize=(size_mod_width * n_cols, size_mod_height * n_rows),
            sharex=share_axes,
            sharey=share_axes,
            squeeze=False,
            facecolor="white",
            edgecolor="black",
        )
    else:
        axs = fig.axes

    row_id = 0
    col_id = 0

    # If color Limits are provided, set them here
    if color_synched is True and ((color_vmax is None) or (color_vmin is None)):
        logging.info(
            "Instructed to use one color bar for all plots, but limits were not set. Determining from data..."
        )
        color_vmax = np.percentile(input_data.select_dtypes(["number"]), 60)
        if color_vmax <= 0:
            color_vmax = 1
        color_vmin = input_data.select_dtypes(["number"]).to_numpy().min()

    for feature in features:
        for group_name, group in groups:
            data = group
            ax = axs[row_id, col_id]

            # Set Color Palette
            if str(data[feature].dtype) == "category" and color_dict is None:
                color_dict_feature = {}
                unique_members = list(data[feature].unique())
                colors = sns.color_palette(
                    color_map_cat, n_colors=len(unique_members)
                ).as_hex()
                for i, member in enumerate(unique_members):
                    color_dict_feature[member] = colors[i]

                    if data[feature].isnull().values.any():
                        data[feature] = data[feature].cat.add_categories(
                            ["not assigned"]
                        )
                        data[feature] = data[feature].fillna("not assigned")
                        color_dict_feature["not assigned"] = color_nan

                    data["colors"] = data[feature].map(color_dict_feature).tolist()

            elif str(data[feature].dtype) == "category" and color_dict is not None:
                if data[feature].isnull().values.any():
                    data[feature] = data[feature].cat.add_categories(["not assigned"])
                    data[feature] = data[feature].fillna("not assigned")
                    color_dict["not assigned"] = color_nan

                data["colors"] = data[feature].map(color_dict).tolist()
            else:
                data["colors"] = data[feature]

            # Check if Labels should be put on data, calculate centroids if so. Also check if labels should be the same category as used for coloring,
            # or a specified (e.g. clustering) column

            if legend_on_data is True and str(data[feature].dtype) == "category":
                # identify centroids to put labels
                all_pos = (
                    data[[dim_name + "_1", dim_name + "_2", feature]]
                    .groupby(feature, observed=True)
                    .median()
                    .sort_index()
                )
                for label, x_pos, y_pos in all_pos.itertuples():
                    ax.text(
                        x_pos,
                        y_pos,
                        label,
                        verticalalignment="center",
                        horizontalalignment="center",
                    )
            elif legend_on_data is True and cluster_id_col is not None:
                all_pos = (
                    data[[dim_name + "_1", dim_name + "_2", cluster_id_col]]
                    .groupby(cluster_id_col, observed=True)
                    .median()
                    .sort_index()
                ).iloc[:, 0:2]
                for label, x_pos, y_pos in all_pos.itertuples():
                    ax.text(
                        x_pos,
                        y_pos,
                        label,
                        verticalalignment="center",
                        horizontalalignment="center",
                    )

            if random_order:
                seed_everything(seed=42)
                data = data.sample(frac=1).reset_index(drop=True)
            else:
                data = data.sort_values(feature, ascending=True)

            if str(data[feature].dtype) == "category":
                data = data.groupby(feature)
                for name, group in data:
                    plot = ax.scatter(
                        data=group,
                        x=dim_name + "_1",
                        y=dim_name + "_2",
                        s=point_size,
                        c=group["colors"],
                        alpha=point_alpha,
                        linewidths=point_linewidth,
                        edgecolors=point_edgecolor,
                        vmax=color_vmax,
                        vmin=color_vmin,
                        label=name,
                        plotnonfinite=True,
                    )
                    box = ax.get_position()
                    ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])

                    lgnd = ax.legend(
                        loc="center left",
                        bbox_to_anchor=(1, 0.5),
                        ncol=2,
                        borderaxespad=0.1,
                        frameon=True,
                        mode=None,
                    )
                    for handle in lgnd.legendHandles:
                        handle.set_sizes([point_size * 10])

            elif color_synched is not True:
                if color_vmax is None:
                    color_vmax_feature = np.percentile(data[feature], 95)
                    if color_vmax_feature <= 0:
                        color_vmax_feature = 1
                else:
                    color_vmax_feature = color_vmax

                plot = ax.scatter(
                    data=data,
                    x=dim_name + "_1",
                    y=dim_name + "_2",
                    s=point_size,
                    c=data["colors"],
                    alpha=point_alpha,
                    linewidths=point_linewidth,
                    edgecolors=point_edgecolor,
                    cmap=color_map_num,
                    vmax=color_vmax_feature,
                    vmin=color_vmin,
                    label="scatter",
                    plotnonfinite=True,
                )
                cbar = fig.colorbar(
                    plot,
                    ax=ax,
                    shrink=0.3,
                    location="right",
                    label=color_label,
                    anchor=(0, 0),
                    drawedges=False,
                    format=color_format,
                )
                cbar.outline.set_edgecolor("black")
                cbar.ax.tick_params(
                    color="black",
                    direction="inout",
                    labelcolor="black",
                    length=5,
                    size=8,
                    which="both",
                    axis="y",
                )
                cbar.set_label(
                    color_label, fontsize=8, fontproperties=font, color="black"
                )

            elif color_synched is True:
                plot = ax.scatter(
                    data=data,
                    x=dim_name + "_1",
                    y=dim_name + "_2",
                    s=point_size,
                    c=data["colors"],
                    alpha=point_alpha,
                    linewidths=point_linewidth,
                    edgecolors=point_edgecolor,
                    cmap=color_map_num,
                    vmax=color_vmax,
                    vmin=color_vmin,
                    label="scatter",
                    plotnonfinite=True,
                )

            # Set default Style of Figure
            ax.set_title(group_name + " - " + feature)
            ax.set_xlabel(
                dim_name + " 1", fontsize="large", fontproperties=font, color="black"
            )
            ax.set_ylabel(
                dim_name + " 2", fontsize="large", fontproperties=font, color="black"
            )
            ax.set_facecolor("white")

            ax.set_xlim(left=x_min, right=x_max)
            ax.set_ylim(bottom=y_min, top=y_max)

            if draw_grid:
                ax.grid(alpha=0.3, color="#a6a6a6")
            else:
                ax.grid(False)
            ax.set_xlim(left=boundary_values[0], right=boundary_values[1])
            ax.set_ylim(bottom=boundary_values[2], top=boundary_values[3])
            ax.spines[["left", "bottom", "top", "right"]].set_color("black")
            ax.spines[["left", "bottom", "top", "right"]].set_linewidth(1)
            ax.spines[["left", "bottom", "top", "right"]].set_visible(True)
            if axis_to_hide is not None:
                ax.spines[axis_to_hide].set_visible(False)

            if hide_axis_labels:
                ax.xaxis.set_label_coords(0.2, -0.01)
                ax.yaxis.set_label_coords(-0.01, 0.1)
                ax.set_xticklabels([])
                ax.set_yticklabels([])
                ax.set_xticks([])
                ax.set_yticks([])
            else:
                ax.xaxis.set_label_coords(0.1, -0.13)
                ax.yaxis.set_label_coords(-0.13, 0.1)

            ax.set_aspect(1.0 / ax.get_data_ratio())

            if remove_legend:
                ax.get_legend().remove()

            # ax.axis("equal")

            if col_id < n_cols - 1:
                col_id += 1
            else:
                row_id += 1
                col_id = 0

    # Fill remaining slots with empty Figures
    while row_id < n_rows and col_id < n_cols:
        axs[row_id, col_id].axis("off")
        if col_id < n_cols - 1:
            col_id += 1
        else:
            row_id += 1
            col_id = 0

    # Draw global color Bar, if requested
    if color_synched is True:
        main_cbar = fig.colorbar(
            plot,
            ax=axs,
            shrink=0.15,
            location="right",
            label=color_label,
            anchor=(0, 1),
            drawedges=False,
            format=color_format,
        )
        main_cbar.outline.set_edgecolor("black")
        main_cbar.ax.tick_params(
            color="black",
            direction="inout",
            labelcolor="black",
            length=5,
            size=8,
            which="both",
            axis="y",
        )
        main_cbar.set_label(color_label, fontsize=8, fontproperties=font, color="black")

    # fig.subplots_adjust(left=pad_left,right=pad_right,top=pad_top,bottom=pad_bottom)
    plt.tight_layout()
    plt.close()

    return fig


def legend_without_duplicate_labels(ax):
    handles, labels = ax.get_legend_handles_labels()
    unique = [
        (h, l) for i, (h, l) in enumerate(zip(handles, labels)) if l not in labels[:i]
    ]
    ax.legend(
        *zip(*unique),
        ncol=1,
        bbox_to_anchor=(1.05, 1),
        loc=0,
        borderaxespad=0.0,
        facecolor="white",
        fontsize="small"
    )


def calculate_cell_proportions(
    adata,
    cluster_col,
    sample_col,
    fig_height=10,
    fig_width=5,
    color_map_sample="Dark2",
    color_map_cluster="Dark2",
    color_dict_sample=None,
    color_dict_cluster=None,
    pad_left=1,
    pad_right=0.5,
    pad_top=1,
    pad_bottom=0.1,
    fig=None,
):
    data = adata.obs.copy()
    frac_per_sample = data.copy()
    frac_per_sample["count"] = data.groupby([cluster_col, sample_col])[
        sample_col
    ].transform("count")
    frac_per_sample = frac_per_sample.drop_duplicates(
        [cluster_col, sample_col, "count"]
    )
    frac_per_sample["sum"] = frac_per_sample.groupby([cluster_col])["count"].transform(
        "sum"
    )
    frac_per_sample = frac_per_sample[
        [cluster_col, sample_col, "count", "sum"]
    ].reset_index(drop=True)

    frac_per_sample["frac"] = frac_per_sample["count"] / frac_per_sample["sum"]
    frac_per_sample["pct"] = frac_per_sample["frac"] * 100
    frac_per_sample = frac_per_sample.sort_values([sample_col, cluster_col])

    if color_dict_sample is None:
        color_dict_sample = {}
        unique_members = list(frac_per_sample[sample_col].unique())
        colors = sns.color_palette(
            color_map_sample, n_colors=len(unique_members)
        ).as_hex()
        for i, member in enumerate(unique_members):
            color_dict_sample[member] = colors[i]
        frac_per_sample["colors"] = (
            frac_per_sample[sample_col].map(color_dict_sample).tolist()
        )

    else:
        frac_per_sample["colors"] = (
            frac_per_sample[sample_col].map(color_dict_sample).tolist()
        )

    frac_per_cluster = data.copy()
    frac_per_cluster["count"] = data.groupby([sample_col, cluster_col])[
        cluster_col
    ].transform("count")
    frac_per_cluster = frac_per_cluster.drop_duplicates(
        [sample_col, cluster_col, "count"]
    )
    frac_per_cluster["sum"] = frac_per_cluster.groupby([sample_col])["count"].transform(
        "sum"
    )
    frac_per_cluster = frac_per_cluster[
        [sample_col, cluster_col, "count", "sum"]
    ].reset_index(drop=True)

    frac_per_cluster["frac"] = frac_per_cluster["count"] / frac_per_cluster["sum"]
    frac_per_cluster["pct"] = frac_per_cluster["frac"] * 100
    frac_per_cluster = frac_per_cluster.sort_values([sample_col, cluster_col])

    if color_dict_cluster is None:
        color_dict_cluster = {}
        unique_members = list(frac_per_cluster[cluster_col].unique())
        colors = sns.color_palette(
            color_map_cluster, n_colors=len(unique_members)
        ).as_hex()
        for i, member in enumerate(unique_members):
            color_dict_cluster[member] = colors[i]
        frac_per_cluster["colors"] = (
            frac_per_cluster[cluster_col].map(color_dict_cluster).tolist()
        )

    else:
        frac_per_cluster["colors"] = (
            frac_per_cluster[cluster_col].map(color_dict_cluster).tolist()
        )

    color_dict_cluster = {}
    unique_members = list(frac_per_cluster[cluster_col].unique())
    colors = sns.color_palette(color_map_cluster, n_colors=len(unique_members)).as_hex()
    for i, member in enumerate(unique_members):
        color_dict_cluster[member] = colors[i]

    if fig is None:
        fig, axs = plt.subplots(
            1,
            2,
            figsize=(fig_width * 2, fig_height),
            squeeze=False,
            facecolor="white",
            edgecolor="black",
        )
    else:
        axs = fig.axes

    fig.subplots_adjust(left=pad_left, right=pad_right, top=pad_top, bottom=pad_bottom)

    i = 0
    cluster_sum = defaultdict(int)

    for row in frac_per_cluster.iterrows():
        ax = axs[0, i]
        x, cluster, count, sums, frac, pct, colors = row[1]
        ax.bar(x, pct, width=1, label=cluster, bottom=cluster_sum[x], color=colors)
        cluster_sum[x] += pct
    ax.spines[["left", "bottom"]].set_color("black")
    ax.spines[["left", "bottom"]].set_linewidth(1)
    ax.spines[["top", "right"]].set_visible(False)
    ax.set_xlabel("Sample Name", fontsize="large", color="black")
    ax.set_ylabel("Percentage", fontsize="large", color="black")
    ax.grid(False)
    ax.spines[["left", "bottom", "top", "right"]].set_color("black")
    ax.spines[["left", "bottom", "top", "right"]].set_linewidth(1)
    ax.spines[["left", "bottom", "top", "right"]].set_visible(True)
    ax.set_facecolor("white")
    ax.margins(x=0)
    ax.margins(y=0)

    legend_without_duplicate_labels(ax)
    plt.setp(ax.get_xticklabels(), rotation=90, ha="right", rotation_mode="anchor")

    i += 1

    for row in frac_per_sample.iterrows():
        ax = axs[0, i]
        x, cluster, count, sums, frac, pct, colors = row[1]
        ax.bar(x, pct, width=1, label=cluster, bottom=cluster_sum[x], color=colors)
        cluster_sum[x] += pct
    ax.spines[["left", "bottom"]].set_color("black")
    ax.spines[["left", "bottom"]].set_linewidth(1)
    ax.spines[["top", "right"]].set_visible(False)
    ax.set_xlabel("Cluster Name", fontsize="large", color="black")
    ax.set_ylabel("Percentage", fontsize="large", color="black")
    ax.grid(False)
    ax.set_facecolor("white")
    ax.spines[["left", "bottom", "top", "right"]].set_color("black")
    ax.spines[["left", "bottom", "top", "right"]].set_linewidth(1)
    ax.spines[["left", "bottom", "top", "right"]].set_visible(True)
    ax.margins(x=0)
    ax.margins(y=0)

    legend_without_duplicate_labels(ax)
    plt.tight_layout()
    plt.setp(ax.get_xticklabels(), rotation=90, ha="right", rotation_mode="anchor")

    plt.close()
    return [fig, frac_per_cluster, frac_per_sample]


def simple_heatmap(
    adata,
    features,
    id_col,
    color_map="viridis",
    color_format="%.1f",
    colors_dict=None,
    do_zscore=True,
    layer="data",
    interpolation="nearest",
    fig=None,
):
    initial_features_len = len(features)
    features = [
        feature
        for feature in features
        if (feature in adata.obs.columns) or (feature in adata.var_names)
    ]
    input_data = adata.obs[id_col].to_frame()

    if len(features) < initial_features_len:
        logging.warning("Not all provided features were found in AnnData Object")

    elif len(features) == 0:
        logging.error("No features found in AnnData Object. Terminating...")

    # Add Features to DimRed Data
    for feature in features:
        input_data[feature] = pd.DataFrame(
            adata.layers[layer][:, adata.var_names == feature].toarray(),
            columns=[feature],
            index=adata.obs.index,
        )[feature]

    data_frame = input_data.groupby(id_col).mean().T

    matrix = data_frame.to_numpy()

    if do_zscore is True:
        matrix = zscore(matrix, axis=1, ddof=0, nan_policy="propagate")

    genes = data_frame.index.tolist()
    cell_types = data_frame.columns.tolist()

    if colors_dict is None:
        colors_dict = {}
        colors = sns.color_palette(color_map, n_colors=len(cell_types)).as_hex()
        for i, member in enumerate(cell_types):
            colors_dict[member] = colors[i]

    cell_type_colors = data_frame.columns.map(colors_dict).tolist()
    colors_df = pd.DataFrame({"cell_type": cell_types, "colors": cell_type_colors})

    if fig is None:
        fig, ax = plt.subplots(
            1,
            1,
            figsize=(8, 5 * (1 + (len(features) / 30))),
            squeeze=True,
            facecolor="white",
            edgecolor="black",
        )
    else:
        ax = fig.axes

    fig.subplots_adjust()
    ax.grid(False)

    im = ax.imshow(
        matrix,
        cmap=color_map,
        vmax=2,
        vmin=-2,
        aspect="auto",
        interpolation=interpolation,
    )
    ax.spines[:].set_color("black")
    ax.spines[:].set_visible(True)
    ax.set_facecolor("white")
    ax.tick_params(axis="both", which="both", length=0)
    ax.set_xticks(np.arange(len(cell_types)), labels=cell_types)
    ax.set_yticks(np.arange(len(genes)), labels=genes)
    ax.tick_params(top=True, bottom=False, labeltop=True, labelbottom=False, pad=15)
    plt.setp(ax.get_xticklabels(), rotation=-90, ha="right", rotation_mode="anchor")

    main_cbar = fig.colorbar(
        im,
        ax=ax,
        shrink=0.15,
        location="right",
        label="Z-Score",
        anchor=(0, 1),
        drawedges=False,
        format=color_format,
    )
    main_cbar.outline.set_edgecolor("black")
    main_cbar.ax.tick_params(
        color="black",
        direction="inout",
        labelcolor="black",
        length=5,
        size=8,
        which="both",
        axis="y",
    )
    main_cbar.set_label("Z-Score", fontsize=8, color="black")

    width_pos = -0.5
    for i, row in colors_df.iterrows():
        ax.add_patch(
            Rectangle(
                (width_pos, -0.9),
                1,
                0.4,
                facecolor=row.colors,
                clip_on=False,
                linewidth=0,
            )
        )
        width_pos += 1
    plt.tight_layout()
    plt.close()
    return fig


def simple_violin(
    adata,
    features,
    id_col,
    id_order=None,
    split_col=None,
    split_order=None,
    color_map="Dark2",
    color_format="%.1f",
    colors_dict=None,
    layer="daata",
    fig=None,
    point_size=2,
):
    initial_features_len = len(features)
    features = [
        feature
        for feature in features
        if (feature in adata.obs.columns) or (feature in adata.var_names)
    ]
    if split_col is None:
        input_data = adata.obs[id_col].copy().to_frame()
        split = False

    else:
        input_data = adata.obs[[id_col, split_col]].copy()
        split = True
        group_types = input_data[split_col].copy().tolist()
        if split_order is not None:
            input_data[split_col] = pd.Categorical(
                input_data[split_col], categories=split_order, ordered=True
            )
            input_data = input_data.sort_values(split_col)

    if id_order is not None:
        input_data[id_col] = pd.Categorical(
            input_data[id_col], categories=id_order, ordered=True
        )
        input_data = input_data.sort_values(id_col)

    if len(features) < initial_features_len:
        logging.warning("Not all provided features were found in AnnData Object")

    elif len(features) == 0:
        logging.error("No features found in AnnData Object. Terminating...")

    # Add Features to DimRed Data
    for feature in features:
        input_data[feature] = pd.DataFrame(
            adata.layers[layer][:, adata.var_names == feature].toarray(),
            columns=[feature],
            index=adata.obs.index,
        )[feature].copy()

    cell_types = input_data[id_col].copy().tolist()

    if colors_dict is None and split_col is None:
        colors_dict = {}
        colors = sns.color_palette(color_map, n_colors=len(cell_types)).as_hex()
        for i, member in enumerate(cell_types):
            colors_dict[member] = colors[i]
    elif colors_dict is None:
        colors_dict = {}
        colors = sns.color_palette(color_map, n_colors=len(group_types)).as_hex()
        for i, member in enumerate(group_types):
            colors_dict[member] = colors[i]

    input_data["color"] = input_data[id_col].map(colors_dict)
    # return (input_data,features)
    if fig is None:
        fig, axs = plt.subplots(
            len(features),
            1,
            figsize=(10, 10 * (1 + (len(features) / 2))),
            squeeze=True,
            facecolor="white",
            edgecolor="black",
        )
    else:
        axs = fig.axes

    fig.subplots_adjust()

    for i, feature in enumerate(features):
        ax = axs[i]
        sns.stripplot(
            x=id_col,
            y=feature,
            hue=split_col,
            dodge=split,
            data=input_data,
            color="black",
            edgecolor="gray",
            ax=ax,
            jitter=0.3,
            alpha=1,
            size=point_size,
            label="_nolegend_",
        )
        sns.violinplot(
            y=feature,
            hue=split_col,
            data=input_data,
            bw="scott",
            cut=0,
            scale="width",
            gridsize=100,
            x=id_col,
            palette=colors_dict,
            width=0.8,
            inner=None,
            split=split,
            orient=None,
            linewidth=0.1,
            saturation=0.75,
            ax=ax,
        )

        ax.spines[:].set_color("black")
        ax.spines[:].set_visible(True)
        ax.grid(False)
        ax.set_facecolor("white")
        ax.tick_params(axis="x", which="both", length=0)
        plt.setp(
            ax.get_xticklabels(), rotation=90, ha="center", rotation_mode="default"
        )
        ax.set_ylabel(
            "Normalized " + feature + " Expression", fontsize="large", color="black"
        )
        if split_col is not None:
            handles, labels = ax.get_legend_handles_labels()
            ax.legend(
                handles=handles[len(colors_dict) :], labels=labels[len(colors_dict) :]
            )

    plt.tight_layout()
    plt.close()
    return fig


def simple_volcano_plot(
    data,
    annotation_col,
    fc_col="log2FoldChange",
    fc_cutoff=2,
    fc_label="log2(FoldChange)",
    sig_col="padj",
    sig_cutoff=0.001,
    sig_label="-log10(pval)",
    n_top=10,
    point_size=2,
    point_alpha=1,
    label_size=10,
    x_max=None,
    x_min=None,
    y_max=None,
    y_min=None,
    sig_color="#104e8b",
    nonsig_color="#818479",
    which_label=None,
    fig=None,
):
    top_n = data
    top_n["Direction"] = np.where(top_n[fc_col] > 0, "Up", "Down")
    top_n = top_n[(top_n[fc_col] >= fc_cutoff) & (top_n[sig_col] >= sig_cutoff)]

    top_n = (
        data.sort_values(by=[sig_col, fc_col], ascending=[True, False], axis=0)
        .groupby("Direction")
        .head(n_top)
        .index.tolist()
    )

    bottom_n = (
        data.sort_values(by=[sig_col, fc_col], ascending=[True, True], axis=0)
        .groupby("Direction")
        .head(n_top)
        .index.tolist()
    )

    data["colors"] = np.where(
        (abs(data[fc_col]) >= fc_cutoff) & (data[sig_col] <= sig_cutoff),
        sig_color,
        nonsig_color,
    )

    if which_label is None:
        data_subset = data.loc[
            (data.index.isin(top_n)) | (data.index.isin(bottom_n)), :
        ]
    else:
        data_subset = data.loc[data[annotation_col].isin(which_label)]

    if fig is None:
        fig, ax = plt.subplots(
            1,
            1,
            figsize=(5, 5),
            squeeze=True,
            facecolor="white",
            edgecolor="black",
        )
    else:
        ax = fig.axes

    ax.scatter(
        data=data,
        x=fc_col,
        y=sig_col,
        s=point_size,
        c=data["colors"],
        alpha=point_alpha,
        linewidths=0,
        edgecolors=None,
    )
    ax.set_yscale("log", base=10)
    ax.invert_yaxis()
    ax.spines[["left", "bottom", "top", "right"]].set_color("black")
    ax.spines[["left", "bottom", "top", "right"]].set_linewidth(1)
    ax.spines[["left", "bottom", "top", "right"]].set_visible(True)

    ax.set_xlim([x_min, x_max])
    ax.set_ylim([y_min, y_max])

    ax.set_xlabel(fc_label, color="black")
    ax.set_ylabel(sig_label, color="black")

    # ax.xaxis.set_label_coords(0.1, -0.13)
    # ax.yaxis.set_label_coords(-0.13, 0.1)

    ax.set_aspect(1.0 / ax.get_data_ratio())
    ax.grid(alpha=0.05, color="#a6a6a6")

    ax.axvline(fc_cutoff, color="grey", linestyle="--", alpha=0.5, linewidth=0.5)
    ax.axvline(-fc_cutoff, color="grey", linestyle="--", alpha=0.5, linewidth=0.5)
    ax.axhline(sig_cutoff, color="grey", linestyle="--", alpha=0.5, linewidth=0.5)

    plt.tight_layout()
    texts = [
        ax.text(
            data_subset[fc_col].iloc[i],
            data_subset[sig_col].iloc[i],
            data_subset[annotation_col].iloc[i],
            color="#000000",
            fontsize=label_size,
        )
        for i in range(0, data_subset.shape[0])
    ]
    adjust_text(texts, arrowprops=dict(arrowstyle="-", color="#000000", lw=0.1))

    plt.close()
    return fig


def add_scale(ax, offset=0):
    # add extra axes for the scale
    rect = ax.get_position()
    rect = (
        rect.xmin - offset,
        rect.ymin + rect.height / 2,  # x, y
        rect.width,
        rect.height / 2,
    )  # width, height
    scale_ax = ax.figure.add_axes(rect)
    # hide most elements of the new axes
    for loc in ["right", "top", "bottom"]:
        scale_ax.spines[loc].set_visible(False)
    scale_ax.tick_params(bottom=False, labelbottom=False)
    scale_ax.patch.set_visible(False)  # hide white background
    # adjust the scale
    scale_ax.spines["left"].set_bounds(*ax.get_ylim())
    scale_ax.set_yticks(ax.get_yticks())
    scale_ax.set_ylim(ax.get_rorigin(), ax.get_rmax())


def jitter(x, spread=0.6):
    # random.seed(42)
    return x + random.uniform(0, spread) - (spread / 2)


def realign_polar_xticks(ax):
    for theta, label in zip(ax.get_xticks(), ax.get_xticklabels()):
        theta = theta * ax.get_theta_direction() + ax.get_theta_offset()
        theta = np.pi / 2 - theta
        y, x = np.cos(theta), np.sin(theta)
        if x >= 0.1:
            label.set_horizontalalignment("left")
        if x <= -0.1:
            label.set_horizontalalignment("right")
        if y >= 0.5:
            label.set_verticalalignment("bottom")
        if y <= -0.5:
            label.set_verticalalignment("top")


def rade_plot(
    input_df,
    point_size=5,
    point_alpha=1,
    max_y=3,
    jitter_spread=0.9,
    color_map="viridis",
    max_log10_pval=5,
    log_cutoff_label=2.5,
    random_order=True,
    n_top=10,
    label_fc_cutoff=0,
    label_sig_cutoff=0.05,
    which_label=None,
    label_size=10,
    sig_col="padj",
    fc_col="log2FoldChange",
    log_10=True,
    fig_size=10,
    fig=None,
):
    input_df = input_df.copy()
    unique_splits = list(set(input_df["Up_In"].tolist()))
    unique_ids = list(set(input_df["ID"].tolist()))
    step_size = round(360 / len(unique_ids))

    ids = {k: math.radians(v * step_size) for v, k in enumerate(unique_ids)}
    input_df["num_id"] = input_df["ID"].apply(lambda x: ids[x])
    input_df["jitter"] = input_df["num_id"].apply(
        lambda x: jitter(x, spread=jitter_spread)
    )

    if log_10:
        input_df[sig_col] = np.log10(input_df[sig_col]).copy()
        input_df.loc[(input_df[sig_col] == -0.0), sig_col] = +0

        highest_value = np.max(
            input_df[sig_col].to_numpy()[np.nonzero(input_df[sig_col].to_numpy())]
        )

        input_df[sig_col] = -np.nan_to_num(
            input_df[sig_col], copy=True, nan=0, posinf=highest_value, neginf=0
        )

        input_df[sig_col] = np.where(input_df[sig_col] == -0, +0, input_df[sig_col])

    if random_order:
        seed_everything(seed=42)
        input_df = input_df.sample(frac=1).reset_index(drop=True)
    else:
        input_df = input_df.sort_values(sig_col, ascending=True)

    if fig is None:
        fig, axs = plt.subplots(
            1,
            len(unique_splits),
            figsize=(fig_size, fig_size),
            squeeze=True,
            sharey=True,
            sharex=True,
            facecolor="white",
            edgecolor="#04253a",
            subplot_kw={"projection": "polar"},
            gridspec_kw={"wspace": 0.5, "right": 1},
        )
    else:
        axs = fig.axes

    for i, split in enumerate(unique_splits):
        if len(unique_splits) > 1:
            ax = axs[i]
        else:
            ax = axs
        data = input_df[input_df["Up_In"] == split].copy()

        top_n = (
            data.loc[
                (data[fc_col] >= label_fc_cutoff) & (data[sig_col] <= label_sig_cutoff),
                :,
            ]
            .sort_values(by=[fc_col, sig_col], ascending=[False, True], axis=0)
            .groupby("ID")
            .head(n_top)
            .index.tolist()
        )

        if which_label is None:
            data_subset = data.loc[data.index.isin(top_n), :]
        else:
            data_subset = data.loc[data["Gene"].isin(which_label)]

        ax.set_title(split, pad=25)
        plot = ax.scatter(
            data=data,
            x="jitter",
            y=fc_col,
            s=point_size,
            c=sig_col,
            cmap=color_map,
            alpha=point_alpha,
            linewidths=0,
            edgecolors=None,
            vmax=max_log10_pval,
        )

        tick_pos = [x + math.radians(step_size / 2) for x in ids.values()]
        label_pos = [x for x in ids.values()]

        ax.set_xticks(tick_pos)
        ax.set_rticks(range(0, max_y, 1))
        ax.set_rmax(max_y + 1)
        ax.set_rlim(bottom=0, top=max_y)
        ax.xaxis.set_major_formatter(ticker.NullFormatter())
        ax.xaxis.set_minor_locator(ticker.FixedLocator(label_pos))
        ax.xaxis.set_minor_formatter(ticker.FixedFormatter(list(ids.keys())))
        ax.xaxis.set_tick_params(pad=20, which="minor")
        ax.set_yticklabels([])
        ax.set_aspect(1.0 / ax.get_data_ratio())

        texts = [
            ax.text(
                data_subset["jitter"].iloc[i],
                data_subset[fc_col].iloc[i],
                data_subset["Gene"].iloc[i],
                color="#000000",
                fontsize=label_size,
            )
            for i in range(0, data_subset.shape[0])
            if data_subset[fc_col].iloc[i] > log_cutoff_label
        ]

        adjust_text(
            texts,
            x=data_subset["jitter"].tolist(),
            y=data_subset[fc_col].tolist(),
            ax=ax,
            expand_text=(1.5, 1.5),
            force_points=(1, 1),
            force_text=(1, 1),
            expand_points=(1.5, 1.5),
            precision=0.001,
            lim=10000000,
            expand_align=(1.5, 1.5),
            autoalign="xy",
            arrowprops=dict(arrowstyle="-", color="black", lw=1),
        )

    main_cbar = fig.colorbar(
        plot,
        ax=axs,
        shrink=0.15,
        location="right",
        label="-log10(p.adj.)",
        anchor=(0, 0.3),
        drawedges=False,
    )

    main_cbar.outline.set_edgecolor("black")
    main_cbar.ax.tick_params(
        color="black",
        direction="inout",
        labelcolor="black",
        length=5,
        size=8,
        which="both",
        axis="y",
    )
    main_cbar.set_label("-log10(p.adj.)", fontsize=8, color="black")
    plt.margins(x=0)
    plt.tight_layout()
    plt.close()
    return fig


def expression_heatmap(
    adata,
    features,
    id_col,
    group_col,
    id_order=None,
    vmax=3,
    vmin=-3,
    groups_order=None,
    do_zscore=True,
    layer="data",
    bar_height=0.45,
    bar_padding=-2,
    x_pad=5,
    y_pad=5,
    fig_width=8,
    fig_height=None,
    rotate_xlabels=-90,
    colors_dict=None,
    color_map="viridis",
    interpolation=None,
    label_pos="right",
    color_format="%.1f",
    colorbar_label="Z-Score",
    cbar_shrink=0.15,
    fig=None,
):
    initial_features_len = len(features)
    features = [
        feature
        for feature in features
        if (feature in adata.obs.columns) or (feature in adata.var_names)
    ]

    if group_col is None:
        input_data = adata.obs[id_col].to_frame()
    else:
        input_data = adata.obs[[id_col, group_col]].copy()

    if len(features) < initial_features_len:
        logging.warning("Not all provided features were found in AnnData Object")

    elif len(features) == 0:
        logging.error("No features found in AnnData Object. Terminating...")

    # Add Features to DimRed Data
    for feature in features:
        input_data[feature] = pd.DataFrame(
            adata.layers[layer][:, adata.var_names == feature].toarray(),
            columns=[feature],
            index=adata.obs.index,
        )[feature]

    if group_col is None:
        data_frame = input_data.groupby(id_col).mean().T
    else:
        data_frame = input_data.groupby([group_col, id_col]).mean().T

    if do_zscore is True:
        matrix = data_frame.to_numpy()
        matrix = zscore(matrix, axis=1, ddof=0, nan_policy="propagate")
        data_frame = pd.DataFrame(
            matrix, index=data_frame.index, columns=data_frame.columns
        )

    genes = data_frame.index.tolist()

    if id_order is None:
        unique_ids = list(set([x for x in data_frame.columns.tolist()]))
    else:
        unique_ids = id_order

    if groups_order is None and group_col is not None:
        unique_groups = list(set(adata.obs[group_col].tolist()))
    elif groups_order is not None:
        unique_groups = groups_order
    else:
        unique_groups = unique_ids

    if group_col is None:
        structure_df = pd.DataFrame({"ID": unique_ids, "GROUP": unique_ids})
    else:
        structure_df = pd.DataFrame(
            [(x, y) for y in unique_ids for x in unique_groups], columns=["ID", "GROUP"]
        )

    if colors_dict is None:
        colors_dict = {}
        colors = sns.color_palette(color_map, n_colors=len(unique_ids)).as_hex()
        for i, member in enumerate(unique_ids):
            colors_dict[member] = colors[i]

    structure_df["colors"] = structure_df["GROUP"].map(colors_dict).tolist()

    data_frame = data_frame.reindex(unique_ids, axis=1, level=0)

    if group_col is not None:
        data_frame = data_frame.reindex(unique_groups, axis=1, level=1)

    if fig_height is None:
        fig_height = 5 * (1 + (len(features) / 30))

    if fig is None:
        fig, ax = plt.subplots(
            1,
            1,
            figsize=(fig_width, fig_height),
            squeeze=True,
            facecolor="white",
            edgecolor="black",
        )
    else:
        ax = fig.axes

    fig.subplots_adjust()
    ax.grid(False)

    im = ax.imshow(
        data_frame,
        cmap=color_map,
        vmax=vmax,
        vmin=vmin,
        aspect="auto",
        interpolation=interpolation,
    )

    ax.spines[:].set_color("black")
    ax.spines[:].set_visible(True)
    ax.set_facecolor("white")
    ax.tick_params(axis="both", which="both", length=0)

    if group_col is None:
        ax.set_xticks(
            np.arange(len(unique_ids)),
            labels=unique_ids,
            rotation=rotate_xlabels,
            ha="center",
        )
    else:
        ax.set_xticks(
            np.arange(len(structure_df)),
            labels=structure_df["ID"].tolist(),
            rotation=rotate_xlabels,
            ha="center",
        )

    ax.set_yticks(np.arange(len(genes)), labels=genes)

    if label_pos == "right":
        ax.tick_params(
            top=True,
            bottom=False,
            labeltop=True,
            labelbottom=False,
            labelright=True,
            labelleft=False,
        )
        cbar_pos = "left"
    else:
        ax.tick_params(
            top=True,
            bottom=False,
            labeltop=True,
            labelbottom=False,
            labelright=False,
            labelleft=True,
        )
        cbar_pos = "right"

    ax.tick_params(axis="x", pad=x_pad)
    ax.tick_params(axis="y", pad=y_pad)

    main_cbar = fig.colorbar(
        im,
        ax=ax,
        shrink=cbar_shrink,
        location=cbar_pos,
        label="Z-Score",
        anchor=(1, 1),
        drawedges=False,
        format=color_format,
    )
    main_cbar.outline.set_edgecolor("black")
    main_cbar.ax.tick_params(
        color="black",
        direction="inout",
        labelcolor="black",
        length=5,
        size=8,
        which="both",
        axis="y",
    )
    main_cbar.set_label(colorbar_label, fontsize=8, color="black")

    width_pos = -0.5
    for i, row in structure_df.iterrows():
        ax.add_patch(
            Rectangle(
                (width_pos, bar_padding * bar_height),
                width=1,
                height=bar_height,
                facecolor=row.colors,
                clip_on=False,
                linewidth=0,
            )
        )
        width_pos += 1
    plt.tight_layout()
    plt.close()
    return fig


def expression_dotplot(
    adata,
    features,
    id_col,
    group_col,
    id_order=None,
    color_vmax=3,
    color_vmin=-3,
    groups_order=None,
    do_zscore=True,
    layer="data",
    bar_height=0.2,
    bar_padding=3.5,
    x_pad=5,
    y_pad=5,
    fig_width=8,
    fig_height=None,
    rotate_xlabels=-90,
    colors_dict=None,
    color_map="viridis",
    point_linewidth=0,
    point_edgecolor=None,
    interpolation=None,
    label_pos="right",
    color_format="%.1f",
    colorbar_label="Z-Score",
    cbar_shrink=0.15,
    ddof=0,
    fig=None,
):
    initial_features_len = len(features)
    features = [
        feature
        for feature in features
        if (feature in adata.obs.columns.tolist())
        or (feature in adata.var_names.tolist())
    ]

    if group_col is None:
        input_data = adata.obs[id_col].to_frame()
    else:
        input_data = adata.obs[[id_col, group_col]].copy()

    if len(features) < initial_features_len:
        logging.warning("Not all provided features were found in AnnData Object")

    elif len(features) == 0:
        logging.error("No features found in AnnData Object. Terminating...")

    # Add Features to DimRed Data
    for feature in features:
        if feature in adata.var_names.tolist():
            input_data[feature] = pd.DataFrame(
                adata.layers[layer][:, adata.var_names == feature].toarray(),
                columns=[feature],
                index=adata.obs.index,
            )[feature].copy()
        elif feature in adata.obs.columns.tolist():
            input_data[feature] = pd.DataFrame(
                adata.obs.loc[:, adata.obs.columns == feature],
                columns=[feature],
                index=adata.obs.index,
            )[feature].copy()
        else:
            continue

    if group_col is None:
        data_frame = input_data.groupby(id_col).mean().T
        frac_frame = input_data.groupby([id_col]).agg(
            lambda x: ((x > 0).sum() / len(x)) * 100
        )
        # frac_frame.index = ["-_-".join(ix).strip() for ix in frac_frame.index.values]
        frac_frame["Sample"] = frac_frame.index
        frac_frame = pd.melt(
            frac_frame, id_vars=["Sample"], var_name="Gene", value_name="frac"
        )
        group_col = id_col
    else:
        data_frame = input_data.groupby([group_col, id_col]).mean().T
        frac_frame = input_data.groupby([group_col, id_col]).agg(
            lambda x: ((x > 0).sum() / len(x)) * 100
        )
        frac_frame.index = ["-_-".join(ix).strip() for ix in frac_frame.index.values]
        frac_frame["Sample"] = frac_frame.index
        frac_frame = pd.melt(
            frac_frame, id_vars=["Sample"], var_name="Gene", value_name="frac"
        )

    data_frame = data_frame.dropna(axis=1, how="all")

    if do_zscore is True:
        matrix = data_frame.to_numpy()
        matrix = zscore(matrix, axis=1, ddof=ddof, nan_policy="propagate")
        data_frame = pd.DataFrame(
            matrix, index=data_frame.index, columns=data_frame.columns
        )

    if colors_dict is None:
        colors_dict = {}
        colors = sns.color_palette(
            color_map,
            n_colors=len(data_frame.columns.get_level_values(group_col).tolist()),
        ).as_hex()
        for i, member in enumerate(
            data_frame.columns.get_level_values(group_col).tolist()
        ):
            colors_dict[member] = colors[i]

    if group_col != id_col:
        data_frame.columns = [
            "-_-".join(col).strip() for col in data_frame.columns.values
        ]
    data_frame["Gene"] = data_frame.index
    data_frame = pd.melt(
        data_frame, id_vars=["Gene"], var_name="Sample", value_name="score"
    )

    data_frame = data_frame.merge(
        frac_frame, left_on=["Sample", "Gene"], right_on=["Sample", "Gene"], how="left"
    )

    if group_col == id_col:
        data_frame["GROUP"] = data_frame["Sample"]
        data_frame["ID"] = data_frame["Sample"]
    else:
        data_frame[["GROUP", "ID"]] = data_frame["Sample"].str.rsplit(
            "-_-", 1, expand=True
        )

    data_frame["GROUP_colors"] = data_frame["GROUP"].map(colors_dict).tolist()

    cmap = matplotlib.cm.get_cmap(color_map)
    norm = matplotlib.colors.Normalize(vmin=color_vmin, vmax=color_vmax)
    m = matplotlib.cm.ScalarMappable(norm=norm, cmap=cmap)

    data_frame["colors"] = [m.to_rgba(x) for x in data_frame["score"].tolist()]

    data_frame["Gene"] = pd.Categorical(
        data_frame["Gene"], categories=features, ordered=True
    )

    if id_order is not None:
        data_frame["ID"] = pd.Categorical(
            data_frame["ID"], categories=id_order, ordered=True
        )
        data_frame = data_frame.sort_values(["ID", "Gene"], ascending=[True, False])

    if groups_order is not None and group_col is not None:
        data_frame["GROUP"] = pd.Categorical(
            data_frame["GROUP"], categories=groups_order, ordered=True
        )
        data_frame = data_frame.sort_values(
            ["GROUP", "ID", "Gene"], ascending=[True, True, False]
        )
    # print(data_frame)
    if fig_height is None:
        fig_height = 5 * (1 + (len(features) / 30))

    genes = data_frame["Gene"].unique()
    if fig is None:
        fig, ax = plt.subplots(
            1,
            1,
            figsize=(fig_width, fig_height),
            squeeze=True,
            facecolor="white",
            edgecolor="black",
        )
    else:
        ax = fig.axes
    fig.subplots_adjust()
    ax.grid(False)

    plot = ax.scatter(
        data=data_frame,
        x="Sample",
        y="Gene",
        s="frac",
        c="colors",
        alpha=1,
        linewidths=point_linewidth,
        edgecolors=point_edgecolor,
        label="scatter",
    )

    ax.spines[:].set_color("black")
    ax.spines[:].set_visible(True)
    ax.set_facecolor("white")
    ax.tick_params(axis="both", which="both", length=0)

    if group_col is None:
        structure_df = data_frame[["ID", "Sample", "GROUP_colors"]].drop_duplicates()
    else:
        structure_df = data_frame[
            ["Sample", "ID", "GROUP", "GROUP_colors"]
        ].drop_duplicates()

    # print(structure_df)

    if group_col is None:
        x_ticks = structure_df["ID"].tolist()
        ax.set_xticks(
            np.arange(len(x_ticks)),
            labels=x_ticks,
            rotation=rotate_xlabels,
            ha="center",
        )
    else:
        x_ticks = structure_df["ID"].tolist()
        ax.set_xticks(
            np.arange(len(x_ticks)),
            labels=x_ticks,
            rotation=rotate_xlabels,
            ha="center",
        )

    ax.set_yticks(np.arange(len(genes)), labels=genes)
    ax.set_xlim(left=-0.5, right=len(x_ticks) - 0.5)
    ax.set_ylim(bottom=-0.5, top=len(genes) - 0.5)
    ax.margins(x=0)

    if label_pos == "right":
        ax.tick_params(
            top=True,
            bottom=False,
            labeltop=True,
            labelbottom=False,
            labelright=True,
            labelleft=False,
        )
        cbar_pos = "left"
    else:
        ax.tick_params(
            top=True,
            bottom=False,
            labeltop=True,
            labelbottom=False,
            labelright=False,
            labelleft=True,
        )
        cbar_pos = "right"

    ax.tick_params(axis="x", pad=x_pad)
    ax.tick_params(axis="y", pad=y_pad)

    main_cbar = fig.colorbar(
        m,
        ax=ax,
        shrink=cbar_shrink,
        location=cbar_pos,
        label="Z-Score",
        anchor=(1, 1),
        drawedges=False,
        format=color_format,
    )
    main_cbar.outline.set_edgecolor("black")
    main_cbar.ax.tick_params(
        color="black",
        direction="inout",
        labelcolor="black",
        length=5,
        size=8,
        which="both",
        axis="y",
    )
    main_cbar.set_label(colorbar_label, fontsize=8, color="black")
    width_pos = -0.5

    for i, row in structure_df.iterrows():
        ax.add_patch(
            Rectangle(
                (width_pos, len(features) + bar_padding),
                width=1,
                height=bar_height,
                facecolor=row.GROUP_colors,
                clip_on=False,
                linewidth=0,
            )
        )
        width_pos += 1

    fig.legend(
        *plot.legend_elements("sizes", num=4),
        loc="center left",
        bbox_to_anchor=(1, 0.5),
        title="Percent Expressed"
    )
    plt.margins(x=0)
    plt.tight_layout()
    plt.close()
    return (fig, data_frame)


def LR_expression_dotplot(
    connectome_results,
    id_order,
    groups_order,
    reference_source,
    Ligands=None,
    Receptors=None,
    color_vmax=1,
    color_vmin=0,
    do_zscore=False,
    bar_height=0.5,
    bar_padding=-0.5,
    x_pad=25,
    y_pad=5,
    sep_pad=0.015,
    fig_width=4,
    fig_height=12,
    rotate_xlabels=-90,
    cell_type_colors_dict=None,
    condition_colors_dict=None,
    mol_type_colors={"Ligand": "#252850", "Receptor": "#b44c43"},
    color_map=None,
    point_linewidth=0,
    point_edgecolor=None,
    interpolation=None,
    label_pos="right",
    color_format="%.1f",
    colorbar_label="Norm. Expression Counts",
    cbar_shrink=0.25,
    cbar_pad=0.5,
    fig=None,
    ddof=0,
    cbar_pos="right",
    split_LR=False,
):
    connectome_results = connectome_results.copy()

    if Ligands is not None:
        connectome_results = connectome_results[
            connectome_results["Ligand"].isin(Ligands)
        ]
    else:
        Ligands = connectome_results["Ligand"].unique().tolist()

    if Receptors is not None:
        connectome_results = connectome_results[
            connectome_results["Receptor"].isin(Receptors)
        ]
    else:
        Receptors = connectome_results["Receptor"].unique().tolist()

    if isinstance(reference_source, list):
        connectome_results = connectome_results[
            connectome_results["Source"].isin(reference_source)
        ]
    else:
        connectome_results = connectome_results[
            connectome_results["Source"] == reference_source
        ]

    connectome_results["Ligand"] = pd.Categorical(
        connectome_results["Ligand"], categories=Ligands, ordered=True
    )

    connectome_results["Receptor"] = pd.Categorical(
        connectome_results["Receptor"], categories=Receptors, ordered=True
    )

    if id_order is not None:
        connectome_results["Source"] = pd.Categorical(
            connectome_results["Source"], categories=id_order, ordered=True
        )
        connectome_results["Target"] = pd.Categorical(
            connectome_results["Target"], categories=id_order, ordered=True
        )
        connectome_results = connectome_results.sort_values(
            ["Source", "Target", "Ligand", "Receptor"],
            ascending=[True, True, False, False],
        )

    if groups_order is not None:
        connectome_results["Condition"] = pd.Categorical(
            connectome_results["Condition"], categories=groups_order, ordered=True
        )
        connectome_results = connectome_results.sort_values(
            ["Condition", "Source", "Target", "Ligand", "Receptor"],
            ascending=[True, True, True, False, False],
        )

    connectome_results = connectome_results[
        [
            "Source",
            "Target",
            "Interactions",
            "Ligand",
            "Condition",
            "Receptor",
            "Ligand_mean",
            "Receptor_mean",
            "Ligand_frac",
            "Receptor_frac",
        ]
    ]

    connectome_results["L_R"] = connectome_results[["Ligand", "Receptor"]].apply(
        list, axis=1
    )
    connectome_results["L_R_score"] = connectome_results[
        ["Ligand_mean", "Receptor_mean"]
    ].apply(list, axis=1)
    connectome_results["Source_Target"] = connectome_results[
        ["Source", "Target"]
    ].apply(list, axis=1)
    connectome_results["frac"] = connectome_results[
        ["Ligand_frac", "Receptor_frac"]
    ].apply(list, axis=1)
    connectome_results["mol_type"] = [
        ["Ligand", "Receptor"] for x in range(connectome_results.shape[0])
    ]

    connectome_results = connectome_results.explode(
        ["L_R", "L_R_score", "mol_type", "Source_Target", "frac"]
    )

    connectome_results["mol_type"] = pd.Categorical(
        values=connectome_results["mol_type"],
        categories=["Ligand", "Receptor"],
        ordered=True,
    )

    connectome_results = (
        connectome_results[
            [
                "Target",
                "Interactions",
                "Condition",
                "L_R",
                "L_R_score",
                "mol_type",
                "Source_Target",
                "frac",
            ]
        ]
        .drop_duplicates(["Target", "Interactions", "Condition", "L_R"])
        .sort_values(["mol_type", "Target", "Interactions", "L_R", "Condition"])
    )

    connectome_results["ID"] = (
        connectome_results["Source_Target"].astype(str)
        + "_"
        + connectome_results["Condition"].astype(str)
        + "_"
        + connectome_results["mol_type"].astype(str)
    )

    if do_zscore:
        connectome_results["use_score"] = (
            connectome_results.groupby(["Interactions", "mol_type"])["L_R_score"]
            .transform(lambda x: zscore(x.tolist(), ddof=ddof))
            .fillna(0)
        )

    else:
        connectome_results["use_score"] = connectome_results["L_R_score"].copy()

    if condition_colors_dict is None:
        condition_colors_dict = {}
        colors_chosen = sns.color_palette(
            color_map, n_colors=len(connectome_results["Condition"].unique())
        ).as_hex()
        for i, member in enumerate(connectome_results["Condition"].unique()):
            condition_colors_dict[member] = colors_chosen[i]

    connectome_results["Condition_colors"] = (
        connectome_results["Condition"].map(condition_colors_dict).tolist()
    )

    if cell_type_colors_dict is None:
        cell_type_colors_dict = {}
        colors_chosen = sns.color_palette(
            color_map, n_colors=len(connectome_results["Target"].unique())
        ).as_hex()
        for i, member in enumerate(connectome_results["Target"].unique()):
            cell_type_colors_dict[member] = colors_chosen[i]

    connectome_results["Target_colors"] = (
        connectome_results["Source_Target"].map(cell_type_colors_dict).tolist()
    )

    cmap = matplotlib.cm.get_cmap(color_map)
    norm = matplotlib.colors.Normalize(vmin=color_vmin, vmax=color_vmax)
    m = matplotlib.cm.ScalarMappable(norm=norm, cmap=cmap)

    connectome_results["score_colors"] = [
        m.to_rgba(x) for x in connectome_results["use_score"].tolist()
    ]

    connectome_results["mol_type_colors"] = (
        connectome_results["mol_type"].map(mol_type_colors).tolist()
    )

    connectome_results["frac"] = connectome_results["frac"].astype(np.float64) * 100
    connectome_results = connectome_results.sort_values(
        ["mol_type", "L_R", "Source_Target", "Condition"]
    )

    connectome_results["L_R"] = pd.Categorical(
        connectome_results["L_R"],
        categories=connectome_results["L_R"].unique().tolist(),
        ordered=True,
    )
    connectome_results["Source_Target"] = pd.Categorical(
        connectome_results["Source_Target"],
        categories=connectome_results["Source_Target"].unique().tolist(),
        ordered=True,
    )
    connectome_results["Interactions"] = pd.Categorical(
        connectome_results["Interactions"],
        categories=connectome_results["Interactions"].unique().tolist(),
        ordered=True,
    )

    # total_len = len(connectome_results["Source_Target"].unique().tolist())

    sender_len = len(
        connectome_results[connectome_results["mol_type"] == "Ligand"]["Source_Target"]
        .unique()
        .tolist()
    )
    receiver_len = len(
        connectome_results[connectome_results["mol_type"] == "Receptor"][
            "Source_Target"
        ]
        .unique()
        .tolist()
    )

    group_len = len(connectome_results["Condition"].unique().tolist())

    split_dfs = {}

    split_dfs["Ligand"] = connectome_results[
        connectome_results["mol_type"] == "Ligand"
    ].sort_values(["Interactions", "Source_Target", "Condition"])

    split_dfs["Receptor"] = connectome_results[
        connectome_results["mol_type"] == "Receptor"
    ].sort_values(["Interactions", "Source_Target", "Condition"])

    if fig_height is None:
        fig_height = 10 * (1 + (len(Ligands) / 30))

    if fig is None:
        fig = plt.figure(figsize=(fig_width, fig_height))

    gs = GridSpec(
        1,
        2,
        width_ratios=[sender_len * group_len, receiver_len * group_len],
        wspace=sep_pad,
    )

    axes = {}
    axes["Ligand"] = fig.add_subplot(gs[0])
    axes["Receptor"] = fig.add_subplot(gs[1])

    for n, ax in axes.items():
        df = split_dfs[n]
        ax.grid(False)
        ax.spines[:].set_color("black")
        ax.spines[:].set_visible(True)
        ax.set_facecolor("white")

        ax.tick_params(axis="both", which="both", length=0)

        x_ticks = df["ID"].drop_duplicates().tolist()

        ax.set_xticks(
            np.arange(len(x_ticks)),
            labels=x_ticks,
            rotation=rotate_xlabels,
            ha="center",
        )

        y_ticks = df.drop_duplicates(["Interactions", "L_R"], keep="first")[
            "Interactions"
        ].tolist()
        y_ticks_text = df.drop_duplicates(["Interactions", "L_R"], keep="first")[
            "L_R"
        ].tolist()
        y_ticks_text_long = df.drop_duplicates(["Interactions", "L_R"], keep="first")[
            "Interactions"
        ].tolist()

        ax.set_xlim(left=-0.5, right=len(x_ticks) - 0.5)
        ax.set_ylim(bottom=-0.5, top=len(y_ticks) - 0.5)

        ax.margins(x=0.1)

        if n == "Ligand":
            if split_LR is False:
                ax.tick_params(
                    top=True,
                    bottom=False,
                    labeltop=True,
                    labelbottom=False,
                    labelright=False,
                    labelleft=False,
                )

            else:
                ax.set_yticks(np.arange(len(y_ticks)), labels=y_ticks_text)

                ax.tick_params(
                    top=True,
                    bottom=False,
                    labeltop=True,
                    labelbottom=False,
                    labelright=False,
                    labelleft=True,
                )

        else:
            if split_LR is False:
                ax.set_yticks(np.arange(len(y_ticks)), labels=y_ticks_text_long)

            else:
                ax.set_yticks(np.arange(len(y_ticks)), labels=y_ticks_text)

            ax.tick_params(
                top=True,
                bottom=False,
                labeltop=True,
                labelbottom=False,
                labelright=True,
                labelleft=False,
            )

        ax.tick_params(axis="x", pad=x_pad)
        ax.tick_params(axis="y", pad=y_pad)

        plot = ax.scatter(
            data=df,
            x="ID",
            y="Interactions",
            s="frac",
            c="score_colors",
            alpha=1,
            linewidths=point_linewidth,
            edgecolors=point_edgecolor,
            cmap=cmap,
            vmax=color_vmax,
            vmin=color_vmin,
            label="scatter",
        )

        structure_df = df[
            ["ID", "Condition_colors", "Target_colors", "mol_type_colors"]
        ].drop_duplicates()

        width_pos = -0.5
        for i, row in structure_df.iterrows():
            ax.add_patch(
                Rectangle(
                    (width_pos, len(y_ticks_text) + bar_padding),
                    width=1,
                    height=bar_height,
                    facecolor=row.Condition_colors,
                    clip_on=False,
                    linewidth=0,
                )
            )
            width_pos += 1

        width_pos = -0.5
        for i, row in structure_df.iterrows():
            ax.add_patch(
                Rectangle(
                    (width_pos, len(y_ticks_text) + bar_padding + bar_height),
                    width=1,
                    height=(bar_height * 2),
                    facecolor=row.mol_type_colors,
                    clip_on=False,
                    linewidth=0,
                )
            )
            width_pos += 1

        width_pos = -0.5
        for i, row in structure_df.iterrows():
            ax.add_patch(
                Rectangle(
                    (
                        width_pos,
                        len(y_ticks_text) + bar_padding + bar_height + bar_height * 2,
                    ),
                    width=1,
                    height=bar_height,
                    facecolor=row.Target_colors,
                    clip_on=False,
                    linewidth=0,
                )
            )
            width_pos += 1

    main_cbar = fig.colorbar(
        m,
        ax=fig.axes,
        shrink=cbar_shrink,
        location=cbar_pos,
        label=colorbar_label,
        pad=cbar_pad,
        anchor=(1, 1),
        drawedges=False,
        format=color_format,
    )

    main_cbar.outline.set_edgecolor("black")

    main_cbar.ax.tick_params(
        color="black",
        direction="inout",
        labelcolor="black",
        length=5,
        size=8,
        which="both",
        axis="y",
    )

    main_cbar.set_label(colorbar_label, fontsize=8, color="black")

    fig.legend(
        *plot.legend_elements("sizes", num=4),
        loc="upper right",
        bbox_to_anchor=(0.5, 0),
        title="Percentage of Cells Expressing"
    )

    plt.margins(x=0)
    plt.tight_layout()
    plt.close()

    return fig


def Expression_LinePlot(
    adata,
    x_axis,
    genes_and_clusters,
    x_axis_names=None,
    x_is_num=True,
    plot_x_axis_areas=False,
    z_scaled=True,
    layer="counts",
    group_by=None,
    x_min_col=None,
    x_max_col=None,
    x_round=3,
    ci=95,
    x_min=None,
    x_max=None,
    y_min=0,
    y_max=1,
    size_mod=5,
    color_dict=None,
    color_palette_celltype="viridis",
    color_palette_clusters="Dark2",
    k_clusters=1,
    random_state=42,
    ddof=0,
):
    adata = adata.copy()

    df = pd.DataFrame(
        adata.layers[layer].toarray(),
        columns=adata.var_names,
        index=adata.obs_names,
    )
    genes = df.columns.tolist()

    df["x"] = adata.obs[x_axis].copy()

    if x_axis_names is None:
        df["x_names"] = df["x"].copy()
    else:
        df["x_names"] = adata.obs[x_axis_names].copy()

    if group_by is not None:
        df["GROUP"] = adata.obs[group_by].copy()
    else:
        df["GROUP"] = "Line Plot"

    if color_dict is None:
        types = df["x_names"].unique()
        n_types = len(types)
        base_colors = sns.color_palette(color_palette_celltype, n_types).as_hex()
        color_dict = dict(zip(types, base_colors))

        df["x_bin_color"] = df["x_names"].map(color_dict)
    else:
        df["x_bin_color"] = df["x_names"].map(color_dict)

    # Find and annotate continuous stretches of the same X annotation

    if x_min_col is not None and x_max_col is not None:
        df = df.sort_values("x", ascending=True)

        df["x_min_bin"] = adata.obs[x_min_col].astype(float).copy()
        df["x_max_bin"] = adata.obs[x_max_col].astype(float).copy()

        df.index = df.index.astype(int)
        df_names = np.asanyarray(df["x_names"])
        partitions = np.split(
            np.r_[: len(df_names)], np.where(df_names[1:] != df_names[:-1])[0] + 1
        )

        df["cont_bin_id"] = None
        i = 0
        for indices in partitions:
            df.loc[df.index.isin(indices.tolist()), "cont_bin_id"] = i
            i += 1

    else:
        df = df.sort_values("x", ascending=True)
        df.index = df.index.astype(int)
        df_names = np.asanyarray(df["x_names"])
        partitions = np.split(
            np.r_[: len(df_names)], np.where(df_names[1:] != df_names[:-1])[0] + 1
        )

        df["cont_bin_id"] = None
        i = 0
        for indices in partitions:
            df.loc[df.index.isin(indices.tolist()), "cont_bin_id"] = i
            i += 1

        x_min_bin = (
            df.groupby("cont_bin_id")["x"]
            .min()
            .reset_index()
            .rename({"x": "x_min_bin"}, axis=1)
        )
        x_max_bin = (
            df.groupby("cont_bin_id")["x"]
            .max()
            .reset_index()
            .rename({"x": "x_max_bin"}, axis=1)
        )

        df = df.merge(x_min_bin).merge(x_max_bin)

    if x_max is None:
        x_bin_max = np.ceil(df["x_max_bin"])
    else:
        x_bin_max = x_max

    if x_min is None:
        x_bin_min = (
            np.floor(df["x_min_bin"].min())
            if np.ceil(df["x_min_bin"].min()) != 0
            else 0
        )
    else:
        x_bin_min = x_min

    color_area = (
        df[
            [
                "x_names",
                "x_bin_color",
                "x_min_bin",
                "x_max_bin",
            ]
        ]
        .copy()
        .rename(
            {
                "x_min_bin": "area_min",
                "x_max_bin": "area_max",
                "x_bin_color": "colors",
                "x_names": "name",
            },
            axis=1,
        )
        .drop_duplicates()
    )

    color_area = color_area.groupby(
        (color_area["name"].shift() != color_area["name"]).cumsum()
    ).agg(
        {
            "area_min": "min",
            "area_max": "max",
            "name": lambda column: column.unique()[0],
            "colors": lambda column: column.unique()[0],
        }
    )

    if x_is_num:
        df["x"] = df["x"].astype(float).round(x_round).copy()

    df = df.melt(
        id_vars=[
            "x",
            "x_names",
            "GROUP",
            "x_bin_color",
            "cont_bin_id",
            "x_min_bin",
            "x_max_bin",
        ],
        var_name="Gene",
        value_name="Observation",
    )

    if genes_and_clusters is None:
        df_temp = df.loc[genes, :]
        clusters = (
            KMeans(n_clusters=k_clusters, random_state=random_state)
            .fit(df_temp)
            .labels_.tolist()
        )
        df["clusters"] = clusters

    else:
        df = df.merge(
            genes_and_clusters,
            how="inner",
            left_on="Gene",
            right_on="Gene",
            validate="many_to_one",
        )

    df = df[~df["clusters"].isin([np.nan, np.inf])]

    if z_scaled:
        df["Observation"] = (
            df.groupby(["GROUP", "Gene"])["Observation"]
            .transform(lambda x: zscore(x.tolist(), ddof=ddof))
            .fillna(0)
        )

    ticks_prepared = [x_bin_min]
    ticks_prepared.extend(df["x"].unique().tolist())
    ticks_prepared.append(x_bin_max)

    fig, axs = plt.subplots(
        1,
        len(df["GROUP"].unique()),
        figsize=(len(df["GROUP"].unique()) * size_mod, 5),
        squeeze=True,
        facecolor="white",
        edgecolor="black",
    )

    i = 0

    df["Pseudotime"] = df["x"]

    for n, group in df.groupby("GROUP"):
        ax = axs[i]
        sns.lineplot(
            data=group,
            x="Pseudotime",
            y="Observation",
            errorbar=("ci", ci),
            hue="clusters",
            legend=True,
            palette=color_palette_clusters,
            ax=ax,
        )

        ax.get_legend().remove()
        box = ax.get_position()
        ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])

        ax.legend(
            loc="center left",
            bbox_to_anchor=(1, 0.5),
            ncol=1,
            borderaxespad=0.1,
            frameon=True,
            mode=None,
        )

        ax.set_title(n)
        ax.set_xlabel("Central Pseudotime of Bin", fontsize="large", color="black")

        ax.set_ylabel(
            "z-scaled Counts per Gene Cluster", fontsize="large", color="black"
        )
        ax.spines[["left", "bottom"]].set_color("black")
        ax.spines[["left", "bottom"]].set_linewidth(1)
        ax.spines[["top", "right"]].set_visible(False)
        ax.grid(False)
        ax.set_facecolor("white")
        ax.set_xlim(left=x_bin_min, right=x_bin_max)
        ax.set_ylim(bottom=y_min, top=y_max)
        ax.set_aspect(1.0 / ax.get_data_ratio())

        if plot_x_axis_areas:
            for ix, row in color_area.iterrows():
                ax.axvspan(
                    row["area_min"],
                    row["area_max"],
                    facecolor=row["colors"],
                    alpha=0.2,
                    zorder=-100,
                    label=row["name"],
                )

        i += 1

    plt.tight_layout()
    plt.close()
    return (df, fig)


def Expression_Heatmap(
    adata,
    x_axis,
    genes_and_clusters=None,
    x_axis_names=None,
    x_is_num=True,
    x_name_is_num=False,
    plot_x_axis_areas=False,
    plot_y_labels=False,
    z_scaled=True,
    layer="counts",
    group_by=None,
    x_min_col=None,
    x_max_col=None,
    x_round=3,
    vmax=1,
    vmin=0,
    figure_width=15,
    figure_height=30,
    interpolation="nearest",
    size_mod=5,
    bar_height=3,
    bar_padding=-0.9,
    x_pad=25,
    y_pad=5,
    color_dict=None,
    color_palette_x_annot="viridis",
    color_palette_y_annot="Dark2",
    color_palette_num="Reds",
    color_format="%.1f",
    k_clusters=1,
    random_state=42,
    ddof=0,
):
    adata = adata.copy()

    df = pd.DataFrame(
        adata.layers[layer].toarray(),
        columns=adata.var_names,
        index=adata.obs_names,
    )
    genes = df.columns.tolist()

    df["x"] = adata.obs[x_axis].copy()

    if x_axis_names is None:
        df["x_names"] = df["x"].copy()
    else:
        df["x_names"] = adata.obs[x_axis_names].copy()

    if group_by is not None:
        df["GROUP"] = adata.obs[group_by].copy()
    else:
        df["GROUP"] = "Line Plot"

    # Find and annotate continuous stretches of the same X annotation

    if x_min_col is not None and x_max_col is not None:
        df = df.sort_values("x", ascending=True)

        df["x_min_bin"] = adata.obs[x_min_col].astype(float).copy()
        df["x_max_bin"] = adata.obs[x_max_col].astype(float).copy()

        # df.index = df.index.astype(int)
        df_names = np.asanyarray(df["x_names"])
        partitions = np.split(
            np.r_[: len(df_names)], np.where(df_names[1:] != df_names[:-1])[0] + 1
        )

        df["cont_bin_id"] = None
        i = 0
        for indices in partitions:
            df.loc[df.index.isin(indices.tolist()), "cont_bin_id"] = i
            i += 1

    else:
        df = df.sort_values("x", ascending=True)

        # df.index = df.index.astype(int)
        df_names = np.asanyarray(df["x_names"])
        partitions = np.split(
            np.r_[: len(df_names)], np.where(df_names[1:] != df_names[:-1])[0] + 1
        )

        df["cont_bin_id"] = None
        i = 0
        for indices in partitions:
            df.loc[df.index.isin(indices.tolist()), "cont_bin_id"] = i
            i += 1

        x_min_bin = (
            df.groupby("cont_bin_id")["x"]
            .min()
            .reset_index()
            .rename({"x": "x_min_bin"}, axis=1)
        )
        x_max_bin = (
            df.groupby("cont_bin_id")["x"]
            .max()
            .reset_index()
            .rename({"x": "x_max_bin"}, axis=1)
        )

        df = df.merge(x_min_bin).merge(x_max_bin)

    if x_is_num:
        df["x"] = df["x"].astype(float).round(x_round).copy()

    if x_name_is_num is False:
        if color_dict is None:
            types = df["x_names"].unique()
            n_types = len(types)
            base_colors = sns.color_palette(color_palette_x_annot, n_types).as_hex()
            color_dict = dict(zip(types, base_colors))

            df["x_bin_color"] = df["x_names"].map(color_dict)
        else:
            df["x_bin_color"] = df["x_names"].map(color_dict)
    else:
        cmap_x = matplotlib.cm.get_cmap(color_palette_x_annot)
        norm_x = matplotlib.colors.Normalize(
            vmin=df["x_min_bin"].min(), vmax=df["x_max_bin"].max()
        )
        m_x = matplotlib.cm.ScalarMappable(norm=norm_x, cmap=cmap_x)
        df["x_bin_color"] = [m_x.to_rgba(x) for x in df["x"].tolist()]

    df = df.melt(
        id_vars=[
            "x",
            "x_names",
            "GROUP",
            "x_bin_color",
            "cont_bin_id",
            "x_min_bin",
            "x_max_bin",
        ],
        var_name="Gene",
        value_name="Observation",
    )

    if genes_and_clusters is None:
        df_temp = df.loc[genes, :]
        clusters = (
            KMeans(n_clusters=k_clusters, random_state=random_state)
            .fit(df_temp)
            .labels_.tolist()
        )
        df["clusters"] = clusters

    else:
        df = df.merge(
            genes_and_clusters,
            how="inner",
            left_on="Gene",
            right_on="Gene",
            validate="many_to_one",
        )

    df = df[~df["clusters"].isin([np.nan, np.inf])]

    if z_scaled:
        df["Observation"] = (
            df.groupby(["GROUP", "Gene"])["Observation"]
            .transform(lambda x: zscore(x.tolist(), ddof=ddof))
            .fillna(0)
        )

    n_groups = len(df["GROUP"].unique())

    fig, axs = plt.subplots(
        1,
        n_groups,
        figsize=(len(df["GROUP"].unique()) * figure_width, figure_height),
        squeeze=True,
        facecolor="white",
        edgecolor="black",
    )

    i = 0

    df["Pseudotime"] = df["x"]

    cmap = matplotlib.cm.get_cmap(color_palette_num)
    norm = matplotlib.colors.Normalize(vmin=vmin, vmax=vmax)

    colors_df_col = df[["Pseudotime", "x_bin_color"]].drop_duplicates()

    df = df.sort_values(["clusters"])
    df["clusters"] = pd.Categorical(
        df["clusters"], categories=df["clusters"].unique().tolist(), ordered=True
    )
    df["Gene"] = pd.Categorical(
        df["Gene"], categories=df["Gene"].unique().tolist(), ordered=True
    )

    df = df.sort_values(["Pseudotime"])
    df["Pseudotime"] = pd.Categorical(
        df["Pseudotime"], categories=df["Pseudotime"].unique().tolist(), ordered=True
    )

    df = df.sort_values(["GROUP"])
    df["GROUP"] = pd.Categorical(
        df["GROUP"], categories=df["GROUP"].unique().tolist(), ordered=True
    )

    df = df.sort_values(["Pseudotime", "Gene", "GROUP"])

    for n, group in df.groupby("GROUP"):
        if n_groups > 1:
            ax = axs[i]
        else:
            ax = axs

        data = group[["Pseudotime", "Gene", "Observation"]].pivot(
            index="Gene", columns="Pseudotime", values="Observation"
        )

        x_names = data.columns.tolist()
        genes = data.index.tolist()

        cluster_ids = pd.DataFrame(genes, columns=["Gene"]).merge(genes_and_clusters)
        cluster_ids["group_size"] = cluster_ids.groupby("clusters")["Gene"].transform(
            len
        )
        cluster_ids["group_size_half"] = np.floor(cluster_ids["group_size"] / 2)
        cluster_ids["group_id"] = cluster_ids.groupby(["clusters"]).cumcount() + 1
        cluster_ids["label"] = np.where(
            cluster_ids["group_id"] == cluster_ids["group_size_half"],
            cluster_ids["clusters"].astype(int).astype(str),
            "",
        )

        im = ax.imshow(
            data,
            cmap=cmap,
            norm=norm,
            aspect="auto",
            interpolation=interpolation,
        )

        ax.spines[:].set_color("black")
        ax.spines[:].set_visible(True)
        ax.set_facecolor("white")
        ax.tick_params(axis="both", which="both", length=0)
        ax.set_xticks(np.arange(len(x_names)), labels=x_names)

        if plot_y_labels == "central":
            ax.set_yticks(
                np.arange(cluster_ids.shape[0]), labels=cluster_ids["label"].tolist()
            )
            ax.tick_params(
                top=True,
                bottom=False,
                left=False,
                labelleft=True,
                labeltop=True,
                labelbottom=False,
                pad=15,
            )
        elif plot_y_labels:
            ax.set_yticks(np.arange(len(genes)), labels=genes)
            ax.tick_params(
                top=True, bottom=False, labeltop=True, labelbottom=False, pad=15
            )
            ax.tick_params(axis="y", pad=y_pad)
        else:
            ax.tick_params(
                top=True,
                left=False,
                labelleft=False,
                bottom=False,
                labeltop=True,
                labelbottom=False,
                pad=15,
            )

        plt.setp(ax.get_xticklabels(), rotation=90, ha="left", rotation_mode="anchor")
        ax.tick_params(axis="x", pad=x_pad)

        ax.set_title(n)
        ax.grid(False)

        width_pos = -0.5
        for index, row in colors_df_col.iterrows():
            ax.add_patch(
                Rectangle(
                    (width_pos, bar_padding),
                    1,
                    bar_height,
                    facecolor=row.x_bin_color,
                    clip_on=False,
                    linewidth=0,
                )
            )
            width_pos += 1

        i += 1

    main_cbar = fig.colorbar(
        im,
        ax=axs,
        shrink=0.15,
        location="right",
        label="Z-Score",
        anchor=(2, 1),
        drawedges=False,
        format=color_format,
    )

    main_cbar.outline.set_edgecolor("black")
    main_cbar.ax.tick_params(
        color="black",
        direction="inout",
        labelcolor="black",
        length=5,
        size=8,
        which="both",
        axis="y",
    )

    main_cbar.set_label("Z-Score", fontsize=8, color="black")

    plt.tight_layout()
    plt.close()
    return (df, fig)


def pseudotime_celltype_composition(
    adata,
    pseudotime_col,
    celltype_col,
    main_identity_col,
    color_map_celltype="viridis",
    color_dict_celltype=None,
    x_round=4,
    fig=None,
    fig_width=10,
    fig_height=2,
    pad_left=1,
    pad_right=1.5,
    pad_top=1,
    pad_bottom=0.1,
    bar_padding=3,
    bar_height=20,
    remove_nan=False,
):
    data = adata.obs[[pseudotime_col, celltype_col, main_identity_col]].copy()

    if remove_nan:
        data = data.dropna()

    data[pseudotime_col] = round(data[pseudotime_col], ndigits=x_round)
    data = data.sort_values(pseudotime_col, ascending=False)

    pseudotime_col_order = data[pseudotime_col].astype(str).unique().tolist()
    data[pseudotime_col] = pd.Categorical(
        data[pseudotime_col].astype(str), ordered=True, categories=pseudotime_col_order
    )

    frac_per_cluster = data.copy()

    frac_per_cluster["count"] = data.groupby([pseudotime_col, celltype_col])[
        celltype_col
    ].transform("count")

    frac_per_cluster = frac_per_cluster.drop_duplicates(
        [pseudotime_col, celltype_col, "count"]
    )
    frac_per_cluster["sum"] = frac_per_cluster.groupby([pseudotime_col])[
        "count"
    ].transform("sum")
    frac_per_cluster = frac_per_cluster[
        [pseudotime_col, celltype_col, "count", "sum", main_identity_col]
    ].reset_index(drop=True)

    frac_per_cluster["frac"] = frac_per_cluster["count"] / frac_per_cluster["sum"]
    frac_per_cluster["pct"] = frac_per_cluster["frac"] * 100
    frac_per_cluster = frac_per_cluster.sort_values(
        [pseudotime_col, celltype_col], ascending=False
    )

    if color_dict_celltype is None:
        color_dict_celltype = {}
        unique_members = list(frac_per_cluster[celltype_col].unique())
        colors = sns.color_palette(
            color_map_celltype, n_colors=len(unique_members)
        ).as_hex()
        for i, member in enumerate(unique_members):
            color_dict_celltype[member] = colors[i]
        frac_per_cluster["colors"] = (
            frac_per_cluster[celltype_col].map(color_dict_celltype).tolist()
        )
        frac_per_cluster["identity_colors"] = (
            frac_per_cluster[main_identity_col].map(color_dict_celltype).tolist()
        )

    else:
        frac_per_cluster["colors"] = (
            frac_per_cluster[celltype_col].map(color_dict_celltype).tolist()
        )
        frac_per_cluster["identity_colors"] = (
            frac_per_cluster[main_identity_col].map(color_dict_celltype).tolist()
        )

    color_dict_celltype = {}
    unique_members = list(frac_per_cluster[celltype_col].unique())
    colors = sns.color_palette(
        color_map_celltype, n_colors=len(unique_members)
    ).as_hex()

    for i, member in enumerate(unique_members):
        color_dict_celltype[member] = colors[i]

    if fig is None:
        fig, axs = plt.subplots(
            1,
            1,
            figsize=(fig_width * 2, fig_height),
            squeeze=False,
            facecolor="white",
            edgecolor="black",
        )
    else:
        axs = fig.axes

    fig.subplots_adjust(left=pad_left, right=pad_right, top=pad_top, bottom=pad_bottom)

    i = 0
    cluster_sum = defaultdict(int)

    color_bars_df = (
        frac_per_cluster[[pseudotime_col, main_identity_col, "identity_colors"]]
        .drop_duplicates([pseudotime_col])
        .reset_index(drop=True)
    )

    frac_per_cluster = frac_per_cluster[
        [pseudotime_col, celltype_col, "count", "sum", "frac", "pct", "colors"]
    ].reset_index(drop=True)

    for row in frac_per_cluster.iterrows():
        ax = axs[0, i]
        x, cluster, count, sums, frac, pct, colors = row[1]
        ax.bar(x, pct, width=1, label=cluster, bottom=cluster_sum[x], color=colors)
        cluster_sum[x] += pct

    ax.spines[["left", "bottom"]].set_color("black")
    ax.spines[["left", "bottom"]].set_linewidth(1)
    ax.spines[["top", "right"]].set_visible(False)
    ax.set_xlabel("Pseudotime Bin", fontsize="large", color="black")
    ax.set_ylabel("Percentage", fontsize="large", color="black")
    ax.grid(False)
    ax.spines[["left", "bottom", "top", "right"]].set_color("black")
    ax.spines[["left", "bottom", "top", "right"]].set_linewidth(1)
    ax.spines[["left", "bottom", "top", "right"]].set_visible(True)
    ax.set_facecolor("white")
    ax.margins(x=0)
    ax.margins(y=0)

    width_pos = -0.5
    for i, row in color_bars_df.iterrows():
        ax.add_patch(
            Rectangle(
                (width_pos, 100 + bar_padding),
                width=1,
                height=bar_height,
                facecolor=row.identity_colors,
                clip_on=False,
                linewidth=0,
            )
        )

        width_pos += 1

    legend_without_duplicate_labels(ax)

    plt.tight_layout()
    plt.setp(ax.get_xticklabels(), rotation=90, ha="right", rotation_mode="anchor")
    ax.set_ylim(bottom=0, top=100)
    plt.close()
    return fig


def general_heatmap(
    matrix,
    color_map="viridis",
    color_format="%.1f",
    colors_dict=None,
    vmax=2,
    vmin=-2,
    bar_padding=0,
    bar_height=1,
    do_zscore=True,
    zscore_axis=1,
    interpolation="nearest",
    color_label="Z-score",
    fig=None,
):
    if do_zscore is True:
        matrix = zscore(matrix, axis=zscore_axis, ddof=0, nan_policy="propagate")

    samples = matrix.index.tolist()
    cell_types = matrix.columns.tolist()

    if colors_dict is None:
        colors_dict = {}
        colors = sns.color_palette(color_map, n_colors=len(cell_types)).as_hex()
        for i, member in enumerate(cell_types):
            colors_dict[member] = colors[i]

    cell_type_colors = matrix.columns.map(colors_dict).tolist()
    colors_df = pd.DataFrame({"cell_type": cell_types, "colors": cell_type_colors})

    if fig is None:
        fig, ax = plt.subplots(
            1,
            1,
            figsize=(8, (len(samples))),
            squeeze=True,
            facecolor="white",
            edgecolor="black",
        )
    else:
        ax = fig.axes

    fig.subplots_adjust()
    ax.grid(False)

    im = ax.imshow(
        matrix,
        cmap=color_map,
        vmax=vmax,
        vmin=vmin,
        aspect="auto",
        interpolation=interpolation,
    )
    ax.spines[:].set_color("black")
    ax.spines[:].set_visible(True)
    ax.set_facecolor("white")
    ax.tick_params(axis="both", which="both", length=0)
    ax.set_xticks(np.arange(len(cell_types)), labels=cell_types)
    ax.set_yticks(np.arange(len(samples)), labels=samples)
    ax.tick_params(top=True, bottom=False, labeltop=True, labelbottom=False, pad=15)
    plt.setp(ax.get_xticklabels(), rotation=-90, ha="right", rotation_mode="anchor")

    main_cbar = fig.colorbar(
        im,
        ax=ax,
        shrink=0.15,
        location="right",
        label="Z-Score",
        anchor=(0, 1),
        drawedges=False,
        format=color_format,
    )
    main_cbar.outline.set_edgecolor("black")
    main_cbar.ax.tick_params(
        color="black",
        direction="inout",
        labelcolor="black",
        length=5,
        size=8,
        which="both",
        axis="y",
    )
    main_cbar.set_label(color_label, fontsize=8, color="black")

    width_pos = -0.5
    for i, row in colors_df.iterrows():
        ax.add_patch(
            Rectangle(
                (width_pos, bar_padding * bar_height),
                1,
                0.4,
                facecolor=row.colors,
                clip_on=False,
                linewidth=0,
            )
        )
        width_pos += 1
    plt.tight_layout()
    plt.close()
    return fig


def general_dotplot(
    matrix_long,
    x_col,
    y_col,
    color_col,
    size_col,
    color_vmax=3,
    color_vmin=-3,
    do_zscore=True,
    zscore_axis=None,
    bar_height=0.2,
    bar_padding=3.5,
    x_pad=5,
    y_pad=5,
    fig_width=8,
    fig_height=None,
    rotate_xlabels=-90,
    colors_dict=None,
    color_map="viridis",
    point_linewidth=0,
    point_edgecolor=None,
    interpolation=None,
    label_pos="right",
    color_format="%.1f",
    colorbar_label="Z-Score",
    size_label="Percent Expressed",
    cbar_shrink=0.15,
    ddof=0,
    fig=None,
):
    if zscore_axis is None:
        zscore_axis = y_col

    features = matrix_long[y_col].unique().tolist()

    if do_zscore is True:
        matrix_long["use_score"] = (
            matrix_long.groupby(zscore_axis)[color_col]
            .transform(lambda x: zscore(x.tolist(), ddof=ddof))
            .fillna(0)
        )

    else:
        matrix_long["use_score"] = matrix_long[color_col]

    if colors_dict is None:
        colors_dict = {}
        colors = sns.color_palette(
            color_map,
            n_colors=len(matrix_long[x_col].tolist()),
        ).as_hex()
        for i, member in enumerate(matrix_long[x_col].tolist()):
            colors_dict[member] = colors[i]

    matrix_long["GROUP_colors"] = matrix_long[x_col].map(colors_dict).tolist()

    cmap = matplotlib.cm.get_cmap(color_map)
    norm = matplotlib.colors.Normalize(vmin=color_vmin, vmax=color_vmax)
    m = matplotlib.cm.ScalarMappable(norm=norm, cmap=cmap)

    matrix_long["colors"] = [m.to_rgba(x) for x in matrix_long["use_score"].tolist()]

    if fig_height is None:
        fig_height = 5 * (1 + (len(features) / 30))

    if fig is None:
        fig, ax = plt.subplots(
            1,
            1,
            figsize=(fig_width, fig_height),
            squeeze=True,
            facecolor="white",
            edgecolor="black",
        )
    else:
        ax = fig.axes
    fig.subplots_adjust()
    ax.grid(False)

    plot = ax.scatter(
        data=matrix_long,
        x=x_col,
        y=y_col,
        s=size_col,
        c="colors",
        alpha=1,
        linewidths=point_linewidth,
        edgecolors=point_edgecolor,
        label="scatter",
    )

    ax.spines[:].set_color("black")
    ax.spines[:].set_visible(True)
    ax.set_facecolor("white")
    ax.tick_params(axis="both", which="both", length=0)

    structure_df = matrix_long[[x_col, "GROUP_colors"]].drop_duplicates()

    x_ticks = structure_df[x_col].unique().tolist()
    ax.set_xticks(
        np.arange(len(x_ticks)),
        labels=x_ticks,
        rotation=rotate_xlabels,
        ha="center",
    )

    ax.set_yticks(np.arange(len(features)), labels=features)
    ax.set_xlim(left=-0.5, right=len(x_ticks) - 0.5)
    ax.set_ylim(bottom=-0.5, top=len(features) - 0.5)
    ax.margins(x=0)

    if label_pos == "right":
        ax.tick_params(
            top=True,
            bottom=False,
            labeltop=True,
            labelbottom=False,
            labelright=True,
            labelleft=False,
        )
        cbar_pos = "left"
    else:
        ax.tick_params(
            top=True,
            bottom=False,
            labeltop=True,
            labelbottom=False,
            labelright=False,
            labelleft=True,
        )
        cbar_pos = "right"

    ax.tick_params(axis="x", pad=x_pad)
    ax.tick_params(axis="y", pad=y_pad)

    main_cbar = fig.colorbar(
        m,
        ax=ax,
        shrink=cbar_shrink,
        location=cbar_pos,
        label="Z-Score",
        anchor=(1, 1),
        drawedges=False,
        format=color_format,
    )
    main_cbar.outline.set_edgecolor("black")
    main_cbar.ax.tick_params(
        color="black",
        direction="inout",
        labelcolor="black",
        length=5,
        size=8,
        which="both",
        axis="y",
    )
    main_cbar.set_label(colorbar_label, fontsize=8, color="black")
    width_pos = -0.5

    for i, row in structure_df.iterrows():
        ax.add_patch(
            Rectangle(
                (width_pos, len(features) + bar_padding),
                width=1,
                height=bar_height,
                facecolor=row.GROUP_colors,
                clip_on=False,
                linewidth=0,
            )
        )
        width_pos += 1

    fig.legend(
        *plot.legend_elements("sizes", num=4),
        loc="center left",
        bbox_to_anchor=(1, 0.5),
        title=size_label
    )
    plt.margins(x=0)
    plt.tight_layout()
    plt.close()

    return (fig, matrix_long)


def add_paga_nodes(
    adata,
    figure,
    pseudotime_col,
    cell_cluster_col,
    dim_red="X_umap",
    pt_size=20,
    color_map="viridis",
    show_pseudotime_mean=True,
    show_cluster_color=False,
    cluster_color_dict=None,
    line_width=10,
    line_color="black",
    edge_color="black",
):
    if show_pseudotime_mean:
        dot_color = "pseudotime_mean"
    elif show_cluster_color and cluster_color_dict is not None:
        dot_color = "cluster_color"
    else:
        dot_color = "black"
    adata = adata.copy()

    dim_red_mat = adata.obsm[dim_red][:, :2]
    if pseudotime_col is not None:
        annotation = adata.obs[[pseudotime_col, cell_cluster_col]]
    else:
        annotation = adata.obs[[cell_cluster_col]]
    clusters = annotation[cell_cluster_col].sort_values().unique().tolist()

    drawing_annot_df = pd.DataFrame({"cluster_names": clusters})
    drawing_annot_df["from"] = [[]] * len(drawing_annot_df)
    drawing_annot_df["to"] = [clusters] * len(drawing_annot_df)
    drawing_annot_df["connection_values"] = [[0, 0, 0]] * len(drawing_annot_df)

    dim_red_mat_annot = pd.DataFrame(dim_red_mat)
    dim_red_mat_annot[cell_cluster_col] = annotation[cell_cluster_col].tolist()

    for clus_num, cluster in enumerate(clusters):
        dim_red_mat_annot_temp = dim_red_mat_annot[
            dim_red_mat_annot[cell_cluster_col] == cluster
        ]

        distance, index = closest_to_centroid(
            arr=dim_red_mat_annot_temp.to_numpy()[:, :2], n_neigh=1
        )
        x_pos = castToList(dim_red_mat_annot_temp.iloc[index, 0].tolist())
        y_pos = castToList(dim_red_mat_annot_temp.iloc[index, 1].tolist())

        drawing_annot_df.loc[drawing_annot_df["cluster_names"] == cluster, "x"] = x_pos
        drawing_annot_df.loc[drawing_annot_df["cluster_names"] == cluster, "y"] = y_pos

        if pseudotime_col is not None:
            drawing_annot_df.loc[
                drawing_annot_df["cluster_names"] == cluster, "pseudotime_mean"
            ] = annotation[annotation[cell_cluster_col] == cluster][
                pseudotime_col
            ].mean()

        connection_values = np.array(adata.uns["paga"]["connectivities"].todense())[
            clus_num, :
        ].tolist()
        drawing_annot_df.at[clus_num, "connection_values"] = connection_values
        drawing_annot_df.at[clus_num, "from"] = [cluster for i in range(len(clusters))]

    axs = figure.axes
    drawing_annot_df = drawing_annot_df.explode(
        ["from", "to", "connection_values"], ignore_index=False
    )
    drawing_annot_df_dots = drawing_annot_df.drop_duplicates(
        ["cluster_names", "x", "y"]
    )

    drawing_annot_df_lines = (
        drawing_annot_df[drawing_annot_df["connection_values"] > 0]
        .rename({"x": "x_from", "y": "y_from"}, axis=1)
        .merge(
            drawing_annot_df_dots[["cluster_names", "x", "y"]].rename(
                {"x": "x_to", "y": "y_to"}, axis=1
            ),
            left_on="to",
            right_on="cluster_names",
            how="left",
        )
        .drop("cluster_names_y", axis=1)
        .rename({"cluster_names_x": "cluster_names"}, axis=1)
    )[
        [
            "cluster_names",
            "from",
            "x_from",
            "y_from",
            "to",
            "x_to",
            "y_to",
            "connection_values",
        ]
    ]

    if dot_color == "cluster_color":
        drawing_annot_df_dots["cluster_color"] = (
            drawing_annot_df_dots["cluster_names"].map(cluster_color_dict).tolist()
        )

    for ax in axs:
        for i, row in drawing_annot_df_lines.iterrows():
            ax.plot(
                [row["x_from"], row["x_to"]],
                [row["y_from"], row["y_to"]],
                c=line_color,
                linewidth=line_width * row["connection_values"],
                linestyle="-",
                label="",
            )

        ax.scatter(
            data=drawing_annot_df_dots,
            x="x",
            y="y",
            s=pt_size,
            alpha=1,
            c=dot_color,
            cmap=color_map,
            linewidths=1,
            label="",
            zorder=10,
            edgecolors=edge_color,
        )

    return figure
