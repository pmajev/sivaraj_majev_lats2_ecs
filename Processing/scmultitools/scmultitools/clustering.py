import contextlib
import copy
import datetime
import gc
import io
import logging
import random
import sys
from functools import partial, reduce

import numpy as np
import pandas as pd
import scanpy as sc
import scipy
import statsmodels.stats.api as sms
from plotnine import *
from plotnine.data import *
from scipy.stats import bootstrap
from sklearn.metrics import silhouette_samples
from tqdm.autonotebook import tqdm
from tqdm.contrib.concurrent import process_map

logger = logging.getLogger(__name__)

###
# This is an implementation of the ChooseR Pipeline described in Patterson-Cross, R.B., Levine, A.J. & Menon, V. Selecting single cell clustering parameter values using subsampling-based robustness metrics. BMC Bioinformatics 22, 39 (2021)
###


@contextlib.contextmanager
def nostdout():
    save_stdout = sys.stdout
    sys.stdout = io.BytesIO()
    yield
    sys.stdout = save_stdout


def outer_complex(x, y):
    x = x
    y = y
    nans = [x for x in np.isnan(y)]
    arr = x == y
    arr = arr.astype(complex)
    arr = pd.DataFrame(arr)
    arr.loc[nans, nans] = complex(0, 1)
    arr = scipy.sparse.csr_matrix(arr.values)
    return arr


def outer_complex_sym_iteration(iteration, x):
    y = copy.deepcopy(x)
    x = x[:, iteration][:, None]
    y = y[:, iteration]
    nans = [x for x in np.isnan(y)]
    arr = x == y
    del x
    del y
    gc.collect()
    arr = arr.astype(complex)
    arr = pd.DataFrame(arr)
    arr.loc[nans, nans] = complex(0, 1)
    arr = scipy.sparse.csr_matrix(arr.values)
    gc.collect()
    return arr


def resolve_complex(x, num_iter):
    if x.real > 0:
        x = x.real / (num_iter - x.imag)
    else:
        x = 0
    return x


def make_dissmat(x):
    x = 1 - x
    return x


def confidence_interval(data, confidence=0.95):
    a = 1.0 * np.array(data)
    n = len(a)
    m, se = np.mean(a), scipy.stats.sem(a)
    h = se * scipy.stats.t.ppf((1 + confidence) / 2.0, n - 1)
    return m, m - h, m + h


def run_silhouette_iteration(
    iteration, adata_sil, clusters, use_dim, res, n_pcs, n_neighbors
):
    random.seed(int(iteration) + (res * 10))
    subsample = random.sample(
        clusters.index.tolist(), int(float(len(adata_sil.obs)) * 0.8)
    )
    sub_adata = adata_sil[subsample].copy()
    del adata_sil

    sub_adata = sc.pp.neighbors(
        sub_adata,
        use_rep=use_dim,
        copy=True,
        random_state=0,
        n_pcs=n_pcs,
        n_neighbors=n_neighbors,
    )

    with nostdout():
        sub_clusters = pd.DataFrame(
            sc.tl.leiden(sub_adata, resolution=res, key_added="clusters", copy=True)
            .obs["clusters"]
            .tolist(),
            index=sub_adata.obs_names,
            columns=[iteration],
        )
        sys.stdout.close()
    clusters = clusters.merge(
        sub_clusters, how="left", left_index=True, right_index=True
    )[[iteration]]
    del sub_adata
    del sub_clusters
    gc.collect()
    return clusters


def calculate_silhouette_score(adata, clusters, num_iter, res, num_threads):
    logger.info("Prepare Co-Occurance Matrix")
    occ_mat = pd.DataFrame(
        index=range(0, len(adata.obs), 1),
        columns=range(0, len(adata.obs), 1),
        dtype=complex,
    ).fillna(complex(0, 0))
    occ_mat = scipy.sparse.csr_matrix(occ_mat.values).copy()

    logger.info("Counting Co-Occurances & Drop-Outs")
    if num_threads > 1:
        outer_complex_sym_iteration_fixed = partial(
            outer_complex_sym_iteration, x=clusters
        )
        occ_mats = process_map(
            outer_complex_sym_iteration_fixed,
            [str(x) for x in range(0, num_iter, 1)],
            max_workers=num_threads,
            desc="Tallying Iterations",
            maxinterval=5,
            mininterval=0.5,
            unit=" its",
        )

    else:
        pbar_co = tqdm(
            total=len(range(0, num_iter, 1)),
            desc="Number of Iterations Counted",
            maxinterval=5,
            mininterval=0.5,
            unit=" its",
            position=1,
        )
        occ_mats = []
        for iteration in [int(x) for x in range(0, num_iter, 1)]:
            occ_mats.append(
                outer_complex_sym_iteration(
                    iteration,
                    clusters.to_numpy(),
                )
            )

            pbar_co.update()
    occ_mats.insert(0, occ_mat)
    logger.info("Combining Results")
    occ_mat = reduce(lambda x, y: x + y, occ_mats)
    occ_mat = pd.DataFrame(occ_mat.toarray())
    del occ_mats

    logger.info("Resolving Co-Occurance Matrix")
    occ_mat = occ_mat.applymap(resolve_complex, num_iter=num_iter)

    logger.info("Create Dissimilarity Matrix")
    diss_mat = 1 - occ_mat

    occ_mat.columns = [str(column) for column in clusters["Truth"].tolist()]
    occ_mat.index = [str(column) for column in clusters["Truth"].tolist()]

    logger.info("Check Values on the Diagonal of the Dissimilarity Matrix")
    diag_series = pd.Series(np.diag(diss_mat), index=[diss_mat.index, diss_mat.columns])
    if len(diag_series[diag_series > 0]) > 0:
        logging.warning("Not all Values along the Diagonal are 0. Setting to 0")
        diss_mat = diss_mat.to_numpy()
        np.fill_diagonal(diss_mat, 0)
    else:
        logger.info("All Values along the Diagonal are 0")
        diss_mat = diss_mat.to_numpy()

    logger.info("Calculating and Storing Silhouette Values")
    sample_silhouette_values = silhouette_samples(
        diss_mat, clusters["Truth"].tolist(), metric="precomputed"
    )
    sil = pd.DataFrame(sample_silhouette_values, index=clusters.index)
    sil["cluster"] = clusters["Truth"].tolist()
    sil_mean = sil.groupby("cluster")[0].mean().to_frame(name="avg_silhouette")
    sil_mean["res"] = res

    logger.info("Calculate Average co-occurance per Cluster")
    grp = pd.DataFrame(
        index=set(clusters["Truth"].tolist()),
        columns=["avg_pct", "cluster_1", "ct_lower", "ct_upper"],
    )
    for cluster_1 in set(clusters["Truth"].tolist()):
        ids_1 = [int(x) == int(cluster_1) for x in occ_mat.index.tolist()]
        values = occ_mat.loc[ids_1, ids_1].values.flatten()
        mean = values.mean()
        ct = sms.DescrStatsW(values).tconfint_mean()
        grp.at[cluster_1, "avg_pct"] = mean
        grp.at[cluster_1, "ct_lower"] = ct[0]
        grp.at[cluster_1, "ct_upper"] = ct[1]
        grp.at[cluster_1, "cluster_1"] = cluster_1
    grp["res"] = res

    return [sil_mean, grp]


def calculate_stability_measures(
    adata_input,
    num_iter=100,
    layer="scale.data",
    prop=0.8,
    num_threads=4,
    res_num_threads=1,
    n_pcs=None,
    n_neighbors=15,
    use_dim="X_scVI",
    resolutions=[
        0.1,
        0.2,
        0.3,
        0.4,
        0.5,
        0.6,
        0.7,
        0.8,
        0.9,
        1,
        1.1,
        1.2,
        1.3,
        1.4,
        1.5,
        1.6,
        1.7,
        1.8,
        1.9,
        2,
    ],
):
    adata = adata_input.copy()
    adata.X = adata.layers[layer].copy()
    start_time = datetime.datetime.now()
    adata.obs = pd.DataFrame(index=adata.obs.index)
    adata.var = pd.DataFrame(index=adata.var.index)

    try:
        del adata.layers["spliced"]
    except Exception:
        logging.warning("'spliced' Assay not found in Adata. Skipping.")
    try:
        del adata.layers["unspliced"]
    except Exception:
        logging.warning("'unspliced' Assay not found in Adata. Skipping.")
    try:
        del adata.layers["data"]
    except Exception:
        logging.warning("'data' Assay not found in Adata. Skipping.")
    try:
        del adata.layers["counts"]
    except Exception:
        logging.warning("'counts' Assay not found in Adata. Skipping.")

    cluster_list = {}
    grp_list = {}
    sil_list = {}

    pbar_res = tqdm(
        total=len(resolutions),
        desc="Number of Resolutions",
        maxinterval=5,
        mininterval=0.5,
        unit=" resolutions",
        position=0,
    )

    for res in resolutions:
        logger.info("Calculating Ground Truth for {}".format(res))
        truth_adata = sc.pp.neighbors(
            adata,
            use_rep=use_dim,
            copy=True,
            random_state=0,
            n_pcs=n_pcs,
            n_neighbors=n_neighbors,
        )
        with nostdout():
            orig_clusters = pd.DataFrame(
                sc.tl.leiden(
                    truth_adata, resolution=res, key_added="clusters", copy=True
                )
                .obs["clusters"]
                .tolist(),
                columns=["Truth"],
                index=adata.obs_names,
            ).apply(pd.to_numeric, errors="ignore", downcast="integer")
            sys.stdout.close()
        del truth_adata

        if res_num_threads == 1:
            pbar_iter = tqdm(
                total=len(range(0, num_iter, 1)),
                desc="Number of Iterations",
                maxinterval=5,
                mininterval=0.5,
                unit=" its",
                position=1,
            )
            clusters_list = []
            for iteration in [str(x) for x in range(0, num_iter, 1)]:
                cluster_sub = run_silhouette_iteration(
                    adata_sil=adata,
                    clusters=orig_clusters,
                    iteration=iteration,
                    use_dim=use_dim,
                    res=res,
                    n_pcs=n_pcs,
                    n_neighbors=n_neighbors,
                )
                clusters_list.append(cluster_sub)
                pbar_iter.update()
        else:
            run_silhouette_iteration_fixed = partial(
                run_silhouette_iteration,
                adata_sil=adata,
                clusters=orig_clusters,
                use_dim=use_dim,
                res=res,
                n_pcs=n_pcs,
                n_neighbors=n_neighbors,
            )
            clusters_list = process_map(
                run_silhouette_iteration_fixed,
                [str(x) for x in range(0, num_iter, 1)],
                max_workers=num_threads,
                desc="Processing Clustering Iterations",
                maxinterval=5,
                mininterval=0.5,
                unit=" its",
            )

        clusters_list.insert(0, orig_clusters)
        clusters = pd.concat(clusters_list, axis=1, ignore_index=False)
        clusters = clusters.apply(pd.to_numeric, errors="ignore", downcast="integer")
        cluster_list[str(res)] = clusters
        sil_mean, grp = calculate_silhouette_score(
            adata=adata,
            clusters=clusters,
            num_iter=num_iter,
            res=res,
            num_threads=num_threads,
        )
        del clusters
        sil_list[str(res)] = sil_mean
        grp_list[str(res)] = grp
        gc.collect()
        pbar_res.update()
    end_time = datetime.datetime.now()
    logger.info(
        "Total Processing took: {duration}".format(duration=end_time - start_time)
    )
    return [cluster_list, grp_list, sil_list]


def bootstrap_median(
    df,
    confidence_level,
    n_resamples,
):
    rng = np.random.default_rng()
    data = (df.tolist(),)
    bootstrap_res = bootstrap(
        data,
        np.median,
        confidence_level=confidence_level,
        random_state=rng,
        n_resamples=n_resamples,
    )
    return list(bootstrap_res.confidence_interval)


def calculate_silhouette_stats(sil_list, confidence_level=0.95, n_resamples=2500):
    sil_list_concat = pd.concat([x for x in sil_list.values()])
    sil_list_concat["n_clusters"] = sil_list_concat.groupby("res").transform(len)
    sil_list_concat["median"] = sil_list_concat.groupby("res")[
        "avg_silhouette"
    ].transform("median")
    sil_list_concat["ct_upper"] = sil_list_concat.groupby("res")[
        "avg_silhouette"
    ].transform(
        lambda x: bootstrap_median(
            x, confidence_level=confidence_level, n_resamples=n_resamples
        )[1]
    )
    sil_list_concat["ct_lower"] = sil_list_concat.groupby("res")[
        "avg_silhouette"
    ].transform(
        lambda x: bootstrap_median(
            x, confidence_level=confidence_level, n_resamples=n_resamples
        )[0]
    )
    sil_list_concat = sil_list_concat.reset_index().astype({"res": "category"})
    median_silhouette_stats = (
        sil_list_concat.drop_duplicates(["res"])[
            ["res", "ct_lower", "median", "ct_upper", "n_clusters"]
        ]
        .reset_index(drop=True)
        .astype({"res": "category"})
    )
    return [median_silhouette_stats, sil_list_concat]


def give_optimal_resolution(median_silhouette_stats):
    threshold = max(median_silhouette_stats["ct_lower"])
    median_silhouette_stats = median_silhouette_stats.reset_index(drop=True)
    choice = (
        median_silhouette_stats[median_silhouette_stats["median"] >= threshold]
        .sort_values("n_clusters")
        .tail(1)
    )
    choice_index = choice.index.tolist()[0]
    choice = choice.reset_index(drop=True).at[0, "res"]
    return [float(threshold), float(choice_index), float(choice)]


def plot_silhouette_scores(
    median_silhouette_stats, silhouette_scores, threshold, choice_index
):
    g = (
        ggplot(median_silhouette_stats, aes("res", "median"))
        + geom_crossbar(aes(ymin="ct_lower", ymax="ct_upper"), fill="grey", size=0.25)
        + geom_hline(aes(yintercept=threshold), colour="blue")
        + geom_vline(aes(xintercept=choice_index + 1), colour="red")
        + geom_jitter(
            silhouette_scores, aes("res", "avg_silhouette"), size=0.35, width=0.15
        )
        + ylim(-1, 1)
        + theme_bw()
        + labs(y="Silhouette Score", x="Clustering Resolution")
    )
    return g


def plot_silhouette_optimal_cluster(silhouette_scores, choice):
    chosen_resolution_ordered = (
        silhouette_scores[silhouette_scores["res"] == choice]
        .sort_values("avg_silhouette", ascending=False)
        .astype({"cluster": "category"})
        .reset_index(drop=True)
    )
    chosen_resolution_ordered = chosen_resolution_ordered.assign(
        cluster=chosen_resolution_ordered["cluster"].cat.reorder_categories(
            chosen_resolution_ordered["cluster"].tolist()
        )
    )
    g = (
        ggplot(chosen_resolution_ordered, aes("cluster", "avg_silhouette"))
        + geom_point()
        + ylim(0, 1)
    )
    return g


def get_optimal_clustering(
    adata_input,
    num_iter=100,
    prop=0.8,
    num_threads=1,
    res_num_threads=1,
    n_pcs=None,
    n_neighbors=15,
    use_dim="X_pca",
    resolutions=[
        0.1,
        0.2,
        0.3,
        0.4,
        0.5,
        0.6,
        0.7,
        0.8,
        0.9,
        1,
        1.1,
        1.2,
        1.3,
        1.4,
        1.5,
    ],
):
    cluster_list, grp_list, sil_list = calculate_stability_measures(
        adata_input=adata_input,
        num_iter=num_iter,
        prop=prop,
        num_threads=num_threads,
        res_num_threads=res_num_threads,
        n_pcs=n_pcs,
        n_neighbors=n_neighbors,
        use_dim=use_dim,
        resolutions=resolutions,
    )

    median_silhouette_stats, sil_list_concat = calculate_silhouette_stats(
        sil_list, confidence_level=0.95, n_resamples=2500
    )

    threshold, choice_index, choice = give_optimal_resolution(median_silhouette_stats)

    optimal_plot = plot_silhouette_scores(
        median_silhouette_stats=median_silhouette_stats,
        silhouette_scores=sil_list_concat,
        threshold=threshold,
        choice_index=choice_index,
    )

    silhouette_of_optimal = plot_silhouette_optimal_cluster(
        silhouette_scores=sil_list_concat, choice=choice
    )

    return [
        cluster_list,
        grp_list,
        sil_list,
        median_silhouette_stats,
        sil_list_concat,
        threshold,
        choice_index,
        choice,
        optimal_plot,
        silhouette_of_optimal,
    ]


###
###
###
