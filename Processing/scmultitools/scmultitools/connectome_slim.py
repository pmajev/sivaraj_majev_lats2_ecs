import gc
import logging
import random
from functools import partial

import numpy as np
import pandas as pd
from pandas.api.types import CategoricalDtype
from scipy.stats import zscore
from statsmodels.stats.multitest import multipletests
from tqdm.autonotebook import tqdm
from tqdm.contrib.concurrent import process_map

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def shuffle_cols(x):
    for column in x.columns:
        x[column] = random.sample(x[column].tolist(), len(x[column]))
    return x


def calculate_matrices(
    raw_data_matrix,
    annotation,
    condition_col,
    cell_type_col,
    randomize=False,
    randomize_col=None,
    random_state=42,
    ddof=0,
    z_of_means=False,
):
    raw_matrix = raw_data_matrix.copy()

    raw_matrix[[cell_type_col, condition_col]] = annotation[
        [cell_type_col, condition_col]
    ]

    if randomize:
        if randomize_col == cell_type_col:
            random.seed(random_state)
            raw_matrix[randomize_col] = (
                raw_matrix[[randomize_col, condition_col]]
                .groupby([condition_col], sort=False)
                .apply(shuffle_cols)[randomize_col]
                .values
            )

        if randomize_col == condition_col:
            random.seed(random_state)
            raw_matrix[randomize_col] = (
                raw_matrix[[randomize_col, cell_type_col]]
                .groupby([cell_type_col], sort=False)
                .apply(shuffle_cols)[randomize_col]
                .values
            )

    mean_expression_matrix = (
        raw_matrix.groupby([cell_type_col, condition_col], sort=False)
        .mean()
        .fillna(0)
        .reset_index()
    )

    mean_expression_matrix = mean_expression_matrix.melt(
        [cell_type_col, condition_col], value_name="mean", var_name="Gene"
    )

    if z_of_means:
        mean_expression_matrix["mean"] = mean_expression_matrix.groupby(
            [condition_col, "Gene"], sort=False
        )["mean"].transform(lambda x: zscore(x, ddof=ddof))

    return mean_expression_matrix


def calculate_fraction(
    raw_data_matrix,
    annotation,
    features,
    condition_col,
    cell_type_col,
    layer="data",
    exp_cutoff=0,
):
    raw_data_matrix = raw_data_matrix.copy()
    annotation = annotation.copy()

    raw_matrix = raw_data_matrix > exp_cutoff
    raw_matrix[[cell_type_col, condition_col]] = annotation[
        [cell_type_col, condition_col]
    ]
    raw_matrix_grouped = raw_matrix.groupby([cell_type_col, condition_col], sort=False)
    fraction_expressed = raw_matrix_grouped.sum() / raw_matrix_grouped.count()
    fraction_expressed = fraction_expressed.T.reset_index().melt("index")
    fraction_expressed.columns = ["Gene", "Cluster", "Condition", "frac_expression"]

    fraction_expressed = fraction_expressed[fraction_expressed["Gene"].isin(features)]

    return fraction_expressed[
        ["Gene", "Cluster", "Condition", "frac_expression"]
    ].copy()


def get_all_interactions(raw_data_matrix, annotation, conditions, cell_type_col, LR_df):
    raw_data_matrix = raw_data_matrix.copy()
    annotation = annotation.copy()

    # conditions = list(set(annotation[condition_col].tolist()))

    all_combinations = annotation[cell_type_col].unique().tolist()

    x, y = pd.core.reshape.util.cartesian_product([all_combinations, all_combinations])

    all_combinations = pd.DataFrame({"comp_1": x, "comp_2": y})

    all_combinations = all_combinations.drop_duplicates().reset_index(drop=True)

    LR_df["LR"] = LR_df["source_genesymbol"] + "--" + LR_df["target_genesymbol"]

    all_combinations["Source_Target"] = (
        all_combinations["comp_1"] + "--" + all_combinations["comp_2"]
    )

    x, y = pd.core.reshape.util.cartesian_product(
        [
            all_combinations["Source_Target"].unique().tolist(),
            LR_df["LR"].unique().tolist(),
        ]
    )

    interactions_template = pd.DataFrame({"Source_Target": x, "Interactions": y})

    interactions_template[["Source", "Target"]] = interactions_template[
        "Source_Target"
    ].str.split("--", n=1, expand=True)
    interactions_template[["Ligand", "Receptor"]] = interactions_template[
        "Interactions"
    ].str.split("--", n=1, expand=True)
    interactions_template["Condition"] = [conditions] * len(interactions_template)
    interactions_template = interactions_template.explode("Condition")
    interactions_template = interactions_template.drop_duplicates(
        ["Source", "Target", "Ligand", "Receptor", "Condition"]
    )

    cat_type = CategoricalDtype(categories=conditions, ordered=True)
    interactions_template["Condition"] = interactions_template["Condition"].astype(
        cat_type
    )

    return interactions_template.sort_values(
        ["Source", "Target", "Ligand", "Receptor", "Condition"]
    ).copy()


def calculate_scores(
    interactions_template,
    conditions,
    mean_expression_matrix,
    cell_type_col,
    condition_col,
    perturbation_score=False,
    mean_score_diff=True,
    directionality=False,
    z_of_means=False,
    product=True,
    threshold_diff=0.05,
    scores="all",
):
    results_df = (
        interactions_template.merge(
            mean_expression_matrix,
            left_on=["Source", "Condition", "Ligand"],
            right_on=[cell_type_col, condition_col, "Gene"],
            how="left",
            validate="many_to_one",
        )
        .drop([cell_type_col, condition_col, "Gene"], axis=1)
        .rename({"mean": "Ligand_mean"}, axis=1)
    )
    results_df = (
        results_df.merge(
            mean_expression_matrix,
            left_on=["Target", "Condition", "Receptor"],
            right_on=[cell_type_col, condition_col, "Gene"],
            how="left",
            validate="many_to_one",
        )
        .drop([cell_type_col, condition_col, "Gene"], axis=1)
        .rename({"mean": "Receptor_mean"}, axis=1)
    )

    if product:
        results_df["mean_score"] = (
            results_df["Ligand_mean"] * results_df["Receptor_mean"]
        )
    else:
        results_df.loc[
            (results_df["Receptor_mean"] == 0),
            "Ligand_mean",
        ] = 0
        results_df.loc[
            (results_df["Ligand_mean"] == 0),
            "Receptor_mean",
        ] = 0
        results_df["mean_score"] = results_df[["Ligand_mean", "Receptor_mean"]].mean(
            axis=1
        )

    if scores == "all":
        # Direction Score
        if directionality:
            results_df[["Ligand_diff", "Receptor_diff"]] = (
                results_df[
                    ["Source_Target", "Interactions", "Ligand_mean", "Receptor_mean"]
                ]
                .groupby(["Source_Target", "Interactions"], sort=False)
                .transform(lambda x: x.diff(periods=-1).fillna(method="ffill", limit=1))
                .rename(
                    {"Ligand_mean": "Ligand_diff", "Receptor_mean": "Receptor_diff"},
                    axis=1,
                )[["Ligand_diff", "Receptor_diff"]]
            )

            results_df.loc[
                results_df["Ligand_diff"] > threshold_diff, "Ligand_diff_string"
            ] = "Up"
            results_df.loc[
                results_df["Ligand_diff"] < -threshold_diff, "Ligand_diff_string"
            ] = "Down"
            results_df.loc[
                (results_df["Ligand_diff"] > -threshold_diff)
                & (results_df["Ligand_diff"] < threshold_diff),
                "Ligand_diff_string",
            ] = "Unchanged"

            results_df.loc[
                results_df["Receptor_diff"] > threshold_diff, "Receptor_diff_string"
            ] = "Up"
            results_df.loc[
                results_df["Receptor_diff"] < -threshold_diff, "Receptor_diff_string"
            ] = "Down"
            results_df.loc[
                (results_df["Receptor_diff"] > -threshold_diff)
                & (results_df["Receptor_diff"] < threshold_diff),
                "Receptor_diff_string",
            ] = "Unchanged"

            results_df["directionality_score"] = (
                results_df["Ligand_diff_string"]
                + "_"
                + results_df["Receptor_diff_string"]
            )

    if scores == "all" or scores == "mean_score_diff":
        if perturbation_score:
            connectome_diff_score = results_df[
                ["Source_Target", "Interactions", "Ligand_mean", "Receptor_mean"]
            ].copy()

            np.seterr(divide="ignore")
            connectome_diff_score.loc[:, "Ligand_mean"] = np.nan_to_num(
                np.log2(connectome_diff_score["Ligand_mean"].to_numpy()),
                copy=True,
                nan=0,
                posinf=0,
                neginf=0,
            )
            connectome_diff_score.loc[:, "Receptor_mean"] = np.nan_to_num(
                np.log2(connectome_diff_score["Receptor_mean"].to_numpy()),
                copy=True,
                nan=0,
                posinf=0,
                neginf=0,
            )
            np.seterr(divide="warn")

            connectome_diff_score = connectome_diff_score.groupby(
                ["Source_Target", "Interactions"], sort=False
            ).transform(lambda x: x.diff(periods=-1).fillna(method="ffill", limit=1))

            connectome_diff_score["perturbation_score"] = connectome_diff_score[
                ["Ligand_mean", "Receptor_mean"]
            ].mean(axis=1)

            results_df["perturbation_score"] = connectome_diff_score[
                "perturbation_score"
            ]
            del connectome_diff_score
        if mean_score_diff:
            results_df[["mean_score_diff"]] = (
                results_df[["Source_Target", "Interactions", "mean_score"]]
                .groupby(["Source_Target", "Interactions"], sort=False)
                .transform(lambda x: x.diff(periods=-1).fillna(method="ffill", limit=1))
                .rename({"mean_score": "mean_score_diff"}, axis=1)[["mean_score_diff"]]
            )

    results_df = results_df
    return results_df


def iteration_results(
    random_state,
    raw_data_matrix,
    annotation,
    conditions,
    interactions_template,
    condition_col,
    cell_type_col,
    ddof=0,
    perturbation_score=False,
    directionality=False,
    z_of_means=False,
    product=True,
    threshold_diff=0.05,
):
    mean_expression_matrix = calculate_matrices(
        raw_data_matrix=raw_data_matrix,
        annotation=annotation,
        condition_col=condition_col,
        cell_type_col=cell_type_col,
        randomize=True,
        randomize_col=cell_type_col,
        random_state=random_state,
        ddof=ddof,
    )

    iter_results_final = calculate_scores(
        interactions_template=interactions_template,
        conditions=conditions,
        mean_expression_matrix=mean_expression_matrix,
        cell_type_col=cell_type_col,
        condition_col=condition_col,
        perturbation_score=False,
        mean_score_diff=False,
        z_of_means=z_of_means,
        directionality=directionality,
        product=product,
        threshold_diff=threshold_diff,
        scores="all",
    )

    mean_expression_matrix = calculate_matrices(
        raw_data_matrix=raw_data_matrix,
        annotation=annotation,
        condition_col=condition_col,
        cell_type_col=cell_type_col,
        randomize=True,
        randomize_col=condition_col,
        random_state=random_state,
        ddof=ddof,
    )

    iter_results = calculate_scores(
        interactions_template=interactions_template,
        conditions=conditions,
        mean_expression_matrix=mean_expression_matrix,
        cell_type_col=cell_type_col,
        condition_col=condition_col,
        perturbation_score=perturbation_score,
        mean_score_diff=True,
        z_of_means=z_of_means,
        directionality=directionality,
        product=product,
        threshold_diff=threshold_diff,
        scores="mean_score_diff",
    )

    if perturbation_score:
        iter_results_final[["mean_score_diff", "perturbation_score"]] = iter_results[
            ["mean_score_diff", "perturbation_score"]
        ].copy()
        del iter_results
        return iter_results_final[
            [
                "Ligand_mean",
                "Receptor_mean",
                "perturbation_score",
                "mean_score",
                "mean_score_diff",
            ]
        ]
    else:
        iter_results_final[["mean_score_diff"]] = iter_results[
            ["mean_score_diff"]
        ].copy()
        del iter_results
        return iter_results_final[
            [
                "Ligand_mean",
                "Receptor_mean",
                "mean_score",
                "mean_score_diff",
            ]
        ]


def calculate_LR_interactions(
    adata,
    LR_df,
    condition_col,
    condition_of_interest,
    cell_type_col,
    layer="data",
    exp_cutoff=0,
    frac_expression_cutoff=0.1,
    random_state=42,
    n_iterations=1000,
    n_process=5,
    chunk_size=10,
    alpha=0.05,
    z_of_means=False,
    output_frac=False,
    perturbation_score=False,
    directionality=False,
    product=True,
    threshold_diff=0.05,
):
    """\
    This function is heavily inspired by CellPhoneDB, Kumar, 2017 and the Connectome Paper, 2021.



    Parameters
    ----------
    adata
        AnnData containing the counts to be sued for LR inference
    layer
        Layer in 'adata' where counts are taken from
    LR_df
        A pandas data frame containing a columns 'source_genesymbol' and 'target_genesymbol' containing the gene symbols of the ligand and receptor respectively. Every row represents one interaction of a ligand with its receptor. The symbols have to be of the same type (e.g. MGI, HGNC ENSEMBL) as the one used in the '.var' dataframe of the 'adata' object.
    condition_col
        Column in '.obs' containing the conditions/treatments found in the data. Currently only datasets with 1 or two conditions are supported.
    condition_of_interest
        Name of the condition in 'condition_col' that is of interest. This means it is tested for an increased LR interaction in that condition compared to the remaining one
    cell_type_col
        Column in '.obs' containing the cell type / cluster annotations to be used
    exp_cutoff
        A gene has to have counts in 'layer' larger than this value to be considered expressed
    frac_expression_cutoff
        Percentage of cells that have to express a gene (Ligand or Receptor) above 'exp_cutoff' for it to be considered further
    random_state
        Seed to be used for initial calculations, later seeds depending on the respective iteration are used
    n_iterations
        Number of Permutations to calculate as background distribution
    n_process
        Number of Processes to use for calculating Permutations
    alpha
        Family-wise error rate used for p-value correction using Benjamini-Hochberg
    use_Halpern_score
        Whether to use the corrected z-score proposed in Haldern, 2018 (?) calculated as sqrt(square(zL)+ square(zR)   )
    perturbation_score
        Whether to calculate and test a perturbation score based on the Connectome Paper: log2FC(L)*log2FC(R)
    directionality
        Whether to output the directionality of expression changes across conditions for Ligand and Receptor (e.g. "Up_Up")
    z_of_means
        For the mean_score alternative z score, whether to calculate the z-score on the mean values of Ligand and Receptor or to calculate z-scores first and take the mean of these
    product
        Whether to use the product or the mean of Lmean and Rmean
    threshold_diff
        What max. diff to consider "unchanged"
    Returns
    -------
    A data frame with all relevant output parameters
    """

    if product and z_of_means:
        logger.error(
            "You are trying to calculate the product of the z of means as your output score. This is highly discouraged!!"
        )

    raw_data_matrix = pd.DataFrame(
        adata.layers[layer].toarray(),
        columns=adata.var_names.tolist(),
        index=adata.obs_names.tolist(),
    )

    annotation = adata.obs[[cell_type_col, condition_col]].copy()

    conditions = list(set(annotation[condition_col].tolist()))
    conditions.insert(0, conditions.pop(conditions.index(condition_of_interest)))

    logger.info("Number of Cells in Analysis: {}".format(adata.obs.shape[0]))

    logger.info("Load signalling Molecules")

    Ligands = list(set(LR_df["source_genesymbol"].tolist()))
    Receptors = list(set(LR_df["target_genesymbol"].tolist()))

    logger.info("Starting with Ligands:{}".format(len(Ligands)))
    logger.info("Starting with Receptors:{}".format(len(Receptors)))
    logger.info("Starting with n. LRs:{}".format(len(LR_df)))

    All_Molecules = list(set(Ligands + Receptors))

    logger.info("Determining Fractions of Cells expressing Ligands and Receptors")

    raw_data_matrix = raw_data_matrix.loc[
        :, raw_data_matrix.columns.isin(All_Molecules)
    ].copy()

    fraction_expressed = calculate_fraction(
        raw_data_matrix=raw_data_matrix,
        annotation=annotation,
        features=All_Molecules,
        cell_type_col=cell_type_col,
        condition_col=condition_col,
        layer=layer,
        exp_cutoff=0,
    )

    logger.info("Subsetting signalling Molecules")

    remaining_genes = list(set(fraction_expressed["Gene"].tolist()))

    LR_df = LR_df[
        (LR_df["source_genesymbol"].isin(remaining_genes))
        & (LR_df["target_genesymbol"].isin(remaining_genes))
    ].drop_duplicates(["source_genesymbol", "target_genesymbol"])

    Ligands = list(set(LR_df["source_genesymbol"].tolist()))
    Receptors = list(set(LR_df["target_genesymbol"].tolist()))

    logger.info("Remaining Ligands:{}".format(len(Ligands)))
    logger.info("Remaining Receptors:{}".format(len(Receptors)))
    logger.info("Remaining n. LRs:{}".format(len(LR_df)))

    All_Molecules = list(set(Ligands + Receptors))

    raw_data_matrix = raw_data_matrix.loc[
        :, raw_data_matrix.columns.isin(All_Molecules)
    ].copy()

    logger.info("Calculating all possible connections")
    interactions_template = get_all_interactions(
        raw_data_matrix=raw_data_matrix,
        annotation=annotation,
        conditions=conditions,
        cell_type_col=cell_type_col,
        LR_df=LR_df,
    )
    logger.info("Calculating initial expression values")

    mean_expression_matrix = calculate_matrices(
        raw_data_matrix=raw_data_matrix,
        annotation=annotation,
        condition_col=condition_col,
        cell_type_col=cell_type_col,
        randomize=False,
        randomize_col=None,
        random_state=42,
        z_of_means=z_of_means,
        ddof=0,
    )

    logger.info("Filter out lowly expressed Ligands & Receptors")

    interactions_template = (
        interactions_template.merge(
            mean_expression_matrix,
            left_on=["Source", "Condition", "Ligand"],
            right_on=[cell_type_col, condition_col, "Gene"],
            how="left",
            validate="many_to_one",
        )
        .drop([cell_type_col, condition_col, "Gene"], axis=1)
        .rename({"mean": "Ligand_mean"}, axis=1)
    )
    interactions_template = (
        interactions_template.merge(
            mean_expression_matrix,
            left_on=["Target", "Condition", "Receptor"],
            right_on=[cell_type_col, condition_col, "Gene"],
            how="left",
            validate="many_to_one",
        )
        .drop([cell_type_col, condition_col, "Gene"], axis=1)
        .rename({"mean": "Receptor_mean"}, axis=1)
    )

    interactions_template = (
        interactions_template.merge(
            fraction_expressed,
            left_on=["Source", "Condition", "Ligand"],
            right_on=["Cluster", "Condition", "Gene"],
            how="left",
            validate="many_to_one",
        )
        .drop(["Cluster", "Gene"], axis=1)
        .rename({"frac_expression": "Ligand_frac"}, axis=1)
    )

    interactions_template = (
        interactions_template.merge(
            fraction_expressed,
            left_on=["Target", "Condition", "Receptor"],
            right_on=["Cluster", "Condition", "Gene"],
            how="left",
            validate="many_to_one",
        )
        .drop(["Cluster", "Gene"], axis=1)
        .rename({"frac_expression": "Receptor_frac"}, axis=1)
    )

    # interactions_template = interactions_template.loc[(interactions_template["Ligand_frac"] >= frac_expression_cutoff) & (interactions_template["Receptor_frac"] >= frac_expression_cutoff) & (interactions_template["Ligand_mean"] >= 0) & (interactions_template["Receptor_mean"] >= 0),:]

    interactions_template = (
        interactions_template.groupby(["Source_Target", "Interactions"], sort=False)
        .filter(
            lambda x: (any(x["Ligand_frac"] >= frac_expression_cutoff))
            & (any(x["Receptor_frac"] >= frac_expression_cutoff))
            & (any(x["Ligand_mean"] >= 0))
            & (any(x["Receptor_mean"] >= 0))
            & (any((x["Ligand_mean"] >= 0) & (x["Receptor_mean"] >= 0)))
        )
        .reset_index()
    )

    interactions_template = interactions_template[
        [
            "Source_Target",
            "Interactions",
            "Source",
            "Target",
            "Ligand",
            "Receptor",
            "Condition",
        ]
    ]

    logger.info("Calculating initial Scores")

    initial_results = calculate_scores(
        interactions_template=interactions_template,
        conditions=conditions,
        mean_expression_matrix=mean_expression_matrix,
        cell_type_col=cell_type_col,
        condition_col=condition_col,
        perturbation_score=perturbation_score,
        directionality=directionality,
        z_of_means=z_of_means,
        product=product,
        threshold_diff=threshold_diff,
    )

    logger.info("Calculating Permutations (this can take a while)...")

    if n_process > 1:
        run_single_iteration = partial(
            iteration_results,
            raw_data_matrix=raw_data_matrix,
            annotation=annotation,
            conditions=conditions,
            interactions_template=interactions_template,
            condition_col=condition_col,
            cell_type_col=cell_type_col,
            ddof=0,
            perturbation_score=perturbation_score,
            directionality=False,
            z_of_means=z_of_means,
            product=product,
            threshold_diff=threshold_diff,
        )

        df_collection = process_map(
            run_single_iteration,
            range(n_iterations),
            max_workers=n_process,
            desc="Processing Permutations",
            maxinterval=1,
            mininterval=0.1,
            unit=" iter",
            chunksize=chunk_size,
        )

        del raw_data_matrix
        del interactions_template
        del annotation
        del conditions
        gc.collect()

    else:
        pbar = tqdm(
            total=n_iterations,
            desc="Number of Iterations",
            maxinterval=5,
            mininterval=0.5,
            unit=" iterations",
            position=1,
        )

        df_collection = dict()
        for i in range(0, n_iterations, 1):
            df_collection[i] = iteration_results(
                random_state=i,
                raw_data_matrix=raw_data_matrix,
                annotation=annotation,
                conditions=conditions,
                interactions_template=interactions_template,
                condition_col=condition_col,
                cell_type_col=cell_type_col,
                ddof=0,
                perturbation_score=perturbation_score,
                directionality=False,
                z_of_means=z_of_means,
                product=product,
                threshold_diff=threshold_diff,
            )
        pbar.update(1)

        del raw_data_matrix
        del interactions_template
        del annotation
        del conditions
        gc.collect()

    logger.info("Tallying Results...")
    combined_results = pd.concat(
        objs=df_collection,
        axis=1,
        join="outer",
        ignore_index=False,
        keys=range(len(df_collection)),
        levels=None,
        names=None,
        verify_integrity=False,
        copy=True,
    )

    del df_collection
    gc.collect()

    logger.info(" - Processing Mean Scores...")

    initial_results["z_mean_score"] = (
        initial_results["mean_score"]
        - combined_results.loc(axis=1)[:, "mean_score"].mean(axis=1)
    ) / combined_results.loc(axis=1)[:, "mean_score"].std(axis=1)

    initial_results["pval_mean_score"] = (
        combined_results.loc(axis=1)[:, "mean_score"]
        .ge(initial_results["mean_score"], axis=0)
        .sum(axis=1)
        / n_iterations
    )

    initial_results["padj_mean_score"] = multipletests(
        initial_results["pval_mean_score"],
        alpha=alpha,
        method="fdr_bh",
        is_sorted=False,
        returnsorted=False,
    )[1].tolist()

    logger.info(" - Processing Mean Score Differences...")

    initial_results["z_mean_score_diff"] = (
        initial_results["mean_score_diff"]
        - combined_results.loc(axis=1)[:, "mean_score_diff"].mean(axis=1)
    ) / combined_results.loc(axis=1)[:, "mean_score_diff"].std(axis=1)

    initial_results["pval_mean_score_diff_bigger"] = (
        combined_results.loc(axis=1)[:, "mean_score_diff"]
        .ge(initial_results["mean_score_diff"], axis=0)
        .sum(axis=1)
        / n_iterations
    )

    initial_results["pval_mean_score_diff_lower"] = (
        combined_results.loc(axis=1)[:, "mean_score_diff"]
        .le(initial_results["mean_score_diff"], axis=0)
        .sum(axis=1)
        / n_iterations
    )

    initial_results["padj_mean_score_diff_bigger"] = multipletests(
        initial_results["pval_mean_score_diff_bigger"],
        alpha=alpha,
        method="fdr_bh",
        is_sorted=False,
        returnsorted=False,
    )[1].tolist()

    initial_results["padj_mean_score_diff_lower"] = multipletests(
        initial_results["pval_mean_score_diff_lower"],
        alpha=alpha,
        method="fdr_bh",
        is_sorted=False,
        returnsorted=False,
    )[1].tolist()

    initial_results.loc[
        initial_results["mean_score_diff"] < 0, "pval_mean_score_diff"
    ] = initial_results.loc[
        initial_results["mean_score_diff"] < 0, "pval_mean_score_diff_lower"
    ].copy()
    initial_results.loc[
        initial_results["mean_score_diff"] > 0, "pval_mean_score_diff"
    ] = initial_results.loc[
        initial_results["mean_score_diff"] > 0, "pval_mean_score_diff_bigger"
    ].copy()

    initial_results.loc[
        initial_results["mean_score_diff"] < 0, "padj_mean_score_diff"
    ] = initial_results.loc[
        initial_results["mean_score_diff"] < 0, "padj_mean_score_diff_lower"
    ].copy()
    initial_results.loc[
        initial_results["mean_score_diff"] > 0, "padj_mean_score_diff"
    ] = initial_results.loc[
        initial_results["mean_score_diff"] > 0, "padj_mean_score_diff_bigger"
    ].copy()

    initial_results = initial_results.drop(
        [
            "pval_mean_score_diff_lower",
            "pval_mean_score_diff_bigger",
            "padj_mean_score_diff_lower",
            "padj_mean_score_diff_bigger",
        ],
        axis=1,
    )

    if perturbation_score:
        logger.info(" - Processing Perturbation Score...")
        initial_results["z_perturbation_score"] = (
            initial_results["perturbation_score"]
            - combined_results.loc(axis=1)[:, "perturbation_score"].mean(axis=1)
        ) / combined_results.loc(axis=1)[:, "perturbation_score"].std(axis=1)

        initial_results["pval_perturbation_score_bigger"] = (
            combined_results.loc(axis=1)[:, "perturbation_score"]
            .ge(initial_results["perturbation_score"], axis=0)
            .sum(axis=1)
            / n_iterations
        )

        initial_results["pval_perturbation_score_lower"] = (
            combined_results.loc(axis=1)[:, "perturbation_score"]
            .le(initial_results["perturbation_score"], axis=0)
            .sum(axis=1)
            / n_iterations
        )

        initial_results["padj_perturbation_score_bigger"] = multipletests(
            initial_results["pval_perturbation_score_bigger"],
            alpha=alpha,
            method="fdr_bh",
            is_sorted=False,
            returnsorted=False,
        )[1].tolist()

        initial_results["padj_perturbation_score_lower"] = multipletests(
            initial_results["pval_perturbation_score_lower"],
            alpha=alpha,
            method="fdr_bh",
            is_sorted=False,
            returnsorted=False,
        )[1].tolist()

        initial_results.loc[
            initial_results["perturbation_score"] < 0, "pval_perturbation_score"
        ] = initial_results.loc[
            initial_results["perturbation_score"] < 0, "pval_perturbation_score_lower"
        ].copy()
        initial_results.loc[
            initial_results["perturbation_score"] > 0, "pval_perturbation_score"
        ] = initial_results.loc[
            initial_results["perturbation_score"] > 0, "pval_perturbation_score_bigger"
        ].copy()

        initial_results.loc[
            initial_results["perturbation_score"] < 0, "padj_perturbation_score"
        ] = initial_results.loc[
            initial_results["perturbation_score"] < 0, "padj_perturbation_score_lower"
        ].copy()
        initial_results.loc[
            initial_results["perturbation_score"] > 0, "padj_perturbation_score"
        ] = initial_results.loc[
            initial_results["perturbation_score"] > 0, "padj_perturbation_score_bigger"
        ].copy()

        initial_results = initial_results.drop(
            [
                "pval_perturbation_score_lower",
                "pval_perturbation_score_bigger",
                "padj_perturbation_score_lower",
                "padj_perturbation_score_bigger",
            ],
            axis=1,
        )

    logger.info(" - Processing Fraction of Expression...")
    initial_results = (
        initial_results.merge(
            fraction_expressed,
            left_on=["Source", "Condition", "Ligand"],
            right_on=["Cluster", "Condition", "Gene"],
            validate="many_to_one",
        )
        .drop(["Gene", "Cluster"], axis=1)
        .rename({"frac_expression": "Ligand_frac"}, axis=1)
        .merge(
            fraction_expressed,
            left_on=["Target", "Condition", "Receptor"],
            right_on=["Cluster", "Condition", "Gene"],
            validate="many_to_one",
        )
        .drop(["Gene", "Cluster"], axis=1)
        .rename({"frac_expression": "Receptor_frac"}, axis=1)
    ).sort_values(["Source_Target", "Interactions", "Condition"])

    initial_results["Ligand_frac"] = initial_results["Ligand_frac"].fillna(0)
    initial_results["Receptor_frac"] = initial_results["Receptor_frac"].fillna(0)

    if z_of_means:
        initial_results = initial_results.rename(
            {
                "Ligand_mean": "Ligand_zmean",
                "Receptor_mean": "Receptor_zmean",
                "mean_score": "zmean_score",
            },
            axis=1,
        )
    if output_frac:
        return (initial_results, fraction_expressed)
    else:
        return initial_results
