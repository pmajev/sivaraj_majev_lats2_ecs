import copy
import datetime
import gc
import logging
import math
import random
from functools import reduce

import igraph as ig
import kneed
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import scanpy as sc
import scipy
import seaborn as sns
import statsmodels.stats.api as sms
from plotnine import *
from plotnine.data import *
from scipy.cluster.hierarchy import fcluster, linkage
from scipy.spatial.distance import squareform
from scipy.stats import bootstrap, t, zscore
from scmultitools.helpers import seed_everything
from sklearn.cluster import KMeans, SpectralCoclustering
from sklearn.metrics import silhouette_samples
from sklearn_extra.cluster import KMedoids
from statsmodels.stats.multitest import multipletests
from tqdm.autonotebook import tqdm

logger = logging.getLogger(__name__)


def cluster_correlation(
    data=None,
    gene_corr=None,
    gene_corr_long_subset=None,
    cluster_alg="leiden",
    leiden_resolution=1,
    leiden_iterations=-1,
    make_leiden_knn=False,
    use_abs=False,
    n_clusters=5,
    n_neighbours=30,
    walktrap_steps=4,
    min_n_genes=0,
    plot=False,
    random_seed=42,
):
    if cluster_alg == "hierarchical":
        logger.info(
            "Clustering using hierarchical clustering aiming for {} clusters".format(
                n_clusters
            )
        )
        knn_matrix = gene_corr.loc[
            gene_corr.index.isin(gene_corr_long_subset["to"])
            | gene_corr.columns.isin(gene_corr_long_subset["from"]),
            gene_corr.index.isin(gene_corr_long_subset["to"])
            | gene_corr.columns.isin(gene_corr_long_subset["from"]),
        ]
        gene_names = knn_matrix.index.tolist()

        # dist = distance.pdist(knn_matrix)
        # linkage_calced = linkage(dist, method='complete')

        dist = squareform(np.sqrt(1 - np.abs(knn_matrix.values)))
        linkage_calced = linkage(dist, method="complete")

        idx = fcluster(linkage_calced, n_clusters, "maxclust")

        gene_memberships = pd.DataFrame(gene_names, columns=["gene"])
        gene_memberships["membership"] = idx

        gene_memberships = gene_memberships.groupby(["membership"]).filter(
            lambda x: len(x) > min_n_genes
        )
        gene_memberships["n_members"] = gene_memberships.groupby(["membership"])[
            "gene"
        ].transform(lambda x: len(x))

    if cluster_alg == "kmedoids":
        logger.info(
            "Clustering using hierarchical clustering aiming for {} clusters".format(
                n_clusters
            )
        )

        knn_matrix = gene_corr.loc[
            gene_corr.index.isin(gene_corr_long_subset["to"]),
            gene_corr.columns.isin(gene_corr_long_subset["from"]),
        ]

        knn_matrix = np.sqrt(1 - abs(knn_matrix))

        print(knn_matrix.shape)

        kmeans = KMedoids(
            n_clusters=n_clusters,
            metric="precomputed",
            method="pam",
            init="k-medoids++",
            max_iter=300,
            random_state=random_seed,
        ).fit(knn_matrix)

        gene_memberships = pd.DataFrame(knn_matrix.index.tolist(), columns=["gene"])
        gene_memberships["membership"] = kmeans.labels_.tolist()

        gene_memberships = gene_memberships.groupby(["membership"]).filter(
            lambda x: len(x) > min_n_genes
        )
        gene_memberships["n_members"] = gene_memberships.groupby(["membership"])[
            "gene"
        ].transform(lambda x: len(x))

        del knn_matrix

    if cluster_alg == "kmeans":
        knn_matrix = data.loc[
            :,
            data.columns.isin(gene_corr_long_subset["from"])
            | data.columns.isin(gene_corr_long_subset["to"]),
        ]

        print(knn_matrix.shape)

        logger.info("Clustering using K-Means clustering with k={}".format(n_clusters))

        kmeans = KMeans(n_clusters=n_clusters, random_state=random_seed).fit(
            knn_matrix.T
        )

        gene_memberships = pd.DataFrame(knn_matrix.columns.tolist(), columns=["gene"])
        gene_memberships["membership"] = kmeans.labels_.tolist()

        gene_memberships = gene_memberships.groupby(["membership"]).filter(
            lambda x: len(x) > min_n_genes
        )
        gene_memberships["n_members"] = gene_memberships.groupby(["membership"])[
            "gene"
        ].transform(lambda x: len(x))

        del knn_matrix

    if cluster_alg == "spectral":
        knn_matrix = gene_corr.loc[
            gene_corr.index.isin(gene_corr_long_subset["from"])
            | gene_corr.index.isin((gene_corr_long_subset["to"])),
            gene_corr.columns.isin(gene_corr_long_subset["from"])
            | gene_corr.columns.isin((gene_corr_long_subset["to"])),
        ]

        logger.info(
            "Performing Spectral Clustering with n_clusters={n_clusters} and n_neighbours={n_neighbours}".format(
                n_clusters=n_clusters, n_neighbours=n_neighbours
            )
        )

        spec_clus = SpectralCoclustering(
            n_clusters=n_clusters,
            svd_method="arpack",
            mini_batch=False,
            n_init=500,
            random_state=random_seed,
        ).fit(knn_matrix)

        gene_memberships = pd.DataFrame(knn_matrix.index.tolist(), columns=["gene"])
        gene_memberships["membership"] = spec_clus.row_labels_.tolist()

        gene_memberships = gene_memberships.groupby(["membership"]).filter(
            lambda x: len(x) > min_n_genes
        )
        gene_memberships["n_members"] = gene_memberships.groupby(["membership"])[
            "gene"
        ].transform(lambda x: len(x))

        knn_matrix = knn_matrix.iloc[
            np.argsort(spec_clus.row_labels_), np.argsort(spec_clus.column_labels_)
        ]

        if plot:
            sns.heatmap(knn_matrix, cmap="vlag", vmax=1, vmin=-1, square=True)
            plt.show()

        del knn_matrix

    if cluster_alg == "leiden" or cluster_alg == "walktrap" or cluster_alg == "louvain":
        if make_leiden_knn:
            logger.info("Reducing Graph to k-NN with k={}".format(n_neighbours))

            if use_abs:
                gene_corr_long_subset["r"] = abs(gene_corr_long_subset["r"])

            gene_corr_long_subset = gene_corr_long_subset.sort_values(
                "r", ascending=False
            )

            gene_corr_long_subset = gene_corr_long_subset.groupby("from").head(
                n_neighbours
            )

        logger.info("Creating Graph from correlation values...")
        graph = ig.Graph.DataFrame(gene_corr_long_subset.iloc[:, :3], directed=False)

        logger.info(
            "Clustering Graph at resolution {} using Leiden clustering...".format(
                leiden_resolution
            )
        )

        seed_everything(random_seed)

        if cluster_alg == "leiden":
            clustered_graph = graph.community_leiden(
                objective_function="modularity",
                weights="r",
                resolution_parameter=leiden_resolution,
                beta=0.01,
                initial_membership=None,
                n_iterations=leiden_iterations,
                node_weights=None,
            )

        if cluster_alg == "louvain":
            clustered_graph = graph.community_multilevel(
                weights="r", return_levels=False
            )

        if cluster_alg == "walktrap":
            clustered_graph = graph.community_walktrap(steps=walktrap_steps)

            clustered_graph = clustered_graph.as_clustering()

        logger.info("Associating genes with their clusters...")
        gene_memberships = graph.get_vertex_dataframe()
        gene_memberships["membership"] = clustered_graph.membership
        gene_memberships = gene_memberships.groupby(["membership"]).filter(
            lambda x: len(x) > min_n_genes
        )
        gene_memberships["n_members"] = gene_memberships.groupby(["membership"])[
            "name"
        ].transform(lambda x: len(x))
        gene_memberships["gene"] = gene_memberships["name"]

    gene_memberships = gene_memberships.sort_values("membership", ascending=True)

    return gene_memberships


def GeneCorr_Analysis(
    adata,
    layer="data",
    sample_col="GROUP",
    zscale_data=True,
    reference_sample=None,
    average_by=None,
    min_counts=50,
    min_cells=10,
    alpha=0.05,
    only_use_genes=None,
    n_hvg=None,
    padj_cutoff=0.01,
    r_cutoff=0.5,
    use_abs=False,
    corr_method="pearson",
    cluster_alg="leiden",
    leiden_resolution=1,
    optimize_clustering=False,
    optimize_iter=100,
    optimize_resolutions=[
        0.1,
        0.5,
        1,
        1.5,
        2,
        2.5,
        3,
        3.5,
        4,
        4.5,
        5,
        5.5,
        6,
        6.5,
        7,
        7.5,
        8,
        8.5,
        9,
        9.5,
        10,
    ],
    leiden_iterations=-1,
    make_leiden_knn=False,
    n_clusters=5,
    n_neighbours=30,
    walktrap_steps=4,
    fdr_method="fdr_bh",
    plot=False,
    min_n_genes=10,
    random_seed=42,
    return_data=False,
):
    adata = adata.copy()
    adata.X = adata.layers[layer].copy()

    logger.info("Removing Genes with less than {} counts...".format(min_counts))
    sc.pp.filter_genes(adata, min_counts=min_counts)

    logger.info("Removing Genes expressed in less than {} cells...".format(min_counts))
    sc.pp.filter_genes(adata, min_cells=min_cells)

    logger.info("Extract data...")
    data = pd.DataFrame(adata.layers[layer].toarray())

    data.columns = adata.var_names
    data.index = adata.obs_names

    if average_by is not None:
        logger.info("Average data across column '{}'...".format(average_by))
        data[[average_by, sample_col]] = adata.obs[[average_by, sample_col]]

        data = data.groupby([sample_col, average_by]).mean().reset_index()
        data.index = data[average_by].astype(str) + "_" + data[sample_col].astype(str)
        data = data.drop([average_by, sample_col], axis=1)

    if zscale_data:
        logger.info("Z-scale data...")
        data = zscore(data, axis=0)

    if only_use_genes == "hvg":
        if type(n_hvg) != int:
            raise ValueError("n_hvg is not numeric")

        logger.info("Calculating highly variable Genes...")
        sc.pp.highly_variable_genes(
            adata,
            layer=layer,
            n_top_genes=n_hvg,
            subset=True,
            inplace=True,
            batch_key=sample_col,
            check_values=True,
            flavor="seurat",
        )

        only_use_genes = adata.var_names.tolist()

    elif isinstance(only_use_genes, list):
        if len(only_use_genes) == 0:
            raise ValueError("Something went wrong, no HVGs provided!")
    else:
        only_use_genes = adata.var_names.tolist()

    logger.info("Subsetting data to {} Genes...".format(len(only_use_genes)))
    data = data.loc[:, data.columns.isin(only_use_genes).tolist()]
    n_genes_in = data.shape[1]

    if n_genes_in <= 5000:
        logger.info("Using {} Genes for further Analysis".format(str(n_genes_in)))
    else:
        logger.info(
            "Using {} Genes for further Analysis. This is a lot and each further step may take a long time!".format(
                str(n_genes_in)
            )
        )

    logger.info("Prepare Data for further Analyses...")

    data_long = (
        data.reset_index()
        .melt(id_vars="index")
        .drop_duplicates()
        .rename({"index": "metacell", "variable": "gene", "value": "z_score"}, axis=1)
    )

    if reference_sample is not None:
        data_save = data.copy()

        data = data[data.index.str.contains(reference_sample)]

        data_long_save = data_long.copy()
        data_long = data_long[data_long["metacell"].str.contains(reference_sample)]

    n = data.shape[0]

    logger.info("Correlating Genes using {} Correlation...".format(corr_method))
    gene_corr = data.corr(method=corr_method).replace(
        {np.nan: 0, np.inf: 1, -np.inf: -1}
    )

    logger.info("Ensuring absolute symmetry...")
    gene_corr = (gene_corr + gene_corr.T) / 2
    np.fill_diagonal(gene_corr.values, 1)

    if plot:
        logger.info("Plotting Correlation Heatmap BEFORE clustering...")
        sns.heatmap(gene_corr, cmap="vlag", vmax=1, vmin=-1, square=True)
        plt.show()

    logger.info(
        "Found {n} Samples in data, meaning {df} degrees of freedom. Transforming to long format (might take long)...".format(
            n=n, df=n - 2
        )
    )

    gene_corr_long = (
        gene_corr.reset_index()
        .melt(id_vars="index")
        .drop_duplicates()
        .rename({"index": "from", "variable": "to", "value": "r"}, axis=1)
    )

    gene_corr_long = gene_corr_long[(gene_corr_long["from"] != gene_corr_long["to"])]

    logger.info("Calculating t-scores...")

    gene_corr_long["t"] = gene_corr_long["r"] * np.sqrt(
        (n - 2) / (1 - gene_corr_long["r"] ** 2)
    )

    gene_corr_long["t"] = np.where(
        (gene_corr_long["t"].isin([np.nan])) & (gene_corr_long["r"] >= 1),
        gene_corr_long["t"].max(skipna=True),
        gene_corr_long["t"],
    )
    gene_corr_long["t"] = np.where(
        (gene_corr_long["t"].isin([np.nan])) & (gene_corr_long["r"] == 0),
        0,
        gene_corr_long["t"],
    )

    logger.info("Calculating p-values...")

    gene_corr_long["pval"] = np.where(
        gene_corr_long["t"] <= 0,
        1 - t.sf(x=gene_corr_long["t"], df=n - 2, scale=1),
        t.sf(x=gene_corr_long["t"], df=n - 2, scale=1),
    )

    logger.info("Correcting p-values...")

    gene_corr_long["padj"] = multipletests(
        gene_corr_long["pval"],
        alpha=alpha,
        method=fdr_method,
        is_sorted=False,
        returnsorted=False,
    )[1].tolist()

    if plot:
        logger.info("Plotting histogramm of correlation scores")
        gene_corr_long.hist("r", bins=100)

    logger.info(
        "Subsetting Correlations by adjusted p-value ({padj_cutoff}) and Correlation Coefficient ({r_cutoff})".format(
            padj_cutoff=padj_cutoff, r_cutoff=r_cutoff
        )
    )

    if use_abs:
        gene_corr_long_subset = gene_corr_long[
            (gene_corr_long["padj"] <= padj_cutoff)
            & (abs(gene_corr_long["r"]) >= r_cutoff)
        ]
    else:
        gene_corr_long_subset = gene_corr_long[
            (gene_corr_long["padj"] <= padj_cutoff)
            & ((gene_corr_long["r"]) >= r_cutoff)
        ]

    logger.info("Removing redundant edges and sorting by Gene name...")
    m = ~pd.DataFrame(
        np.sort(gene_corr_long_subset[["from", "to"]], axis=1)
    ).duplicated()
    gene_corr_long_subset = gene_corr_long_subset.loc[m.tolist(), :]

    gene_corr_long_subset = gene_corr_long_subset.sort_values(
        ["from", "to"]
    ).reset_index(drop=True)

    logger.info(
        " - Largest adj. p-value remaining: {}".format(
            gene_corr_long_subset["padj"].max()
        )
    )
    logger.info(
        " - Smallest Correlation Coefficient remaining: {}".format(
            gene_corr_long_subset["r"].min()
        )
    )

    if plot:
        logger.info("Plotting histogramm of filtered correlation scores")
        gene_corr_long_subset.hist("r", bins=100)
        plt.show()

    if optimize_clustering and cluster_alg == "leiden":
        leiden_resolution, optimal_plot = get_optimal_clustering(
            gene_corr_long_subset,
            num_iter=optimize_iter,
            prop=0.8,
            resolutions=optimize_resolutions,
            make_leiden_knn=make_leiden_knn,
            n_neighbours=n_neighbours,
            random_seed=random_seed,
            leiden_iterations=leiden_iterations,
            cluster_alg="leiden",
            plot=plot,
        )

        if plot:
            optimal_plot.draw(show=True)

        logger.info(
            "Maximum optimal leiden resolution was determined to be: {}".format(
                leiden_resolution
            )
        )

    if optimize_clustering and cluster_alg == "kmeans":
        knn_matrix = data.loc[
            :,
            data.columns.isin(gene_corr_long_subset["from"])
            | data.columns.isin(gene_corr_long_subset["to"]),
        ]

        pbar = tqdm(
            total=len(range(1, 100, 1)),
            desc="Calculating K",
            maxinterval=5,
            mininterval=0.5,
            unit=" its",
            position=1,
        )
        Sum_of_squared_distances = {"K": [], "Sum of squared distances": []}
        for k in range(1, 100, 1):
            kmeans = KMeans(
                n_clusters=k, n_init=100, max_iter=300, random_state=random_seed
            ).fit(knn_matrix.T)
            Sum_of_squared_distances["K"].append(k)
            Sum_of_squared_distances["Sum of squared distances"].append(kmeans.inertia_)

            pbar.update()
        pbar.close()

        Sum_of_squared_distances_df = pd.DataFrame(Sum_of_squared_distances)

        plt.plot(
            Sum_of_squared_distances_df["K"].tolist(),
            Sum_of_squared_distances_df["Sum of squared distances"].tolist(),
            "bx-",
        )
        plt.xlabel("k")
        plt.ylabel("Sum_of_squared_distances")
        plt.title("Elbow Method For Optimal k")
        plt.show()

        kneedle = kneed.KneeLocator(
            x=Sum_of_squared_distances_df["K"].tolist(),
            y=Sum_of_squared_distances_df["Sum of squared distances"].tolist(),
            S=1.0,
            curve="convex",
            direction="decreasing",
        )

        n_clusters = int(round(kneedle.knee, 1))

        logger.info(
            "Maximum optimal k for KMeans was determined to be: {}".format(n_clusters)
        )

    gene_memberships = cluster_correlation(
        data=data,
        gene_corr=gene_corr.copy(),
        gene_corr_long_subset=gene_corr_long_subset.copy(),
        cluster_alg=cluster_alg,
        leiden_resolution=leiden_resolution,
        leiden_iterations=leiden_iterations,
        make_leiden_knn=make_leiden_knn,
        n_clusters=n_clusters,
        n_neighbours=n_neighbours,
        walktrap_steps=walktrap_steps,
        min_n_genes=min_n_genes,
        use_abs=use_abs,
        plot=plot,
        random_seed=random_seed,
    )

    gene_corr = gene_corr.loc[
        gene_corr.index.isin(gene_memberships["gene"]),
        gene_corr.columns.isin(gene_memberships["gene"]),
    ].copy()
    gene_corr = gene_corr.loc[gene_memberships["gene"], gene_memberships["gene"]].copy()

    if plot:
        logger.info("Plotting Correlation Heatmap AFTER clustering...")
        sns.heatmap(gene_corr, cmap="vlag", vmax=1, vmin=-1, square=True)
        plt.show()

    logger.info(
        "Final number of clusters: {}".format(
            len(gene_memberships["membership"].unique())
        )
    )

    logger.info("All Done!")

    if reference_sample is not None:
        data = data_save.copy()
        data_long = data_long_save.copy()

    if return_data:
        data_long = data_long.merge(
            gene_memberships, how="left", left_on="gene", right_on="gene"
        ).dropna()
        gene_clusters = (
            data_long[["gene", "membership", "n_members"]]
            .drop_duplicates()
            .rename({"gene": "Gene", "membership": "clusters"}, axis=1)
        )

        genes_background = pd.DataFrame(only_use_genes, columns=["Gene"])

        gene_corr_long_subset = gene_corr_long_subset.merge(
            gene_memberships, how="left", left_on="from", right_on="gene"
        ).rename(
            {"from": "Source", "to": "Target", "r": "Weight", "membership": "clusters"},
            axis=1,
        )

        return (gene_clusters, data_long, gene_corr_long_subset, genes_background)
    else:
        return gene_clusters


def outer_complex(x, y):
    x = x
    y = y
    nans = [x for x in np.isnan(y)]
    arr = x == y
    arr = arr.astype(complex)
    arr = pd.DataFrame(arr)
    arr.loc[nans, nans] = complex(0, 1)
    arr = scipy.sparse.csr_matrix(arr.values)
    return arr


def outer_complex_sym_iteration(iteration, x):
    y = copy.deepcopy(x)
    x = x[:, iteration][:, None]
    y = y[:, iteration]
    nans = [x for x in np.isnan(y)]
    arr = x == y
    del x
    del y
    gc.collect()
    arr = arr.astype(complex)
    arr = pd.DataFrame(arr)
    arr.loc[nans, nans] = complex(0, 1)
    arr = scipy.sparse.csr_matrix(arr.values)
    gc.collect()
    return arr


def resolve_complex(x, num_iter):
    if x.real > 0:
        x = x.real / (num_iter - x.imag)
    else:
        x = 0
    return x


def make_dissmat(x):
    x = 1 - x
    return x


def confidence_interval(data, confidence=0.95):
    a = 1.0 * np.array(data)
    n = len(a)
    m, se = np.mean(a), scipy.stats.sem(a)
    h = se * scipy.stats.t.ppf((1 + confidence) / 2.0, n - 1)
    return m, m - h, m + h


def run_silhouette_iteration(
    iteration,
    edge_df,
    clusters,
    cluster_alg="leiden",
    leiden_resolution=1,
    leiden_iterations=1000,
    make_leiden_knn=False,
    n_neighbours=30,
    walktrap_steps=4,
    random_seed=42,
    plot=False,
):
    random.seed(int(iteration) + (leiden_resolution * 10))

    subsample = random.sample(
        clusters.index.tolist(), int(float(clusters.shape[0]) * 0.8)
    )

    sub_edge_df = edge_df[
        (edge_df["from"].isin(subsample)) | (edge_df["to"].isin(subsample))
    ].copy()

    sub_clusters = cluster_correlation(
        gene_corr_long_subset=sub_edge_df,
        cluster_alg=cluster_alg,
        leiden_resolution=leiden_resolution,
        leiden_iterations=leiden_iterations,
        make_leiden_knn=make_leiden_knn,
        n_neighbours=n_neighbours,
        walktrap_steps=walktrap_steps,
        plot=plot,
        random_seed=random_seed,
    ).apply(pd.to_numeric, errors="ignore", downcast="integer")[["name", "membership"]]

    sub_clusters.index = sub_clusters["name"].tolist()

    sub_clusters = sub_clusters[["membership"]].rename(
        {"membership": iteration}, axis=1
    )

    clusters_merged = clusters.merge(
        sub_clusters, how="left", left_index=True, right_index=True
    )[[iteration]].copy()

    return clusters_merged


def calculate_silhouette_score(edge_df, clusters, num_iter, res):
    logger.info("Prepare Co-Occurance Matrix")
    occ_mat = pd.DataFrame(
        index=range(0, clusters.shape[0], 1),
        columns=range(0, clusters.shape[0], 1),
        dtype=complex,
    ).fillna(complex(0, 0))

    occ_mat = scipy.sparse.csr_matrix(occ_mat.values).copy()

    logger.info("Counting Co-Occurances & Drop-Outs")

    pbar_co = tqdm(
        total=len(range(0, num_iter, 1)),
        desc="Number of Iterations Counted",
        maxinterval=5,
        mininterval=0.5,
        unit=" its",
        position=1,
    )
    occ_mats = []
    for iteration in [int(x) for x in range(0, num_iter, 1)]:
        occ_mats.append(
            outer_complex_sym_iteration(
                iteration,
                clusters.to_numpy(),
            )
        )

        pbar_co.update()

    pbar_co.close()
    occ_mats.insert(0, occ_mat)
    logger.info("Combining Results")
    occ_mat = reduce(lambda x, y: x + y, occ_mats)
    occ_mat = pd.DataFrame(occ_mat.toarray())
    del occ_mats

    logger.info("Resolving Co-Occurance Matrix")
    occ_mat = occ_mat.applymap(resolve_complex, num_iter=num_iter)

    logger.info("Create Dissimilarity Matrix")
    diss_mat = 1 - occ_mat

    occ_mat.columns = [str(column) for column in clusters["Truth"].tolist()]
    occ_mat.index = [str(column) for column in clusters["Truth"].tolist()]

    logger.info("Check Values on the Diagonal of the Dissimilarity Matrix")
    diag_series = pd.Series(np.diag(diss_mat), index=[diss_mat.index, diss_mat.columns])
    if len(diag_series[diag_series > 0]) > 0:
        logging.warning("Not all Values along the Diagonal are 0. Setting to 0")
        diss_mat = diss_mat.to_numpy()
        np.fill_diagonal(diss_mat, 0)
    else:
        logger.info("All Values along the Diagonal are 0")
        diss_mat = diss_mat.to_numpy()

    logger.info("Calculating and Storing Silhouette Values")
    sample_silhouette_values = silhouette_samples(
        diss_mat, clusters["Truth"].tolist(), metric="precomputed"
    )
    sil = pd.DataFrame(sample_silhouette_values, index=clusters.index)
    sil["cluster"] = clusters["Truth"].tolist()
    sil_mean = sil.groupby("cluster")[0].mean().to_frame(name="avg_silhouette")
    sil_mean["res"] = res

    logger.info("Calculate Average co-occurance per Cluster")
    grp = pd.DataFrame(
        index=set(clusters["Truth"].tolist()),
        columns=["avg_pct", "cluster_1", "ct_lower", "ct_upper"],
    )

    for cluster_1 in set(clusters["Truth"].tolist()):
        ids_1 = [int(x) == int(cluster_1) for x in occ_mat.index.tolist()]
        values = occ_mat.loc[ids_1, ids_1].values.flatten()
        mean = values.mean()
        # print(mean)
        ct = sms.DescrStatsW(values).tconfint_mean()
        # print(ct)
        grp.at[cluster_1, "avg_pct"] = mean
        grp.at[cluster_1, "ct_lower"] = ct[0]
        grp.at[cluster_1, "ct_upper"] = ct[1]
        grp.at[cluster_1, "cluster_1"] = cluster_1
    grp["res"] = res

    return [sil_mean, grp]


def calculate_stability_measures(
    edge_df,
    num_iter=100,
    prop=0.8,
    resolutions=None,
    make_leiden_knn=False,
    n_neighbours=30,
    walktrap_steps=4,
    random_seed=42,
    leiden_iterations=1000,
    cluster_alg="leiden",
    plot=False,
):
    start_time = datetime.datetime.now()

    edge_df = edge_df.copy()

    cluster_list = {}
    grp_list = {}
    sil_list = {}

    pbar_res = tqdm(
        total=len(resolutions),
        desc="Number of Resolutions",
        maxinterval=5,
        mininterval=0.5,
        unit=" resolutions",
        position=0,
    )

    for res in resolutions:
        logger.info("Processing Resolution {}".format(res))
        logging.getLogger().setLevel(logging.ERROR)
        orig_clusters = cluster_correlation(
            gene_corr_long_subset=edge_df,
            cluster_alg=cluster_alg,
            leiden_resolution=res,
            leiden_iterations=leiden_iterations,
            make_leiden_knn=make_leiden_knn,
            n_neighbours=n_neighbours,
            walktrap_steps=walktrap_steps,
            plot=plot,
            random_seed=random_seed,
        ).apply(pd.to_numeric, errors="ignore", downcast="integer")[
            ["name", "membership"]
        ]

        orig_clusters.index = orig_clusters["name"]
        orig_clusters = orig_clusters[["membership"]].rename(
            {"membership": "Truth"}, axis=1
        )

        pbar_iter = tqdm(
            total=len(range(0, num_iter, 1)),
            desc="Number of Iterations",
            maxinterval=5,
            mininterval=0.5,
            unit=" its",
            position=1,
        )

        clusters_list = []

        for iteration in [str(x) for x in range(0, num_iter, 1)]:
            cluster_sub = run_silhouette_iteration(
                edge_df=edge_df,
                clusters=orig_clusters,
                iteration=iteration,
                cluster_alg=cluster_alg,
                leiden_resolution=res,
                leiden_iterations=leiden_iterations,
                make_leiden_knn=make_leiden_knn,
                n_neighbours=n_neighbours,
                walktrap_steps=walktrap_steps,
                random_seed=random_seed,
                plot=False,
            )
            clusters_list.append(cluster_sub)
            pbar_iter.update()

        pbar_iter.close()

        clusters_list.insert(0, orig_clusters)
        clusters = pd.concat(clusters_list, axis=1, ignore_index=False)
        clusters = clusters.apply(pd.to_numeric, errors="ignore", downcast="integer")
        cluster_list[str(res)] = clusters

        sil_mean, grp = calculate_silhouette_score(
            edge_df=edge_df,
            clusters=clusters,
            num_iter=num_iter,
            res=res,
        )

        del clusters
        sil_list[str(res)] = sil_mean
        grp_list[str(res)] = grp
        gc.collect()
        logging.getLogger().setLevel(logging.INFO)

        logger.info("Done processing resolution {}".format(res))
        pbar_res.update()

    pbar_res.close()
    end_time = datetime.datetime.now()

    logger.info(
        "Total Processing took: {duration}".format(duration=end_time - start_time)
    )
    return [cluster_list, grp_list, sil_list]


def bootstrap_median(
    df,
    confidence_level,
    n_resamples=9999,
):
    rng = np.random.default_rng()
    data = (df.tolist(),)
    # print(df)
    # print(data)
    bootstrap_res = bootstrap(
        data,
        np.median,
        confidence_level=confidence_level,
        random_state=rng,
        n_resamples=n_resamples,
    )

    bootstrap_res = [
        1 if (x == np.nan) or (math.isnan(x)) else x
        for x in bootstrap_res.confidence_interval
    ]

    # logger.info("Confidence interval is {}".format(bootstrap_res))

    return bootstrap_res


def calculate_silhouette_stats(sil_list, confidence_level=0.95, n_resamples=9999):
    sil_list_concat = pd.concat([x for x in sil_list.values()])

    # print(sil_list_concat)

    sil_list_concat["n_clusters"] = sil_list_concat.groupby("res").transform(len)
    sil_list_concat["median"] = sil_list_concat.groupby("res")[
        "avg_silhouette"
    ].transform("median")

    sil_list_concat["ct_upper"] = sil_list_concat.groupby("res")[
        "avg_silhouette"
    ].transform(
        lambda x: bootstrap_median(
            x, confidence_level=confidence_level, n_resamples=n_resamples
        )[1]
    )

    sil_list_concat["ct_lower"] = sil_list_concat.groupby("res")[
        "avg_silhouette"
    ].transform(
        lambda x: bootstrap_median(
            x, confidence_level=confidence_level, n_resamples=n_resamples
        )[0]
    )
    sil_list_concat = sil_list_concat.reset_index().astype({"res": "category"})

    median_silhouette_stats = (
        sil_list_concat.drop_duplicates(["res"])[
            ["res", "ct_lower", "median", "ct_upper", "n_clusters"]
        ]
        .reset_index(drop=True)
        .astype({"res": "category"})
    )
    return [median_silhouette_stats, sil_list_concat]


def give_optimal_resolution(median_silhouette_stats):
    print(median_silhouette_stats)

    threshold = max(median_silhouette_stats["ct_lower"])

    median_silhouette_stats = median_silhouette_stats.reset_index(drop=True)

    choice = (
        median_silhouette_stats[median_silhouette_stats["median"] >= threshold]
        .sort_values("n_clusters")
        .tail(1)
    )

    choice_index = choice.index.tolist()[0]

    print(choice)
    print(choice_index)

    choice = choice.reset_index(drop=True).at[0, "res"]
    return [float(threshold), float(choice_index), float(choice)]


def plot_silhouette_scores(
    median_silhouette_stats, silhouette_scores, threshold, choice_index
):
    g = (
        ggplot(median_silhouette_stats, aes("res", "median"))
        + geom_crossbar(aes(ymin="ct_lower", ymax="ct_upper"), fill="grey", size=0.25)
        + geom_hline(aes(yintercept=threshold), colour="blue")
        + geom_vline(aes(xintercept=choice_index + 1), colour="red")
        + geom_jitter(
            silhouette_scores, aes("res", "avg_silhouette"), size=0.35, width=0.15
        )
        + ylim(-1, 1)
        + theme_bw()
        + labs(y="Silhouette Score", x="Clustering Resolution")
    )
    return g


def plot_silhouette_optimal_cluster(silhouette_scores, choice):
    chosen_resolution_ordered = (
        silhouette_scores[silhouette_scores["res"] == choice]
        .sort_values("avg_silhouette", ascending=False)
        .astype({"cluster": "category"})
        .reset_index(drop=True)
    )
    chosen_resolution_ordered = chosen_resolution_ordered.assign(
        cluster=chosen_resolution_ordered["cluster"].cat.reorder_categories(
            chosen_resolution_ordered["cluster"].tolist()
        )
    )
    g = (
        ggplot(chosen_resolution_ordered, aes("cluster", "avg_silhouette"))
        + geom_point()
        + ylim(0, 1)
    )
    return g


def get_optimal_clustering(
    edge_df,
    num_iter=100,
    prop=0.8,
    resolutions=[0.1, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5, 6],
    make_leiden_knn=False,
    n_neighbours=30,
    walktrap_steps=4,
    random_seed=42,
    leiden_iterations=1000,
    cluster_alg="leiden",
    plot=False,
):
    cluster_list, grp_list, sil_list = calculate_stability_measures(
        edge_df,
        num_iter=num_iter,
        prop=prop,
        resolutions=resolutions,
        make_leiden_knn=make_leiden_knn,
        n_neighbours=n_neighbours,
        walktrap_steps=walktrap_steps,
        random_seed=random_seed,
        leiden_iterations=leiden_iterations,
        cluster_alg=cluster_alg,
        plot=plot,
    )

    median_silhouette_stats, sil_list_concat = calculate_silhouette_stats(
        sil_list, confidence_level=0.95, n_resamples=9999
    )

    threshold, choice_index, choice = give_optimal_resolution(median_silhouette_stats)

    optimal_plot = plot_silhouette_scores(
        median_silhouette_stats=median_silhouette_stats,
        silhouette_scores=sil_list_concat,
        threshold=threshold,
        choice_index=choice_index,
    )

    # silhouette_of_optimal = plot_silhouette_optimal_cluster(
    #    silhouette_scores=sil_list_concat, choice=choice
    # )

    return [choice, optimal_plot]
