import logging
import os
import random

import numpy as np
import pandas as pd
import torch
from anndata import AnnData
from mudata import MuData
from scipy import spatial

from .r import run_deseq2

logger = logging.getLogger(__name__)


def NormalizeData(data):
    return (data - np.min(data)) / (np.max(data) - np.min(data))


def PercentMax(data):
    return (data / np.max(data)) * 100


def PercentSum(data):
    return (data / np.sum(data)) * 100


def seed_everything(seed=42):
    """ "
    Seed everything.
    """
    random.seed(seed)
    os.environ["PYTHONHASHSEED"] = str(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)
    torch.backends.cudnn.deterministic = True


def prepare_rade(
    adata_input,
    id_col,
    split_by,
    sample_col,
    min_frac=0.2,
    min_fracdiff=0,
    both_frac=True,
):
    if isinstance(adata_input, AnnData):
        adata = adata_input.copy()
    elif isinstance(adata_input, MuData) and "rna" in adata_input.mod:
        adata = adata_input.mod["rna"].copy()
    else:
        raise TypeError("Expected AnnData or MuData object with 'rna' modality")

    # unique_splits = list(set(adata.obs[split_by].tolist()))
    unique_ids = list(set(adata.obs[id_col].tolist()))
    unique_models = list(set(adata.obs[split_by].tolist()))

    data_frame_collection = []
    for id in unique_ids:
        logger.info("Processing ID {}".format(id))
        subset_adata = adata[adata[adata.obs[id_col] == id].obs.index.tolist()].copy()
        subset_unique_models = list(set(subset_adata.obs[split_by].tolist()))
        if subset_unique_models != unique_models:
            logger.info(
                "Skipping {} because it is not present in all Conditions.".format(id)
            )
            continue

        subset_DEG = run_deseq2(
            adata=subset_adata,
            sample_key=sample_col,
            model_key=split_by,
            layer="counts",
            replicate_key=None,
            one_v_all=False,
            n_pseudoreplicates=1,
            log2_cutoff=0,
            min_counts_per_gene=0,
            min_counts_per_sample=1000,
            post_process=False,
        )
        subset_DEG["ID"] = id
        if both_frac:
            subset_DEG = subset_DEG[
                (subset_DEG["frac_expression"] >= min_frac)
                & (subset_DEG["Down_In_frac_expression"] >= min_frac)
            ]
        else:
            subset_DEG = subset_DEG[
                (subset_DEG["frac_expression"] >= min_frac)
                | (subset_DEG["Down_In_frac_expression"] >= min_frac)
            ]

        subset_DEG = subset_DEG[(subset_DEG["frac_diff"] >= min_fracdiff)]

        subset_DEG = subset_DEG[["Gene", "log2FoldChange", "padj", "Up_In", "ID"]]

        subset_DEG.loc[subset_DEG["log2FoldChange"] <= 0, "padj"] = 1
        subset_DEG.loc[subset_DEG["log2FoldChange"] <= 0, "log2FoldChange"] = 0
        subset_DEG = subset_DEG.sort_values(["log2FoldChange", "padj"])
        data_frame_collection.append(subset_DEG)

    return pd.concat(data_frame_collection)


###
# Calculation of the cluster centroids was taken from scanpy
# https://github.com/theislab/scanpy/blob/d072abd05bda07f280ea91f5e7e4a84f9782c118/scanpy/plotting/_tools/scatterplots.py
###
def closest_to_centroid(arr, n_neigh=10):
    length = arr.shape[0]
    sum_x = np.sum(arr[:, 0])
    sum_y = np.sum(arr[:, 1])
    cent_x = sum_x / length
    cent_y = sum_y / length
    distance, index = spatial.KDTree(arr).query((cent_x, cent_y), k=n_neigh)
    return [distance, index]


def castToList(x):  # casts x to a list
    if isinstance(x, list):
        return x
    elif isinstance(x, str):
        return [x]
    elif isinstance(x, float):
        return [x]
    try:
        return list(x)
    except TypeError:
        return [x]
