import logging
import math
import os

import anndata as ad
import matplotlib.collections
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import scanpy as sc
import scipy
import seaborn as sns
from scipy.sparse import csr_matrix
from scipy.stats import zscore
from sklearn.cluster import KMeans

from .helpers import PercentMax

logger = logging.getLogger(__name__)


def AnnotatePseudotime(
    adata,
    pseudotime_col="dpt_pseudotime",
    cell_type_col="final_cell_type",
    bins=200,
):
    adata = adata.copy()

    # obs_index = adata.obs.index
    logger.info("Splitting Data into equal Pseudotime Bins...")

    adata.obs = adata.obs.loc[
        :,
        ~adata.obs.columns.isin(
            [
                "Pseudotime_Binned",
                "Bin_Identity",
                "Bin_size",
                "Pseudotime_min",
                "Pseudotime_max",
            ]
        ),
    ]

    adata.obs["Pseudotime_Binned"] = [
        i.mid for i in pd.qcut(adata.obs[pseudotime_col].tolist(), bins)
    ]

    adata.obs["Pseudotime_min"] = [
        i.left for i in pd.qcut(adata.obs[pseudotime_col].tolist(), bins)
    ]

    adata.obs["Pseudotime_max"] = [
        i.right for i in pd.qcut(adata.obs[pseudotime_col].tolist(), bins)
    ]

    counted_identity = (
        adata.obs[["Pseudotime_Binned", cell_type_col, pseudotime_col]]
        .groupby(["Pseudotime_Binned", cell_type_col])
        .count()
        .reset_index()
        .rename({pseudotime_col: "count"}, axis=1)
    ).copy()

    idx = (
        counted_identity.groupby(["Pseudotime_Binned"])["count"].transform(max)
        == counted_identity["count"]
    )

    counted_identity.columns = ["Pseudotime_Binned", "Bin_Identity", "Bin_size"]
    counted_identity = (
        counted_identity[idx]
        .drop_duplicates(["Pseudotime_Binned"])
        .drop(["Bin_size"], axis=1)
    )

    adata.obs = (
        adata.obs.reset_index()
        .merge(
            counted_identity, on="Pseudotime_Binned", how="left", validate="many_to_one"
        )
        .set_index("index")
        .copy()
    )

    logger.info("Done.")

    return adata


def CreatePseudotimeAdata(
    adata,
    pseudotime_col="dpt_pseudotime",
    cell_type_col="final_cell_type",
    exclude_celltypes=None,
    split_by=None,
    bins=200,
    k=6,
    use_premade_binning=False,
    layer="counts",
    random_seed=42,
    min_counts=50,
    min_cells=10,
):
    adata = adata.copy()
    adata.X = adata.layers[layer].copy()

    logger.info("Removing Genes with less than {} counts...".format(min_counts))
    sc.pp.filter_genes(adata, min_counts=min_counts)

    logger.info("Removing Genes expressed in less than {} cells...".format(min_counts))
    sc.pp.filter_genes(adata, min_cells=min_cells)

    logger.info("Splitting Data into equal Pseudotime Bins...")

    adata_data = adata.obs.copy()
    adata_data["Cell_Name"] = adata_data.index.tolist()

    if split_by is None:
        split_by = "split"
        adata_data[split_by] = "-"

    if exclude_celltypes is not None:
        logger.info("Removing cell types {}...".format(exclude_celltypes))
        adata_data = adata_data[~adata_data[cell_type_col].isin(exclude_celltypes)]

    if use_premade_binning is not True:
        logger.info("Binning Pseudotime...")

        adata_data = adata_data.loc[
            :,
            ~adata_data.columns.isin(
                [
                    "Pseudotime_Binned",
                    "Bin_Identity",
                    "Bin_size",
                    "Pseudotime_min",
                    "Pseudotime_max",
                ]
            ),
        ]

        adata_data["Pseudotime_Binned"] = [
            i.mid for i in pd.qcut(adata_data[pseudotime_col].tolist(), bins)
        ]

        pseudotime_binned = adata_data[["Pseudotime_Binned"]].copy()

        pseudotime_binned["Pseudotime_min"] = [
            i.left for i in pd.qcut(adata_data[pseudotime_col].tolist(), bins)
        ]

        pseudotime_binned["Pseudotime_max"] = [
            i.right for i in pd.qcut(adata_data[pseudotime_col].tolist(), bins)
        ]

        pseudotime_binned = pseudotime_binned.drop_duplicates(
            ["Pseudotime_Binned", "Pseudotime_min", "Pseudotime_max"]
        )
    else:
        logger.info("Using Pre-binned Pseudotime...")

        pseudotime_binned = adata_data[
            ["Pseudotime_Binned", "Pseudotime_min", "Pseudotime_max"]
        ].copy()

        pseudotime_binned = pseudotime_binned.drop_duplicates(
            ["Pseudotime_Binned", "Pseudotime_min", "Pseudotime_max"]
        )

        adata_data = adata_data.loc[
            :, ~adata_data.columns.isin(["Bin_Identity", "Bin_size"])
        ]

    adata_data = adata_data.sort_values([pseudotime_col])[
        [pseudotime_col, "Pseudotime_Binned", cell_type_col, split_by, "Cell_Name"]
    ].copy()

    logger.info("Determining prevalent Cell Identity per Bin...")

    counted_identity = (
        adata_data.groupby(["Pseudotime_Binned", cell_type_col])
        .count()
        .reset_index()
        .drop([pseudotime_col], axis=1)
        .rename({"Cell_Name": "count"}, axis=1)
    )

    counted_identity = counted_identity.drop(split_by, axis=1)

    idx = (
        counted_identity.groupby(["Pseudotime_Binned"])["count"].transform(max)
        == counted_identity["count"]
    )

    counted_identity.columns = ["Pseudotime_Binned", "Cell_Identity", "count"]
    counted_identity = (
        counted_identity[idx]
        .drop_duplicates(["Pseudotime_Binned"])
        .drop(["count"], axis=1)
    )

    adata_data = adata_data.merge(
        counted_identity, on="Pseudotime_Binned", how="left", validate="many_to_one"
    )

    adata_data.index = adata_data["Cell_Name"].tolist()

    logger.info("Gathering Gene Expression data from layer {}...".format(layer))

    gene_expression = pd.DataFrame(
        adata.layers[layer].toarray().copy(),
        index=adata.obs_names.copy(),
        columns=adata.var_names.copy(),
    )

    adata_data = adata_data.merge(
        gene_expression, how="left", left_index=True, right_index=True
    )

    logger.info("Calculate Mean and other Statistics per Bin...")

    split_adata_data_mean = adata_data.groupby(
        ["Pseudotime_Binned", split_by], observed=True
    ).mean()[gene_expression.columns.tolist()]

    split_adata_data_sum = adata_data.groupby(
        ["Pseudotime_Binned", split_by], observed=True
    ).sum()[gene_expression.columns.tolist()]

    split_adata_data_std = adata_data.groupby(
        ["Pseudotime_Binned", split_by], observed=True
    ).std()[gene_expression.columns.tolist()]

    split_adata_data_count = np.sqrt(
        adata_data.groupby(["Pseudotime_Binned", split_by], observed=True).count()[
            gene_expression.columns.tolist()
        ]
    )

    split_adata_data_ci = 1.96 * split_adata_data_std / split_adata_data_count
    split_adata_data_ci_upper = split_adata_data_mean + split_adata_data_ci
    split_adata_data_ci_lower = split_adata_data_mean - split_adata_data_ci

    obs_prep = (
        adata_data.drop_duplicates(["Cell_Identity", "Pseudotime_Binned", split_by])
        .reset_index(drop=True)[["Cell_Identity", split_by, "Pseudotime_Binned"]]
        .sort_values(["Pseudotime_Binned", split_by])
    )

    X_mat = split_adata_data_mean[gene_expression.columns.tolist()]

    obs_ref = X_mat.reset_index()[["Pseudotime_Binned", split_by]]

    obs = obs_ref.merge(obs_prep, how="left").rename(
        {"Cell_Identity": "major_cluster", "Pseudotime_Binned": "pseudotime_center"},
        axis=1,
    )

    obs["name"] = obs["pseudotime_center"].astype(str) + "_" + obs[split_by].astype(str)

    cells_counts = (
        adata_data.groupby(["Cell_Identity", "Pseudotime_Binned", split_by])
        .count()["Cell_Name"]
        .reset_index()
        .rename({"Cell_Name": "n_cells"}, axis=1)
    )

    obs = obs.merge(
        cells_counts,
        left_on=["major_cluster", "pseudotime_center", split_by],
        right_on=["Cell_Identity", "Pseudotime_Binned", split_by],
        how="left",
    ).drop("Pseudotime_Binned", axis=1)

    obs = obs.rename({split_by: "GROUP"}, axis=1)

    cells_per_bin = (
        obs.groupby("pseudotime_center")
        .sum()["n_cells"]
        .to_frame()
        .reset_index()
        .rename({"n_cells": "n_cells_bin"}, axis=1)
    ).copy()

    obs = obs.merge(
        cells_per_bin,
        how="left",
        left_on="pseudotime_center",
        right_on="pseudotime_center",
    ).copy()

    obs = obs.merge(
        pseudotime_binned,
        how="left",
        left_on="pseudotime_center",
        right_on="Pseudotime_Binned",
    ).drop("Pseudotime_Binned", axis=1)

    obs["Pseudotime_Binned"] = obs["pseudotime_center"].copy()
    obs.index = obs["name"].tolist()

    X_mat.index = obs["name"]

    split_pseudotime_adata = ad.AnnData(
        X=scipy.sparse.csr_matrix(X_mat.values),
        dtype=np.float32,
        obs=obs,
        var=adata.var,
    )

    split_pseudotime_adata.layers["raw"] = split_pseudotime_adata.X

    split_pseudotime_adata.layers["sum"] = scipy.sparse.csr_matrix(
        split_adata_data_sum[gene_expression.columns.tolist()].values
    )
    split_pseudotime_adata.layers["std"] = scipy.sparse.csr_matrix(
        split_adata_data_std[gene_expression.columns.tolist()].values
    )
    split_pseudotime_adata.layers["ci"] = scipy.sparse.csr_matrix(
        split_adata_data_ci[gene_expression.columns.tolist()].values
    )
    split_pseudotime_adata.layers["ci_upper"] = scipy.sparse.csr_matrix(
        split_adata_data_ci_upper[gene_expression.columns.tolist()].values
    )
    split_pseudotime_adata.layers["ci_lower"] = scipy.sparse.csr_matrix(
        split_adata_data_ci_lower[gene_expression.columns.tolist()].values
    )

    if split_by == "split":
        logger.info("-- Z-scale and Cluster the Data...")

        split_pseudotime_adata.layers["z_scaled"] = scipy.sparse.csr_matrix(
            split_adata_data_mean[adata.var_names]
            .apply(zscore, axis=0)
            .fillna(0)
            .values
        )

        clustering = KMeans(n_clusters=k, random_state=random_seed).fit(
            split_pseudotime_adata.layers["z_scaled"].T.todense()
        )

        split_pseudotime_adata.var["clusters"] = clustering.labels_.tolist()

    split = pd.DataFrame(split_pseudotime_adata.layers["raw"].todense())
    split.columns = split_pseudotime_adata.var_names
    split["pseudotime_center"] = split_pseudotime_adata.obs["pseudotime_center"].copy()

    split[
        [
            "pseudotime_center",
            "Cell_Identity",
            "GROUP",
            "n_cells",
            "n_cells_bin",
            "Pseudotime_Binned",
            "Pseudotime_min",
            "Pseudotime_max",
        ]
    ] = (
        split_pseudotime_adata.obs[
            [
                "pseudotime_center",
                "Cell_Identity",
                "GROUP",
                "n_cells",
                "n_cells_bin",
                "Pseudotime_Binned",
                "Pseudotime_min",
                "Pseudotime_max",
            ]
        ]
        .reset_index(drop=True)
        .copy()
    )

    split_obs = split[
        [
            "pseudotime_center",
            "Cell_Identity",
            "GROUP",
            "n_cells",
            "n_cells_bin",
            "Pseudotime_Binned",
            "Pseudotime_min",
            "Pseudotime_max",
        ]
    ].copy()

    split_obs["n_cells"] = split_obs["n_cells"].astype(int)
    split_obs["n_cells_bin"] = split_obs["n_cells_bin"].astype(int)
    split_obs["Pseudotime_min"] = split_obs["Pseudotime_min"].astype(np.float32)
    split_obs["Pseudotime_max"] = split_obs["Pseudotime_max"].astype(np.float32)
    split_obs["pseudotime_center"] = split_obs["pseudotime_center"].astype(np.float32)
    split_obs["Pseudotime_Binned"] = split_obs["Pseudotime_Binned"].astype(np.float32)

    split_obs["Cell_Identity"] = split_obs["Cell_Identity"].astype(str)
    split_obs["GROUP"] = split_obs["GROUP"].astype(str)

    split = split.iloc[
        :,
        ~split.columns.isin(
            [
                "pseudotime_center",
                "Cell_Identity",
                "GROUP",
                "n_cells",
                "n_cells_bin",
                "Pseudotime_Binned",
                "Pseudotime_min",
                "Pseudotime_max",
            ]
        ),
    ].copy()

    split_var = split.columns.tolist()

    split_pseudotime_adata.uns["prepared_df"] = csr_matrix(split)
    split_pseudotime_adata.uns["prepared_df_obs"] = split_obs
    split_pseudotime_adata.uns["prepared_df_var"] = split_var

    logger.info("All Done!")

    return split_pseudotime_adata


def equal_bin(N, m):
    sep = (N.size / float(m)) * np.arange(1, m + 1)
    idx = sep.searchsorted(np.arange(N.size))
    return idx[N.argsort().argsort()]


def DotPlotPseudotime(
    adata,
    output_path,
    genes_of_interest,
    color_dict_grouping=None,
    color_dict_celltype=None,
    color_background=True,
    output=True,
    size_mod=5,
    x_min=None,
    x_max=None,
    rounding_decimals_pt=4,
    padding_measure=10,
    y_min=None,
    y_max=None,
    point_size=0.1,
    n_cell_cutoff=10,
    point_alpha=1,
    cmap="Dark2",
    draw_grid=True,
    axis_to_hide=["top", "right"],
    rot_axis_labels=True,
    what_label=None,
):
    adata = adata.copy()

    x_min_orig = x_min
    x_max_orig = x_max
    y_min_orig = y_min
    y_max_orig = y_max

    prepared_df = pd.DataFrame(adata.uns["prepared_df"].todense())
    prepared_df.columns = adata.uns["prepared_df_var"]

    prepared_df = pd.concat(
        [prepared_df, adata.uns["prepared_df_obs"]],
        axis=1,
        join="inner",
        ignore_index=False,
        keys=None,
        levels=None,
        names=None,
        verify_integrity=False,
        copy=True,
    )

    adata.uns["prepared_df"] = prepared_df

    if "GROUP" in adata.uns["prepared_df"].columns.to_list() and what_label is None:
        what_label = "grouping"
        print("Chose grouping")
    elif what_label is None:
        what_label = "name"

    for gene in genes_of_interest:
        gene_ci = (
            adata.layers["ci"][:, adata.var_names.isin([gene])]
            .toarray()
            .flatten()
            .tolist()
        )

        if what_label == "grouping":
            plot_df_mean = pd.DataFrame(
                {
                    "Cell_Identity": adata.uns["prepared_df"]["Cell_Identity"].tolist(),
                    "Pseudotime_Binned": adata.uns["prepared_df"][
                        "pseudotime_center"
                    ].tolist(),
                    "Pseudotime_min": adata.uns["prepared_df"][
                        "Pseudotime_min"
                    ].tolist(),
                    "Pseudotime_max": adata.uns["prepared_df"][
                        "Pseudotime_max"
                    ].tolist(),
                    "GROUP": adata.uns["prepared_df"]["GROUP"].tolist(),
                    "n_cells": adata.uns["prepared_df"]["n_cells"].tolist(),
                    gene: adata.uns["prepared_df"][gene].tolist(),
                    "gene_ci": gene_ci,
                }
            )
            plot_df_mean["n_cells"] = plot_df_mean["n_cells"] * point_size

            if x_max_orig is None:
                x_max = plot_df_mean["Pseudotime_max"].max() + (
                    plot_df_mean["Pseudotime_max"].max() / padding_measure
                )

            if x_min_orig is None:
                x_min = plot_df_mean["Pseudotime_min"].min() - (
                    plot_df_mean["Pseudotime_min"].min() / padding_measure
                )

            if y_max_orig is None:
                y_max = np.ceil(plot_df_mean[gene].max().tolist())

            if y_min_orig is None:
                y_min = np.floor(plot_df_mean[gene].min().tolist())

            fig, ax = plt.subplots(
                1,
                1,
                figsize=(size_mod, size_mod),
                squeeze=True,
                facecolor="white",
                edgecolor="black",
            )
            fig.subplots_adjust(right=2)
            fig.tight_layout()

            data = plot_df_mean.copy()

            if color_dict_celltype is None:
                color_dict_celltype = {}
                unique_members = list(data["Cell_Identity"].unique())
                colors = sns.color_palette(cmap, n_colors=len(unique_members)).as_hex()
                for i, member in enumerate(unique_members):
                    color_dict_celltype[member] = colors[i]

            data["celltype_colors"] = (
                data["Cell_Identity"].map(color_dict_celltype).tolist()
            )

            if color_dict_grouping is None:
                color_dict_grouping = {}
                unique_members = list(data["GROUP"].unique())
                colors = sns.color_palette(cmap, n_colors=len(unique_members)).as_hex()
                for i, member in enumerate(unique_members):
                    color_dict_grouping[member] = colors[i]

            data["GROUP_colors"] = data["GROUP"].map(color_dict_grouping).tolist()

            color_area = (
                data[
                    [
                        "Cell_Identity",
                        "celltype_colors",
                        "Pseudotime_min",
                        "Pseudotime_max",
                    ]
                ]
                .rename(
                    {
                        "Pseudotime_min": "area_min",
                        "Pseudotime_max": "area_max",
                        "celltype_colors": "colors",
                        "Cell_Identity": "name",
                    },
                    axis=1,
                )
                .drop_duplicates()
            )
            color_area = color_area.groupby(
                (color_area["name"].shift() != color_area["name"]).cumsum()
            ).agg(
                {
                    "area_min": "min",
                    "area_max": "max",
                    "name": lambda column: column.unique()[0],
                    "colors": lambda column: column.unique()[0],
                }
            )
            # color_area["area_min"] = color_area["area_min"] - (-0.01)
            # color_area["area_max"] = color_area["area_max"] - (-0.001)

            data_raw = data.copy()

            data = data.groupby(["GROUP"])

            for x, group in data:
                fig_label = x
                color_use = group["GROUP_colors"]

                plot = ax.scatter(
                    data=group,
                    x="Pseudotime_Binned",
                    y=gene,
                    s="n_cells",
                    c=color_use,
                    alpha=point_alpha,
                    linewidths=0,
                    label=fig_label,
                )

                ax.fill_between(
                    x=group["Pseudotime_Binned"],
                    y1=group[gene] - group["gene_ci"],
                    y2=group[gene] + group["gene_ci"],
                    alpha=0.3,
                    color=color_use.unique(),
                    interpolate=True,
                )

            x_ticks = np.round(
                data_raw["Pseudotime_Binned"].unique().tolist(), rounding_decimals_pt
            )

            ax.set_xlim(left=x_min, right=x_max)
            ax.set_ylim(bottom=y_min - 0.01, top=y_max + 0.01)
            ax.set_xticks(x_ticks)
            ax.set_xticklabels(x_ticks, rotation=90)

            ax.set_xlabel("Central Pseudotime of Bin", fontsize="large", color="black")
            ax.set_ylabel(
                "Mean " + gene + " Expression", fontsize="large", color="black"
            )
            ax.spines[["left", "bottom", "top", "right"]].set_color("black")
            ax.spines[["left", "bottom", "top", "right"]].set_linewidth(1)
            ax.spines[["left", "bottom", "top", "right"]].set_visible(True)

            if axis_to_hide is not None:
                ax.spines[axis_to_hide].set_visible(False)

            if draw_grid:
                ax.grid(alpha=0.3, color="#a6a6a6")
            else:
                ax.grid(False)

            ax.set_facecolor("white")
            ax.set_aspect(1.0 / ax.get_data_ratio())
            if color_background:
                for i, row in color_area.iterrows():
                    ax.axvspan(
                        row["area_min"],
                        row["area_max"],
                        facecolor=row["colors"],
                        alpha=0.2,
                        zorder=-100,
                        label=row["name"],
                    )

            # fig.legend()
            box = ax.get_position()
            ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])

            # Put a legend to the right of the current axis
            color_legend = ax.legend(loc="upper left", bbox_to_anchor=(1, 1))

            number_legend = ax.legend(
                *plot.legend_elements("sizes", num=4),
                loc="lower left",
                bbox_to_anchor=(1, 0),
                title="Number of Cells*" + str(point_size)
            )

            for handle in color_legend.legendHandles:
                if isinstance(handle, matplotlib.collections.PathCollection):
                    handle.set_sizes([200 * point_size])

            ax.add_artist(color_legend)
            ax.add_artist(number_legend)

            # ax.set_xlim(left=x_min - 0.01, right=x_max + 0.01)
            # ax.set_ylim(bottom=y_min - 0.01, top=y_max + 0.01)

        if what_label == "name":
            plot_df_mean = pd.DataFrame(
                {
                    "Cell_Identity": adata.uns["prepared_df"]["Cell_Identity"].tolist(),
                    "Pseudotime_Binned": adata.uns["prepared_df"][
                        "pseudotime_center"
                    ].tolist(),
                    "Pseudotime_min": adata.uns["prepared_df"][
                        "Pseudotime_min"
                    ].tolist(),
                    "Pseudotime_max": adata.uns["prepared_df"][
                        "Pseudotime_max"
                    ].tolist(),
                    "n_cells": adata.uns["prepared_df"]["n_cells"].tolist(),
                    gene: adata.uns["prepared_df"][gene].tolist(),
                    "gene_ci": gene_ci,
                }
            )
            plot_df_mean["n_cells"] = plot_df_mean["n_cells"] * point_size

            if x_max_orig is None:
                x_max = plot_df_mean["Pseudotime_max"].max() + (
                    plot_df_mean["Pseudotime_max"].max() / padding_measure
                )

            if x_min_orig is None:
                x_min = plot_df_mean["Pseudotime_min"].min() - (
                    plot_df_mean["Pseudotime_min"].min() / padding_measure
                )

            if y_max_orig is None:
                y_max = np.ceil(plot_df_mean[gene].max())

            if y_min_orig is None:
                y_min = np.floor(plot_df_mean[gene].min())

            fig, ax = plt.subplots(
                1,
                1,
                figsize=(size_mod, size_mod),
                squeeze=True,
                facecolor="white",
                edgecolor="black",
            )
            fig.subplots_adjust(right=2)
            fig.tight_layout()

            data = plot_df_mean.copy()

            if color_dict_celltype is None:
                color_dict_celltype = {}
                unique_members = list(data["Cell_Identity"].unique())
                colors = sns.color_palette(cmap, n_colors=len(unique_members)).as_hex()
                for i, member in enumerate(unique_members):
                    color_dict_celltype[member] = colors[i]

            data["celltype_colors"] = (
                data["Cell_Identity"].map(color_dict_celltype).tolist()
            )

            data_raw = data.copy()

            data = data.groupby(["Cell_Identity"])

            for x, group in data:
                fig_label = x
                color_use = group["celltype_colors"]

                plot = ax.scatter(
                    data=group,
                    x="Pseudotime_Binned",
                    y=gene,
                    c=color_use,
                    alpha=point_alpha,
                    linewidths=0,
                    s=point_size,
                    label=fig_label,
                )
            ax.fill_between(
                x=data_raw["Pseudotime_Binned"],
                y1=data_raw[gene] - data_raw["gene_ci"],
                y2=data_raw[gene] + data_raw["gene_ci"],
                alpha=0.1,
                color="grey",
                interpolate=True,
            )

            x_ticks = np.round(
                data_raw["Pseudotime_Binned"].unique().tolist(), rounding_decimals_pt
            )

            ax.set_xlim(left=x_min, right=x_max)
            ax.set_ylim(bottom=y_min - 0.01, top=y_max + 0.01)
            ax.set_xticks(x_ticks)
            ax.set_xticklabels(x_ticks, rotation=90)

            ax.set_title(gene)
            ax.set_xlabel("Central Pseudotime of Bin", fontsize="large", color="black")
            ax.set_ylabel(
                "Mean " + gene + " Expression", fontsize="large", color="black"
            )
            ax.spines[["left", "bottom", "top", "right"]].set_color("black")
            ax.spines[["left", "bottom", "top", "right"]].set_linewidth(1)
            ax.spines[["left", "bottom", "top", "right"]].set_visible(True)

            if axis_to_hide is not None:
                ax.spines[axis_to_hide].set_visible(False)

            if draw_grid:
                ax.grid(alpha=0.3, color="#a6a6a6")
            else:
                ax.grid(False)

            ax.set_facecolor("white")
            ax.set_aspect(1.0 / ax.get_data_ratio())

            # fig.legend()
            box = ax.get_position()
            ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])

            # Put a legend to the right of the current axis
            ax.legend(loc="center left", bbox_to_anchor=(1, 0.5))

        plt.tight_layout()

        if output:
            plt.savefig(
                os.path.join(output_path, "Pseudotime_DotPlot_" + str(gene) + ".pdf"),
                bbox_inches="tight",
            )


def PlotPseudotimeLines(
    adata,
    output_path=None,
    z_scaled=False,
    output=True,
    ci=95,
    cmap="Dark2",
    size_mod_width=8,
    size_mod_height=5,
    rounding_decimals_pt=3,
):
    adata = adata.copy()

    adata.X = adata.layers["raw"]

    prepared_df = pd.DataFrame(adata.X.todense())
    prepared_df.columns = adata.var["Gene"]
    prepared_df.index = adata.uns["prepared_df_obs"]["Pseudotime_Binned"]
    prepared_df.index = prepared_df.index.astype(np.float64)
    prepared_df.index = np.round(
        prepared_df.index.tolist(), decimals=rounding_decimals_pt
    )

    pseudotime_map = {
        k: v
        for k, v in zip(
            prepared_df.index.tolist(), range(0, len(prepared_df.index.tolist()))
        )
    }

    prepared_df = (
        prepared_df.T.merge(
            adata.var[["clusters", "Gene"]],
            left_index=True,
            right_on="Gene",
            how="left",
        )
        .drop("Gene", axis=1)
        .rename_axis("Gene")
        .reset_index()
    )

    prepared_df = prepared_df.melt(
        id_vars=["Gene", "clusters"], var_name="Pseudotime", value_name="Observation"
    )

    prepared_df["Pseudotime_Bins"] = prepared_df["Pseudotime"].map(pseudotime_map)

    if z_scaled:
        prepared_df["Observation"] = prepared_df.groupby(["Gene"], sort=False)[
            "Observation"
        ].transform(lambda x: zscore(x, ddof=0))

    fig, axs = plt.subplots(
        1,
        1 + len(prepared_df["clusters"].unique()),
        figsize=(
            size_mod_width * len(prepared_df["clusters"].unique()),
            size_mod_height,
        ),
        squeeze=True,
        facecolor="white",
        edgecolor="black",
    )
    fig.subplots_adjust(right=2)
    fig.tight_layout()

    logger.info("Plot Cluster Averages")
    ax = axs[0]

    sns.lineplot(
        data=prepared_df,
        x="Pseudotime_Bins",
        y="Observation",
        ci=ci,
        hue="clusters",
        legend=True,
        palette=cmap,
        ax=ax,
    )
    ax.set_xticks(prepared_df["Pseudotime_Bins"].unique().tolist())
    ax.set_xticklabels(prepared_df["Pseudotime"].unique().tolist(), rotation=90)
    ax.set_title("Gene Clusters")
    ax.set_xlabel("Central Pseudotime of Bin", fontsize="large", color="black")
    if z_scaled:
        ax.set_ylabel(
            "Z-scaled Normalized Expression Counts per Gene Cluster",
            fontsize="large",
            color="black",
        )
    else:
        ax.set_ylabel(
            "Mean Normalized Expression Counts per Gene Cluster",
            fontsize="large",
            color="black",
        )
    ax.spines[["left", "bottom"]].set_color("black")
    ax.spines[["left", "bottom"]].set_linewidth(1)
    ax.spines[["top", "right"]].set_visible(False)
    ax.grid(False)
    ax.set_facecolor("white")
    logger.info("Plot all Genes per Cluster")
    for i, cluster in enumerate(set(prepared_df["clusters"].tolist()), start=1):
        ax = axs[i]
        logger.info("Plot Cluster " + str(cluster))
        cluster_data = (
            prepared_df[prepared_df["clusters"] == cluster]
            .sort_values(["Gene", "Pseudotime"])
            .copy()
        )

        sns.lineplot(
            data=cluster_data,
            x="Pseudotime_Bins",
            y="Observation",
            legend=False,
            hue="Gene",
            palette="Greys",
            ax=ax,
        )
        ax.set_xticks(prepared_df["Pseudotime_Bins"].unique().tolist())
        ax.set_xticklabels(prepared_df["Pseudotime"].unique().tolist(), rotation=90)
        ax.set_title("Gene Cluster " + str(cluster))
        ax.set_xlabel("Central Pseudotime of Bin", fontsize="large", color="black")
        if z_scaled:
            ax.set_ylabel(
                "Z-scaled Normalized Expression Counts per Gene Cluster",
                fontsize="large",
                color="black",
            )
        else:
            ax.set_ylabel(
                "Mean Normalized Expression Counts per Gene Cluster",
                fontsize="large",
                color="black",
            )
        ax.spines[["left", "bottom"]].set_color("black")
        ax.spines[["left", "bottom"]].set_linewidth(1)
        ax.spines[["top", "right"]].set_visible(False)
        ax.grid(False)
        ax.set_facecolor("white")

    if output:
        logger.info("Writing to File")
        plt.savefig(output_path, bbox_inches="tight")


def PlotPseudotimeHeatmap(
    adata,
    output_path,
    var_cutoff=0.2,
    cutoff=1,
    z_scaled=True,
    clusters=None,
    vmax=2,
    vmin=-2,
    color_dict=None,
    metric="euclidean",
    figure_size=(20, 20),
    color_palette_time="viridis",
    color_palette_celltype="viridis",
    color_palette_heatmap="viridis",
    time_or_celltype="celltype",
    yticklabels=False,
    xticklabels=False,
    output=True,
):
    adata = adata.copy()

    prepared_df = pd.DataFrame(adata.uns["prepared_df"].todense())
    prepared_df.columns = adata.uns["prepared_df_var"]

    prepared_df = pd.concat(
        [prepared_df, adata.uns["prepared_df_obs"]],
        axis=1,
        join="inner",
        ignore_index=False,
        keys=None,
        levels=None,
        names=None,
        verify_integrity=False,
        copy=True,
    )

    adata.uns["prepared_df"] = prepared_df

    if z_scaled:
        data_raw = pd.DataFrame(adata.layers["raw"].T.todense())
        adata.X = adata.layers["z_scaled"]
        data = pd.DataFrame(adata.X.T.todense())
        data.index = adata.var_names.tolist()
        data_raw.index = adata.var_names.tolist()
    else:
        data = pd.DataFrame(adata.X.T.todense())
        data.index = adata.var_names.tolist()
        data_raw = data

    if time_or_celltype == "time":
        bins = adata.X.shape[0]
        colors = sns.color_palette(color_palette_time, bins)

    if time_or_celltype == "celltype":
        bin_names = adata.obs["major_cluster"]
        bin_names.index = adata.obs["major_cluster"].tolist()
        if color_dict is None:
            types = adata.obs["major_cluster"].unique()
            n_types = len(adata.obs["major_cluster"].unique())
            base_colors = sns.color_palette(color_palette_celltype, n_types).as_hex()
            color_dict = dict(zip(types, base_colors))

        colors = bin_names.map(color_dict)
        colors = colors.tolist()

    remove = ((data >= cutoff).any(axis=1)) & (data.var(axis=1) > var_cutoff)

    data["clusters"] = adata.var["clusters"]
    data_raw["clusters"] = adata.var["clusters"]

    data = data.loc[remove, :]
    data_raw = data_raw.loc[remove, :]

    if clusters is not None:
        data = data[data["clusters"].isin(clusters)]
        data_raw = data_raw[data_raw["clusters"].isin(clusters)]

    logger.info("Sorting by cluster...")
    data = data.sort_values(["clusters"])
    data_raw = data_raw.sort_values(["clusters"])
    logger.info("Done. Plotting...")

    plot = sns.clustermap(
        data.drop("clusters", axis=1),
        z_score=None,
        figsize=figure_size,
        cmap=color_palette_heatmap,
        col_colors=colors,
        vmax=vmax,
        vmin=vmin,
        yticklabels=yticklabels,
        xticklabels=xticklabels,
        col_cluster=False,
        row_cluster=False,
    )
    if output:
        plot.savefig(output_path)
        return data_raw
    else:
        return [plot, data_raw]


def CellNumberPseudotime(
    adata,
    color_dict_grouping=None,
    color_dict_celltype=None,
    relative_number=False,
    size_mod=5,
    point_size=30,
    n_cell_cutoff=10,
    point_alpha=1,
    cmap="Dark2",
    draw_grid=True,
    axis_to_hide=["top", "right"],
    rot_axis_labels=True,
):
    adata = adata.copy()

    prepared_df = pd.DataFrame(adata.uns["prepared_df"].todense())
    prepared_df.columns = adata.uns["prepared_df_var"]

    prepared_df = pd.concat(
        [prepared_df, adata.uns["prepared_df_obs"]],
        axis=1,
        join="inner",
        ignore_index=False,
        keys=None,
        levels=None,
        names=None,
        verify_integrity=False,
        copy=True,
    )

    adata.uns["prepared_df"] = prepared_df

    plot_df_mean = pd.DataFrame(
        {
            "Cell_Identity": adata.obs["major_cluster"].tolist(),
            "Pseudotime_Binned": adata.obs["pseudotime_center"].tolist(),
            "Pseudotime_min": adata.uns["prepared_df"]["Pseudotime_min"].tolist(),
            "Pseudotime_max": adata.uns["prepared_df"]["Pseudotime_max"].tolist(),
            "GROUP": adata.obs["GROUP"].tolist(),
            "n_cells": adata.obs["n_cells"].tolist(),
        }
    )

    plot_df_mean = plot_df_mean[plot_df_mean["n_cells"] >= n_cell_cutoff].copy()

    pt_max = np.ceil(plot_df_mean["Pseudotime_max"].max().tolist())
    pt_min = (
        np.floor(plot_df_mean["Pseudotime_min"].min().tolist())
        if np.ceil(plot_df_mean["Pseudotime_min"].min().tolist()) != 0
        else 0
    )
    if color_dict_celltype is None:
        color_dict_celltype = {}
        unique_members = list(plot_df_mean["Cell_Identity"].unique())
        colors = sns.color_palette(cmap, n_colors=len(unique_members)).as_hex()
        for i, member in enumerate(unique_members):
            color_dict_celltype[member] = colors[i]

    if color_dict_grouping is None:
        color_dict_grouping = {}
        unique_members = list(plot_df_mean["GROUP"].unique())
        colors = sns.color_palette(cmap, n_colors=len(unique_members)).as_hex()
        for i, member in enumerate(unique_members):
            color_dict_grouping[member] = colors[i]

    plot_df_mean["celltype_colors"] = (
        plot_df_mean["Cell_Identity"].map(color_dict_celltype).tolist()
    )
    plot_df_mean["GROUP_colors"] = (
        plot_df_mean["GROUP"].map(color_dict_grouping).tolist()
    )

    data_raw = plot_df_mean.copy()
    color_area = (
        data_raw[
            [
                "Cell_Identity",
                "celltype_colors",
                "Pseudotime_min",
                "Pseudotime_max",
            ]
        ]
        .rename(
            {
                "Pseudotime_min": "area_min",
                "Pseudotime_max": "area_max",
                "celltype_colors": "colors",
                "Cell_Identity": "name",
            },
            axis=1,
        )
        .drop_duplicates()
    )
    color_area = color_area.groupby(
        (color_area["name"].shift() != color_area["name"]).cumsum()
    ).agg(
        {
            "area_min": "min",
            "area_max": "max",
            "name": lambda column: column.unique()[0],
            "colors": lambda column: column.unique()[0],
        }
    )

    fig, ax = plt.subplots(
        1,
        1,
        figsize=(size_mod, size_mod),
        squeeze=True,
        facecolor="white",
        edgecolor="black",
    )
    fig.subplots_adjust(right=2)
    fig.tight_layout()
    for name, group in plot_df_mean.groupby("GROUP"):
        color = group["GROUP_colors"].values[0]
        max_cells = str(group["n_cells"].max())
        if relative_number:
            group["n_cells"] = PercentMax(group["n_cells"])
        ax.plot(
            "Pseudotime_Binned",
            "n_cells",
            data=group,
            color=color,
            marker=".",
            label="{name} (max. Cells per Bin: {max_cells})".format(
                name=name, max_cells=max_cells
            ),
        )

    ax.set_title("Relative (to max. per Sample) cell numbers per Pseudotime bin")
    ax.set_xlim(left=pt_min, right=pt_max)
    ax.set_ylim(bottom=-0.1)
    ax.set_xlabel("Central Pseudotime of Bin", fontsize="large", color="black")
    if relative_number:
        ax.set_ylabel(
            "Percentage of max. Cells per Bin", fontsize="large", color="black"
        )
    else:
        ax.set_ylabel("Number of Cells per Bin", fontsize="large", color="black")
    ax.spines[["left", "bottom", "top", "right"]].set_color("black")
    ax.spines[["left", "bottom", "top", "right"]].set_linewidth(1)
    ax.spines[["left", "bottom", "top", "right"]].set_visible(True)

    if axis_to_hide is not None:
        ax.spines[axis_to_hide].set_visible(False)

    if draw_grid:
        ax.grid(alpha=0.3, color="#a6a6a6")
    else:
        ax.grid(False)

    ax.set_facecolor("white")
    ax.set_aspect(1.0 / ax.get_data_ratio())

    for i, row in color_area.iterrows():
        ax.axvspan(
            row["area_min"],
            row["area_max"],
            facecolor=row["colors"],
            alpha=0.3,
            zorder=-100,
            label=row["name"],
        )

    fig.legend(loc="center left", bbox_to_anchor=(1, 0.5))

    plt.xticks(
        np.arange(
            math.floor(min(plot_df_mean["Pseudotime_Binned"])),
            math.ceil(max(plot_df_mean["Pseudotime_Binned"])) + 0.2,
            0.2,
        )
    )
    plt.xticks(rotation=45)

    fig.tight_layout()

    plt.close()
    return fig
