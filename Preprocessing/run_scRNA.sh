#!/bin/bash
#
#$ -cwd
#$ -l mem_free=2G
#$ -q all.q
#$ -j y
#$ -S /bin/bash
#$ -pe smp 2
#$ -N snakejobs
#$ -m ea
#$ -M paul-georg.majev@mpi-muenster.mpg.de
#

mkdir -p ./logs

snakemake -n --unlock

snakemake -n --rulegraph | dot -Tpdf > DAG.pdf

snakemake --conda-cleanup-envs -n

snakemake --cluster "qsub -pe smp {threads} -l mem_free={params.mem} -j yes -V -cwd -o ./logs/" --jobs 50 --use-conda -p --latency-wait 30 --rerun-incomplete --conda-frontend mamba --reason --cluster-cancel qdel

snakemake --conda-cleanup-envs -n

snakemake --report results/QC/workflow.html -n
